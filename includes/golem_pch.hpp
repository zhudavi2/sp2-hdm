
#ifndef GOLEM_SP2_PCH
#define GOLEM_SP2_PCH
#include "../golem_pch.hpp"

#include "../includes/MapEngine/map_engine.h"
#include "../includes/SDK/golem_sdk.h"
#include "../includes/common_lib/sp2_constants.h"
#include "../includes/common_lib/sp2_constants_str_id.h"
#include "../includes/common_lib/sp2_common.h"

#include "../hdm_common/Include/sp2_dzdebug.h"
#include "../hdm_common/Include/sp2_hdm_covert_action_cell.h"
#include "../hdm_common/Include/sp2_hdm_event_country_info.h"
#include "../hdm_common/Include/sp2_hdm_release.h"
#include "../hdm_common/Include/sp2_hdm_set_player_info.h"
#include "../hdm_common/Include/sp2_hdm_synchronize_country_data_event.h"
#include "../hdm_common/Include/sp2_satellite_state_control.h"


using namespace SP2;

#endif