/**************************************************************
*
* sp2_satellite_state_control.h
*
* Description
* ===========
* Common satellite state logic
*
* Copyright  (C) 2022, D.Z. (BlasterMillennia)
***************************************************************/
#ifndef _GOLEM_SP2_SATELLITE_STATE_CONTROL_H
#define _GOLEM_SP2_SATELLITE_STATE_CONTROL_H

namespace SP2
{
    class GSatelliteStateControl
    {
    public:
        //! Default or recommended treaty prefix
        static const GString c_sTreatyPrefix;

        static bool IsSatelliteStateTreaty(ETreatyType::Enum in_eType, const GString& in_sName);
        static bool IsSatelliteStateTreaty(const GTreaty& in_Treaty);
    };
};

#endif //_GOLEM_SP2_SATELLITE_STATE_CONTROL_H
