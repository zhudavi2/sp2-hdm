/**************************************************************
*
* sp2_hdm_covert_action_cell.h
*
* Description
* ===========
* Covert action cell helper utilities
*
* Copyright  (C) 2023, D.Z. (BlasterMillennia)
**************************************************************/
#ifndef _SP2_HDM_COVERT_ACTION_CELL_H
#define _SP2_HDM_COVERT_ACTION_CELL_H

namespace SP2
{
    struct GCovertActionCellCountryData
    {
        ENTITY_ID m_iAssignedCountry;
        ENTITY_ID m_iNextAssignedCountry;
        ENTITY_ID m_iCountryToFrame;
    };

    // <Cell ID, data>
    typedef hash_map<UINT32, GCovertActionCellCountryData> GCovertActionCellsCountryDataMap;

    class GHdmCovertActionCell
    {
    public:
        static void UpdateCountryDataWithCellMap(const GCovertActionCellsCountryDataMap& in_mCellMap, GCountryDataItf& in_CountryData);
    };
}

#endif // _SP2_HDM_COVERT_ACTION_CELL_H
