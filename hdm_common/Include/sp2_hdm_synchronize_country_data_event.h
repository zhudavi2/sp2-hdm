/**************************************************************
*
* sp2_hdm_synchronize_country_data_event.h
*
* Description
* ===========
*  HDM event version of player country data synchronization.
*
* Copyright  (C) 2023, D. Z. (BlasterMillennia)
***************************************************************/
#ifndef _GOLEM_SP2_HDM_COUNTRY_DATA_EVENT_H_
#define _GOLEM_SP2_HDM_COUNTRY_DATA_EVENT_H_

namespace SP2
{
    namespace Event
    {
        class GHdmSynchronizeCountryDataEvent : public SP2::Event::GSynchronizeCountryData
        {
            friend class GClient;
            friend class GServer;

            static SDK::GGameEventSPtr New();

        public:
            virtual bool Serialize(GIBuffer& io_Buffer) const;
            virtual bool Unserialize(GOBuffer& io_Buffer);

            const GCovertActionCellsCountryDataMap& CellMap() const;

        private:
            GCovertActionCellsCountryDataMap m_mCellMap;
        };
    }
}

#endif //_GOLEM_SP2_HDM_COUNTRY_DATA_EVENT_H_
