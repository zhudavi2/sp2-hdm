/**************************************************************
*
* sp2_hdm_release.h
*
* Description
* ===========
* Encapsulates HDM release number for serialization and comparison
*
* Copyright  (C) 2022, D.Z. (BlasterMillennia)
**************************************************************/
#ifndef _SP2_HDM_RELEASE_H_
#define _SP2_HDM_RELEASE_H_

namespace SP2
{
    class GHdmRelease : public GSerializable
    {
    public:
        //! Current release
        static const GHdmRelease c_HdmRelease;

        //! Dummy value for non-HDM
        static const GHdmRelease c_NonHdm;

        //! HDM save file marker, c_iSP2GameType counterpart
        static const UINT32 c_iGameType = MAKEFOURCC('H', 'D', 'M', '\0');

        // SDK mod name to display in main menu and server list
        static const GString c_sName;

        GHdmRelease();
        GHdmRelease(INT32 in_iMajor);
        GHdmRelease(INT32 in_iMajor, INT32 in_iMinor);
        GHdmRelease(INT32 in_iMajor, INT32 in_iMinor, INT32 in_iPatch);

        GString ToString() const;

        bool Serialize(GIBuffer& io_Buffer) const;
        bool Unserialize(GOBuffer& io_Buffer);

        bool operator == (const GHdmRelease& in_Other) const;
        bool operator > (const GHdmRelease& in_Other) const;
        bool operator >= (const GHdmRelease& in_Other) const;
        bool operator <= (const GHdmRelease& in_Other) const;

    private:
        //! Number of components in internal vector representing release number
        static const INT32 c_iNbComponents = 3;

        GVector3D<INT32> m_viRelease;
    };
}

#endif // _SP2_HDM_RELEASE_H_
