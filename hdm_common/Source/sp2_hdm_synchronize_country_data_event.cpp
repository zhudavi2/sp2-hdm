/**************************************************************
*
* sp2_hdm_synchronize_country_data_event.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Copyright  (C) 2023, D. Z. (BlasterMillennia)
***************************************************************/

#include "golem_pch.hpp"

//! Markers for data types
namespace EEventDataType
{
    enum Enum
    {
        End114,
        CovertActionCellCountries114,
    };
}

SDK::GGameEventSPtr SP2::Event::GHdmSynchronizeCountryDataEvent::New()
{
    GDZLOG(EDZLogLevel::Entry, L"");
    SDK::GGameEventSPtr l_Event = SDK::GGameEventSPtr(new GHdmSynchronizeCountryDataEvent());
    GDZLOG(EDZLogLevel::Exit, L"Returning " + GDZDebug::FormatPtr(l_Event.get()));
    return l_Event;
}

bool SP2::Event::GHdmSynchronizeCountryDataEvent::Serialize(GIBuffer& io_Buffer) const
{
    GDZLOG(EDZLogLevel::Entry, L"io_Buffer.Size() = " + GString(io_Buffer.Size()));

    bool l_bReturn = false;
    if(__super::Serialize(io_Buffer))
    {
        GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Size() = " + GString(io_Buffer.Size()) + L", m_mCellMap.size() = " + GString(m_mCellMap.size()));

        io_Buffer << static_cast<UINT32>(EEventDataType::CovertActionCellCountries114)
                  << m_mCellMap;

        GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Size() = " + GString(io_Buffer.Size()));

        io_Buffer << static_cast<UINT32>(EEventDataType::End114);
        GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Size() = " + GString(io_Buffer.Size()));
        l_bReturn = true;
    }

    GDZLOG(EDZLogLevel::Exit, L"Returning " + GString(l_bReturn));
    return l_bReturn;
}

bool SP2::Event::GHdmSynchronizeCountryDataEvent::Unserialize(GOBuffer& io_Buffer)
{
    GDZLOG(EDZLogLevel::Entry, L"io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()));

    bool l_bReturn = false;
    if(__super::Unserialize(io_Buffer) && io_Buffer.Remaining() >= sizeof(UINT32))
    {
        GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()));

        bool l_bEndFound = false;
        while((io_Buffer.Remaining() > sizeof(UINT32)) && !l_bEndFound)
        {
            UINT32 l_iDataType = 0;
            io_Buffer >> l_iDataType;
            switch(l_iDataType)
            {
            case EEventDataType::End114:
            {
                GDZLOG(EDZLogLevel::Info1, L"Found end marker " + GString(EEventDataType::End114) + L", stop loading");
                l_bEndFound = true;
                break;
            }
            case EEventDataType::CovertActionCellCountries114:
            {
                io_Buffer >> m_mCellMap;
                break;
            }
            default:
                GDZLOG(EDZLogLevel::Info1, L"Ignoring data marker value " + GString(l_iDataType));
                break;
            }
        }

        GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()) + L", m_mCellMap.size() = " + GString(m_mCellMap.size()));
        l_bReturn = true;
    }

    GDZLOG(EDZLogLevel::Exit, L"Returning " + GString(l_bReturn));
    return l_bReturn;
}

const GCovertActionCellsCountryDataMap& SP2::Event::GHdmSynchronizeCountryDataEvent::CellMap() const
{
    return m_mCellMap;
}
