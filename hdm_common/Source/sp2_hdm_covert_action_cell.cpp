/**************************************************************
*
* sp2_hdm_covert_action_cell.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Copyright  (C) 2023, D. Z. (BlasterMillennia)
***************************************************************/

#include "golem_pch.hpp"

void GHdmCovertActionCell::UpdateCountryDataWithCellMap(const GCovertActionCellsCountryDataMap& in_mCellMap, GCountryDataItf& io_CountryData)
{
    GDZLOG(EDZLogLevel::Entry, L"in_mCellMap.size() = " + GString(in_mCellMap.size()) + L", io_CountryData.CountryID() = " + GString(io_CountryData.CountryID()));

    for_each(in_mCellMap.cbegin(), in_mCellMap.cend(), [&io_CountryData](const GCovertActionCellsCountryDataMap::value_type& in_Pair)
    {
        GCovertActionCell& l_Cell = io_CountryData.CovertActionCell(in_Pair.first);
        l_Cell.OwnerID(io_CountryData.CountryID());

        const GCovertActionCellCountryData& l_Data = in_Pair.second;
        l_Cell.AssignedCountry(l_Data.m_iAssignedCountry);
        l_Cell.NextAssignedCountry(l_Data.m_iNextAssignedCountry);
        l_Cell.CountryToFrame(l_Data.m_iCountryToFrame);

        gassert(l_Cell.Dirty(), "Updated cell but cell not dirty");
    });

    GDZLOG(EDZLogLevel::Exit, L"");
}
