/**************************************************************
*
* sp2_satellite_state_control.cpp
*
* Description
* ===========
* See corresponding .h file
*
** Copyright  (C) 2022, D.Z. (BlasterMillennia)
***************************************************************/
#include "golem_pch.hpp"

const GString GSatelliteStateControl::c_sTreatyPrefix = L"SATELLITE";

bool GSatelliteStateControl::IsSatelliteStateTreaty(const ETreatyType::Enum in_eType, const GString& in_sName)
{
    bool l_bResult = false;

    if(in_eType == ETreatyType::MilitaryAccess)
    {
        // Accept any of these prefixes for satellite state treaty
        static const array<GString, 2> c_vsPrefixes = { c_sTreatyPrefix, L"CLIENT" };
        l_bResult = any_of(c_vsPrefixes.cbegin(), c_vsPrefixes.cend(), [&in_sName](const GString& in_sPrefix) { return in_sName.find(in_sPrefix) == 0; });
    }

    return l_bResult;
}

bool GSatelliteStateControl::IsSatelliteStateTreaty(const GTreaty& in_Treaty)
{
    return IsSatelliteStateTreaty(in_Treaty.Type(), in_Treaty.Name());
}
