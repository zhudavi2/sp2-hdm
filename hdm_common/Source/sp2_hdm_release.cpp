/**************************************************************
*
* sp2_hdm_release.cpp
*
* Description
* ===========
* See corresponding .h file
*
** Copyright  (C) 2022, D.Z. (BlasterMillennia)
***************************************************************/
#include "golem_pch.hpp"

const GHdmRelease GHdmRelease::c_HdmRelease = GHdmRelease(11, 4);
const GHdmRelease GHdmRelease::c_NonHdm = GHdmRelease();
const GString GHdmRelease::c_sName = L"Human Dev Mod " + c_HdmRelease.ToString();

GHdmRelease::GHdmRelease() :
m_viRelease(0, 0, 0)
{
}

GHdmRelease::GHdmRelease(const INT32 in_iMajor) :
m_viRelease(in_iMajor, 0, 0)
{
}

GHdmRelease::GHdmRelease(const INT32 in_iMajor, const INT32 in_iMinor) :
m_viRelease(in_iMajor, in_iMinor, 0)
{
}

GHdmRelease::GHdmRelease(const INT32 in_iMajor, const INT32 in_iMinor, const INT32 in_iPatch) :
m_viRelease(in_iMajor, in_iMinor, in_iPatch)
{
}

GString GHdmRelease::ToString() const
{
    return GString(m_viRelease.x) + L"." + GString(m_viRelease.y) + ((m_viRelease.z != 0) ? (L"." + GString(m_viRelease.z)) : L"");
}

bool GHdmRelease::Serialize(GIBuffer& io_Buffer) const
{
    GDZLOG(EDZLogLevel::Entry, L"io_Buffer.Size() = " + GString(io_Buffer.Size()));
    io_Buffer << m_viRelease;
    GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Size() = " + GString(io_Buffer.Size()) + L", " + ToString());
    GDZLOG(EDZLogLevel::Exit, L"Return true");
    return true;
}

bool GHdmRelease::Unserialize(GOBuffer& io_Buffer)
{
    GDZLOG(EDZLogLevel::Entry, L"io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()));
    bool l_bResult = true;
    if(io_Buffer.Remaining() >= sizeof(GVector3D<INT32>))
        io_Buffer >> m_viRelease;
    else
    {
        GDZLOG(EDZLogLevel::Warning, L"Buffer remaining size " + GString(io_Buffer.Remaining()) + L" not enough for data size " + GString(sizeof(GVector3D<INT32>)));
        l_bResult = false;
    }
    GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()) + L", " + ToString());
    GDZLOG(EDZLogLevel::Exit, L"Returning " + GString(l_bResult));
    return l_bResult;
}

bool GHdmRelease::operator == (const GHdmRelease& in_Other) const
{
    return m_viRelease == in_Other.m_viRelease;
}

bool GHdmRelease::operator > (const GHdmRelease& in_Other) const
{
    bool l_bResult = false;

    for(INT32 i = 0; i < c_iNbComponents; i++)
    {
        l_bResult = m_viRelease[i] > in_Other.m_viRelease[i];
        if(m_viRelease[i] != in_Other.m_viRelease[i])
            break;
    }

    return l_bResult;
}

bool GHdmRelease::operator >= (const GHdmRelease& in_Other) const
{
    return (*this == in_Other) || (*this > in_Other);
}

bool GHdmRelease::operator <= (const GHdmRelease& in_Other) const
{
    return !(*this > in_Other);
}

