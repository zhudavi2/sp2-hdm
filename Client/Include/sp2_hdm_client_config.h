/**************************************************************
*
* sp2_hdm_client_config.h
*
* Description
* ===========
*  Helper class to contain HDM client config settings.
*
* Copyright  (C) 2022, D. Z. (BlasterMillennia)
***************************************************************/
#ifndef _SP2_HDM_CLIENT_CONFIG_H
#define _SP2_HDM_CLIENT_CONFIG_H

namespace SP2
{
    namespace EShownIdType
    {
        enum Enum
        {
            Country = 0,
            Region,
            Treaty,
            ItemCount,
        };
    };

    class GHdmClientConfig
    {
    private:
        REAL32 m_fCloudSpeed;
        REAL32 m_fDaySpeed;

        REAL32 m_fDisplayThresholdLanguage;
        REAL32 m_fDisplayThresholdReligion;

        map<EShownIdType::Enum, bool> m_mShownIds;

    public:
        GHdmClientConfig();

        void LoadConfigFile();

        REAL32 CloudSpeed() const;
        REAL32 DaySpeed() const;

        REAL32 DisplayThresholdLanguage() const;
        REAL32 DisplayThresholdReligion() const;

        bool ShownId(EShownIdType::Enum in_eType) const;
   };
}

#endif //_SP2_HDM_CLIENT_CONFIG_H
