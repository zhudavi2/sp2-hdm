/**************************************************************
*
* sp2_hdm_client_config.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Copyright  (C) 2022, D. Z. (BlasterMillennia)
***************************************************************/
#include "golem_pch.hpp"

#include "../include/sp2_fsm_main.h"

GHdmClientConfig::GHdmClientConfig() :
m_fCloudSpeed(-1.f / 40.f),
m_fDaySpeed(1.f / 50.f),
m_fDisplayThresholdLanguage(0.f),
m_fDisplayThresholdReligion(0.f)
{
    GDZLOG(EDZLogLevel::Entry, L"");

    for(INT32 i = EShownIdType::Country; i < EShownIdType::ItemCount; i++)
        m_mShownIds[static_cast<EShownIdType::Enum>(i)] = false;

    GDZLOG(EDZLogLevel::Exit, L"Returning " + GDZDebug::FormatPtr(this));
}

void GHdmClientConfig::LoadConfigFile()
{
    GDZLOG(EDZLogLevel::Entry, L"");

    do
    {
        GFile l_HdmClientConfig;
        if(!g_Joshua.FileManager()->File(L"hdm_cfg_client.xml", l_HdmClientConfig))
            break;

        GXMLParser l_Parser;
        GTree<GXMLNode>* const m_pHdmClientConfigRoot = l_Parser.Parse(l_HdmClientConfig);
        if(m_pHdmClientConfigRoot == nullptr)
            break;

        CXML_Util l_Util;

        {
            const CTreeXMLNode* const l_pCloudSpeedRoot = l_Util.Search_Const_Node(m_pHdmClientConfigRoot->Root(), L"Cloud_Speed", L"", nullptr, 0);
            if(l_pCloudSpeedRoot != nullptr)
                m_fCloudSpeed = l_pCloudSpeedRoot->Data().m_value.ToREAL32();

            g_Joshua.Log(L"Cloud_Speed = " + GString::FormatNumber(m_fCloudSpeed, 3));
        }

        {
            const CTreeXMLNode* const l_pDaySpeedRoot = l_Util.Search_Const_Node(m_pHdmClientConfigRoot->Root(), L"Day_Speed", L"", nullptr, 0);
            if(l_pDaySpeedRoot != nullptr)
                m_fDaySpeed = l_pDaySpeedRoot->Data().m_value.ToREAL32();

            g_Joshua.Log(L"Day_Speed = " + GString::FormatNumber(m_fDaySpeed, 3));
        }

        {
            const CTreeXMLNode* const l_pDisplayThresholdLanguageRoot = l_Util.Search_Const_Node(m_pHdmClientConfigRoot->Root(), L"Display_Threshold_Language", L"", nullptr, 0);
            if(l_pDisplayThresholdLanguageRoot != nullptr)
                m_fDisplayThresholdLanguage = l_pDisplayThresholdLanguageRoot->Data().m_value.ToREAL32() / 100;

            g_Joshua.Log(L"Display_Threshold_Language = " + GString::FormatNumber(m_fDisplayThresholdLanguage, 3));
        }

        {
            const CTreeXMLNode* const l_pDisplayThresholdReligionRoot = l_Util.Search_Const_Node(m_pHdmClientConfigRoot->Root(), L"Display_Threshold_Religion", L"", nullptr, 0);
            if(l_pDisplayThresholdReligionRoot != nullptr)
                m_fDisplayThresholdReligion = l_pDisplayThresholdReligionRoot->Data().m_value.ToREAL32() / 100;

            g_Joshua.Log(L"Display_Threshold_Religion = " + GString::FormatNumber(m_fDisplayThresholdReligion, 3));
        }

        {
            const CTreeXMLNode* const l_pShownIdsRoot = l_Util.Search_Const_Node(m_pHdmClientConfigRoot->Root(), L"Shown_IDs", L"", nullptr, 0);
            if(l_pShownIdsRoot != nullptr)
            {
                {
                    //Same order as EShownIdType::Enum
                    static const GString c_psShownIdStrings[] =
                    {
                        L"Country",
                        L"Region",
                        L"Treaty",
                    };

                    for(INT32 i = EShownIdType::Country; i < EShownIdType::ItemCount; i++)
                    {
                        const EShownIdType::Enum l_eType = static_cast<EShownIdType::Enum>(i);
                        const GString l_sTypeString = c_psShownIdStrings[l_eType];
                        const CTreeXMLNode* const l_pTypeRoot = l_Util.Search_Const_Node(l_pShownIdsRoot, l_sTypeString, L"", nullptr, 0);
                        const bool l_bOldValue = m_mShownIds[l_eType];
                        if(l_pTypeRoot != nullptr)
                            m_mShownIds[l_eType] = l_pTypeRoot->Data().m_value == L"true";
                        g_Joshua.Log(l_pShownIdsRoot->Data().m_sName + L"." + l_sTypeString + L" = " + GString(m_mShownIds[l_eType]));

                        //If changing country ID display, request country list to force text display update on client side
                        if((l_eType == EShownIdType::Country) && (m_mShownIds[EShownIdType::Country] != l_bOldValue) && (g_SP2Client->CurrentFSMState() != FSM::EStates::LoadData))
                            g_ClientDCL.RequestCountryListToServer();
                    }
                }
            }
        }
    } while(false);

    GDZLOG(EDZLogLevel::Exit, L"");
}

REAL32 GHdmClientConfig::CloudSpeed() const
{
    return m_fCloudSpeed;
}

REAL32 GHdmClientConfig::DaySpeed() const
{
    return m_fDaySpeed;
}

REAL32 GHdmClientConfig::DisplayThresholdLanguage() const
{
    return m_fDisplayThresholdLanguage;
}

REAL32 GHdmClientConfig::DisplayThresholdReligion() const
{
    return m_fDisplayThresholdReligion;
}

bool GHdmClientConfig::ShownId(const EShownIdType::Enum in_eType) const
{
    return m_mShownIds.at(in_eType);
}
