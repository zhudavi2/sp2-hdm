/**************************************************************
*
* sp2_research_information_holder.cpp
*
* Description
* ===========
*  Contains all the data for research management
*
* Owner
* =====
*  Francois Durand
*
* Copyright  (C) 2004, Laboratoires Golemlabs Inc.
***************************************************************/

#include "golem_pch.hpp"

GResearchInformationHolder::GResearchInformationHolder() :
m_fBudgetGround(0.0),
m_fBudgetAir(0.0),
m_fBudgetNaval(0.0),
m_fBudgetNuclear(0.0)
{
   for(INT32 c = EUnitCategory::Infantry; c < EUnitCategory::ItemCount; c++)
   {
      for(INT32 d = EUnitDesignCharacteristics::GunRange; d < EUnitDesignCharacteristics::ItemCount; d++)
      {
         m_fBudget[c][d] = 0.0;
         m_fMaxValues[c][d] = 0.f;
      }
   }
   VerifyData();
}

GResearchInformationHolder& GResearchInformationHolder::operator =(const GResearchInformationHolder& in_Original)
{
   m_fBudgetGround   = in_Original.m_fBudgetGround;
   m_fBudgetAir      = in_Original.m_fBudgetAir;
   m_fBudgetNaval    = in_Original.m_fBudgetNaval;
   m_fBudgetNuclear  = in_Original.m_fBudgetNuclear;
   memcpy(m_fBudget     , in_Original.m_fBudget   ,sizeof(REAL64)*EUnitDesignCharacteristics::ItemCount*EUnitCategory::ItemCount);
   memcpy(m_fMaxValues  , in_Original.m_fMaxValues,sizeof(REAL32)*EUnitDesignCharacteristics::ItemCount*EUnitCategory::ItemCount);
   VerifyData();
   return *this;
}

void GResearchInformationHolder::Update()
{

   SDK::GGameEventSPtr l_pEvent                  = CREATE_GAME_EVENT(SP2::Event::GUnitResearchUpdate);
   SP2::Event::GUnitResearchUpdate* l_pInfoEvent = (SP2::Event::GUnitResearchUpdate*)l_pEvent.get();
   l_pInfoEvent->m_iCountryId                    = g_SP2Client->DAL().ControlledCountryID();
   l_pInfoEvent->m_iTarget                       = SDK::Event::ESpecialTargets::Server;
   l_pInfoEvent->m_iSource                       = g_SP2Client->Id();
   g_Joshua.RaiseEvent(l_pEvent);
}

void GResearchInformationHolder::VerifyData() const
{
    const REAL64* const l_pCategoryBudgets = &m_fBudgetGround;
    for(INT32 c = EUnitCategory::Ground; c < EUnitCategory::ItemCount; c++)
    {
        const REAL64 l_fCategoryBudget = l_pCategoryBudgets[c - 1];
        gassert(!_isnan(l_fCategoryBudget), "NaN budget");
        gassert(0.0 <= l_fCategoryBudget, "Budget out of range");

        for(INT32 d = EUnitDesignCharacteristics::GunRange; d < EUnitDesignCharacteristics::ItemCount; d++)
        {
            gassert(!_isnan(m_fBudget[c][d]), "NaN budget");
            gassert(0.0 <= m_fBudget[c][d], "Budget out of range");

            gassert(!_isnan(m_fMaxValues[c][d]), "NaN max value");
            gassert(0.f <= m_fMaxValues[c][d], "Max value out of range");
        }

        const REAL64 l_fCategoryTotal = accumulate(m_fBudget[c], m_fBudget[c] + EUnitDesignCharacteristics::ItemCount, 0.0);
        gassert(abs(l_fCategoryTotal - l_fCategoryBudget) < 1.0, "Inconsistent category budget");
    }
}
