<SP2-HDM>
  <!-- If true, starting a new game will add each region's initial political controller as a claimant to that region (if it's not already a claimant). 
  Allowed values are true and false. Default is false.-->
  <Add_Current_Region_Owner_Claims>false</Add_Current_Region_Owner_Claims>

  <!-- Setting to 0 disallows human-controlled countries to have AI countries
  assume their debts.
  Allowed values are 0 and 1. Default is 1. -->
  <allowAIAssumeDebt>1</allowAIAssumeDebt>
  
  <!-- Setting to 0 disallows a country on the defending side of a war from occupying the home
  territory of a country on the attacking side, unless the occupying country is also currently in an
  offensive war against the owning country.
  Region liberation is still allowed.
  Allowed values are 0 and 1. Default is 1. -->
  <allowDefenderAttackAttackerTerritory>1</allowDefenderAttackAttackerTerritory>
  
  <!-- Anarchy-related options. -->
  <anarchy>
    <!-- When a country's actual stability drops below stabilityLowerLimit, the chance of it falling into anarchy automatically will be this %.
	This effect is independent from that of expectedStabilityLowerLimit.
    Allowed values are 0-100. Default is 0. -->
    <chanceDueToStability>0</chanceDueToStability>
  
    <!-- A country will enter Anarchy when its actual and expected stability both drop to this value.
    Works best when this is less than expectedStabilityUpperLimit.
    Allowed values are 0-100. Default is 10. -->
    <expectedStabilityLowerLimit>10</expectedStabilityLowerLimit>
  
    <!-- A country will be eligible to leave Anarchy when its actual and expected stability both rise to this value.
	The country's actual stability must additionally exceed stabilityLowerLimit to actually leave anarchy.
    Allowed values are 0-100. Default is 20. -->
    <expectedStabilityUpperLimit>20</expectedStabilityUpperLimit>
    
    <!-- When a country's actual stability drops below this value, the chance of
    it falling into anarchy automatically will be chanceDueToStability %.
    Allowed values are 0-100. Default is 0. -->
    <stabilityLowerLimit>0</stabilityLowerLimit>
  </anarchy>
  
  <!-- Loss of relations with other countries due to annexing another country,
  as a percentage of normal relation loss upon annexing.
  Minimum value is 0. Default is 100. -->
  <annexationRelationLossPercent>100</annexationRelationLossPercent>
  
  <!-- Set to 1 to enable covert cells of human-controlled countries to
  automatically execute, re-prepare, and re-execute their missions when
  assigned, until their mission preparation is cancelled or when they are captured.
  Doesn't apply to coup missions.
  Allowed values are 0 and 1. Default is 0. -->
  <autoCovertMissions>0</autoCovertMissions>
  
  <!-- Civil-war-related parameters.
  A civil war may occur if a country falls into anarchy. If so, some of the
  country's regions and military units may fall under the military control of
  "rebel" countries (from hdm_database.xml), with some of *those* regions
  possibly undergoing rebel annexation.
  If rebel countries don't exist, then civil wars won't occur. -->
  <civilWar>
    <!-- Percentage chance of a civil war occurring in a country that falls into
    anarchy.
    If this is 0, then controlChance and annexChance have no effect.
    Allowed values are 0-100. Default is 0. -->
    <chance>0</chance>
    
    <!-- After a country in Anarchy exits civil war, if this many in-game days pass with the country still in Anarchy but not in civil war, then force a civil war to occur.
    A negative value means new civil wars will not automatically occur in countries that exit civil war while still in Anarchy.
    Default is -1. -->
    <period>-1</period>
    
    <!-- Percentage chance of any given region of a country, under civil war, to
    fall under rebel military control. All deployed military units of the
    original country, in that region, will also defect to the rebel side,
	unless if they are in battle when the civil war begins.
    If this is 0, then annexChance has no effect.
    Allowed values are 0-100. Default is 40. -->
    <controlChance>40</controlChance>
    
    <!-- Percentage chance of any rebel-occupied region, of a country under
    civil war, to undergo annexation by the rebels.
    This is multiplicative with controlChance; for example, if a civil war
    happens, and controlChance is 40 and annexChance is 50, then the total
    chance of any region being rebel-annexed would be 20%.
    Allowed values are 0-100. Default is 50. -->
    <annexChance>50</annexChance>
  </civilWar>
  
  <!-- Hostile military units in the same region must be at most this far from each other, in
  approximate degrees of latitude/longitude, in order to automatically engage in combat.
  Default is 0.70. -->
  <combatRangeDegrees>0.70</combatRangeDegrees>
  
  <!-- Controls save file compatibility with SP2 V1.5.x.
  Set to 1 to generate and load save files compatible with SP2 V1.5.x, but not with HDM 10.7 and earlier.
  Set to 0 to generate and load save files compatible with HDM 10.7 and earlier, but not with SP2 V1.5.x.
  Allowed values are 0 and 1. Default is 1. -->
  <compatibleSaveFormat>1</compatibleSaveFormat>
  
  <!-- Set to 1 to enable a human player to continue playing after their
  economy fails.
  Allowed values are 0 and 1. Default is 0. -->
  <continueAfterEconomicFailure>0</continueAfterEconomicFailure>
  
  <!-- Set to 1 to enable a human player to continue playing after they lose an election.
  Allowed values are 0 and 1. Default is 0. -->
  <continueAfterElectionLoss>0</continueAfterElectionLoss>
  
  <!-- Set to 0 to allow countries without regions to remain in existence.
  Big-time Conqueror achievement is unaffected.
  Allowed values are 0 and 1. Default is 1. -->
  <countryNeedsRegions>1</countryNeedsRegions>
  
  <!-- Set to 1 to show extra warning messages during game start, if HDM finds database issues.
  Allowed values are 0 and 1. Default is 0. -->
  <databaseWarningsEnabled>0</databaseWarningsEnabled>

  <!-- For dedicated servers, the game autosaves every time this number of
  minutes have passed.
  0 disables the feature. It's not recommended to set this nonzero on a non-dedicated server.
  Default is 0. -->
  <dedicatedServerAutosavePeriod>0</dedicatedServerAutosavePeriod>
  
  <!-- For dedicated servers, if set to 1, the game autosaves its save file to
  the same folder as joshua.exe, instead of the "save" folder.
  It's not recommended to set this on a non-dedicated server.
  Allowed values are 0 and 1. Default is 0. -->
  <dedicatedServerAutosaveToJoshuaFolder>0</dedicatedServerAutosaveToJoshuaFolder>
  
  <!-- Set to 1 to automatically lower a country's nuclear tech levels, and halt further nuclear
  research, for a country when
  it becomes fully occupied.
  Allowed values are 0 and 1. Default is 0. -->
  <disableNuclearOnOccupy>0</disableNuclearOnOccupy>
  
  <!-- Set to 1 to automatically cancel AMDS research or disband a completed AMDS for a country when
  it becomes fully occupied.
  Allowed values are 0 and 1. Default is 0. -->
  <disbandAMDSOnOccupy>0</disbandAMDSOnOccupy>

  <!-- The game automatically saves the EHE entity file (sp2_cfg_server.xml's ENTITIES_FILE) every time this number of minutes have passed.
  0 disables the feature. Default is 0. -->
  <Entity_Save_Period>0</Entity_Save_Period>
  
  <!-- Maximum Global Tax for resources.
  Entering into the Global Tax box anything more than this value, but less than
  any of the globalTaxSpecials values AND less than or equal to
  resourceTaxLimit, will set all resource sector taxes to the value that you
  entered.
  The above feature works best (and least confusingly) when globalTaxLimit is
  0, AND resourceTaxLimit is lower than any value in globalTaxSpecials.
  Allowed values are 0-100. Default is 100. -->
  <globalTaxLimit>100</globalTaxLimit>
  
  <!-- When maximum Global Tax is set to less than any of these values, then
  entering the following values into the Global Tax box will accordingly set
  each legal resource sector's governmental control and/or import/export level.
  Works best (and least confusingly) when globalTaxLimit is 0, AND
  resourceTaxLimit is lower than any value in globalTaxSpecials.
  Allowed values are 0-100 for each. -->
  <globalTaxSpecials>

    <!-- Set all resources to state control, and export all production. -->
    <exportAll>100</exportAll>

    <!-- Set all resources to state control, and meet domestic demand. -->
    <meetDomestic>99</meetDomestic>

    <!-- Set all resources to state control, and import all demand. -->
    <importAll>98</importAll>

    <!-- Set all resources to private control. -->
    <privatizeAll>97</privatizeAll>
  </globalTaxSpecials>
  
  <!-- Production modifiers per government type.
  A country's in-game resource production will be multiplied by the specified
  percentage corresponding to its government type. Effectively, when the game
  starts, a country's in-game resource production will not necessarily match its
  production according to the database.
  This is a configurable version of the SP2 V1.5.1 feature where production is
  naturally lower for non-multi-party-democracies.
  Allowed values are 0-100. -->
  <gvtTypeProductionModifiers>
    <com>60</com>  <!-- Default is 60.  -->
    <mil>40</mil>  <!-- Default is 40.  -->
    <mon>70</mon>  <!-- Default is 70.  -->
    <mpd>100</mpd> <!-- Default is 100. -->
    <spd>80</spd>  <!-- Default is 80.  -->
    <the>75</the>  <!-- Default is 75.  -->
  </gvtTypeProductionModifiers>
  
  <!-- Set to 0 to limit death rate to a low value when a country's population
  is aging. Setting it to 0 would be more like V1.5.1, where a country's death
  rate is independent of its age composition.
  Allowed values are 0 and 1. Default is 1. -->
  <increaseDeathRateForAgingPopulation>1</increaseDeathRateForAgingPopulation>
  
  <!-- Maximum income taxes per government type.
  Allowed values are 0-100. Defaults are 100 for all types. -->
  <incomeTaxLimits>
    <com>100</com>
    <mil>100</mil>
    <mon>100</mon>
    <mpd>100</mpd>
    <spd>100</spd>
    <the>100</the>
  </incomeTaxLimits>
  
  <!-- When set to 1, income tax rate has a more direct effect on economic growth.
  Allowed values are 0 and 1. Default is 0. -->
  <incomeTaxRateAffectsGrowth>0</incomeTaxRateAffectsGrowth>
  
  <!-- Set to 1 to print a server log message whenever a country's economy fails.
  Allowed values are 0 and 1. Default is 0. -->
  <logBankruptcies>0</logBankruptcies>
  
  <!-- The maximum number of covert cells each country can have in each foreign country.
  0 means no limit.
  Default is 0. -->
  <maximumCellsInForeignCountry>0</maximumCellsInForeignCountry>
  
  <!-- Message to broadcast to each player who joins.
  Default is "Welcome to SuperPower 2 Human Development Mod!". -->
  <message>Welcome to SuperPower 2 Human Development Mod!</message>
  
  <!-- Military production capacity as percentage of normal.
  Default is 100. -->
  <militaryProductionCapacityPercent>100</militaryProductionCapacityPercent>
  
  <!-- Military unit upkeep as a percentage of normal, per unit category,
  per training level.
  Defaults are 100 for all categories and training levels. -->
  <militaryUpkeepPercentages>
    <inf>
      <rec>100</rec>
      <reg>100</reg>
      <vet>100</vet>
      <eli>100</eli>
    </inf>
    <gro>
      <rec>100</rec>
      <reg>100</reg>
      <vet>100</vet>
      <eli>100</eli>
    </gro>
    <air>
      <rec>100</rec>
      <reg>100</reg>
      <vet>100</vet>
      <eli>100</eli>
    </air>
    <nav>
      <rec>100</rec>
      <reg>100</reg>
      <vet>100</vet>
      <eli>100</eli>
    </nav>
    <nuc>100</nuc>
  </militaryUpkeepPercentages>
  
  <!-- Set to 0 if you want to disable the "Naval Rule", which forces you to
  have a navy in order to attack an overseas enemy, among other features.
  Allowed values are 0 and 1. Default is 1. -->
  <navalRuleEnabled>1</navalRuleEnabled>
  
  <!-- Percentage of default missile range to use in calculating civilian and military casualties.
  Minimum value is 0. Default is 100. -->
  <nuclearMissileRangePercentage>100</nuclearMissileRangePercentage>
  
  <!-- % of occupied regions for a nuclear-armed country before it can use nuclear weapons.
  Allowed values are 0-100. Default is 0. -->
  <occupiedRegionPercentageForNuclear>0</occupiedRegionPercentageForNuclear>

    <!-- Limits on player actions. -->
    <Player_Limits>
        <!-- If true, these limits will also affect admin players, though server console commands are still usable.
        Allowed values are true and false. Default is false.-->
        <Admins_Affected>false</Admins_Affected>
        
        <!-- A player must wait this number of in-game days after joining, before declaring or joining war.
        Default is 0.-->
        <Days_Before_War>0</Days_Before_War>
    </Player_Limits>
  
  <!-- % drop in resource production that occurs in a region when it's annexed
  by force.
  Allowed values are 0-100. Default is 0. -->
  <productionLossOnAnnex>0</productionLossOnAnnex>
  
  <!-- Set to true so that whenever a new unit of some unit type enters a battle arena, send all existing units from the same owner and of that unit type to the arena's edge.
  Allowed values are true and false. Defaults are false for all types.
  -->
  <Reinforced_Unit_Types_To_Arena_Edge>
    <Infantry>false</Infantry>
    <Infantry_Vehicle>false</Infantry_Vehicle>
    <Air_Defense>false</Air_Defense>
    <Mobile_Launcher>false</Mobile_Launcher>
    <Tank>false</Tank>
    <Artillery_Gun>false</Artillery_Gun>
    <Attack_Helicopter>false</Attack_Helicopter>
    <Transport_Helicopter>false</Transport_Helicopter>
    <ASW_Helicopter>false</ASW_Helicopter>
    <Fighter_Aircraft>false</Fighter_Aircraft>
    <Attack_Aircraft>false</Attack_Aircraft>
    <Bomber>false</Bomber>
    <Patrol_Craft>false</Patrol_Craft>
    <Corvette>false</Corvette>
    <Frigate>false</Frigate>
    <Destroyer>false</Destroyer>
    <Attack_Submarine>false</Attack_Submarine>
    <Ballistic_Missile_Submarine>false</Ballistic_Missile_Submarine>
    <Aircraft_Carrier>false</Aircraft_Carrier>
    <Ballistic_Missile>false</Ballistic_Missile>
  </Reinforced_Unit_Types_To_Arena_Edge>

  <!-- Maximum tax for each resource sector.
  Allowed values are 0-100. Default is 100. -->
  <resourceTaxLimit>100</resourceTaxLimit>
  
  <!-- Set to 1 to enable players to receive chat messages from the server regarding various game events.
  Allowed values are 0 and 1. Default is 1. -->
  <Server_Chats_Enabled>1</Server_Chats_Enabled>
  
  <!-- Annual tribute in that a client state would theoretically pay to its
  master in one year, as a percentage of client GDP.
  Minimum value is 0. Default is 8. -->
  <tributePercent>8</tributePercent>
  
  <!-- Set to 1 to use new export mechanics, which allows countries that either have a high market share in certain resource sectors, and/or specialize in certain resource sectors, to have advantages in exporting those resources, as long as their GDP per capita values are high.
  Also forces countries that produce many non-service resources not to export as much of service resources.
  Allowed values are 0 and 1. Default is 1. -->
  <useNewExportMechanics>1</useNewExportMechanics>
  
  <!-- Set to 1 to use new formula to determine resource growth from resource tax rate.
  Allowed values are 0 and 1. Default is 1. -->
  <useNewResourceTaxFormula>1</useNewResourceTaxFormula>
</SP2-HDM>