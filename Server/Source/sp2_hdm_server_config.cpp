/**************************************************************
*
* sp2_hdm_server_config.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Copyright  (C) 2021, D. Z. (BlasterMillennia)
***************************************************************/
#include "golem_pch.hpp"

GHdmServerConfig::GHdmServerConfig() :
m_bAddCurrentRegionOwnerClaims(false),
m_bDatabaseWarningsEnabled(false),
m_bAllowDefenderAttackAttackerTerritory(true),
m_bAllowAIAssumeDebt(true),
m_fAnnexationRelationLossPercent(1.f),
m_fCombatThresholdSquare(c_fCombatThresholdSquare),
m_bCompatibleSaveFormat(true),
m_bContinueAfterEconomicFailure(false),
m_bContinueAfterElectionLoss(false),
m_bCountryNeedsRegions(true),
m_bDedicatedServerAutosaveToJoshuaFolder(false),
m_fDedicatedServerAutosavePeriod(0.f),
m_bDisableNuclearOnOccupy(false),
m_bDisbandAMDSOnOccupy(false),
m_fEntitySavePeriod(0.0),
m_fGlobalTaxLimit(1.f),
m_bIncomeTaxRateAffectsGrowth(false),
m_bIncreaseDeathRateForAgingPopulation(true),
m_bLogBankruptcies(false),
m_iMaximumCellsInForeignCountry(0),
m_fMilitaryProductionCapacityPercent(1.f),
m_bNavalRuleEnabled(true),
m_fNuclearMissileRangePercentage(1.f),
m_fNuclearUpkeepPercentage(1.f),
m_fOccupiedRegionPercentageForNuclear(0.f),
m_fResourceTaxLimit(1.f),
m_bServerChatsEnabled(true),
m_fTributePercent(0.08f),
m_bUseNewExportMechanics(true),
m_bUseNewResourceTaxFormula(true)
{
    GDZLOG(EDZLogLevel::Entry, L"");

    // Default values that can't be initialized in initializer list
    m_AnarchyConfig.m_fChanceDueToStability        = 0.f;
    m_AnarchyConfig.m_fExpectedStabilityLowerLimit = c_fStabilityAnarchyLowerLimit;
    m_AnarchyConfig.m_fExpectedStabilityUpperLimit = c_fStabilityAnarchyHigherLimit;
    m_AnarchyConfig.m_fStabilityLowerLimit         = 0.f;

    m_CivilWarConfig.m_fChance        = 0.f;
    m_CivilWarConfig.m_fPeriod        = -1.f;
    m_CivilWarConfig.m_fControlChance = 0.f;
    m_CivilWarConfig.m_fAnnexChance   = 0.f;

    m_GlobalTaxSpecials[EGlobalTaxSpecialType::ExportAll]    = 100;
    m_GlobalTaxSpecials[EGlobalTaxSpecialType::MeetDomestic] = 99;
    m_GlobalTaxSpecials[EGlobalTaxSpecialType::ImportAll]    = 98;
    m_GlobalTaxSpecials[EGlobalTaxSpecialType::PrivatizeAll] = 97;

    m_PlayerLimitsConfig.m_bAdminsAffected = false;
    m_PlayerLimitsConfig.m_iDaysBeforeWar = 0;

    for(INT32 i = 0; i < EGovernmentType::ItemCount; i++)
    {
        const EGovernmentType::Enum l_eGvtType = static_cast<EGovernmentType::Enum>(i);
        m_mGvtTypeProductionModifiers[l_eGvtType] = SP2::c_pGvtTypeProductionModifier[i];
        m_IncomeTaxLimits[l_eGvtType] = PersonalTaxes_UpperCap;
    }

    // Training levels don't exist for nuclear units, so store their upkeep separately from the main map
    for(INT32 i = EUnitCategory::Infantry; i < EUnitCategory::Nuclear; i++)
    {
        EUnitCategory::Enum l_eCategory = static_cast<EUnitCategory::Enum>(i);

        m_mMilitaryUpkeepPercentages[l_eCategory] = map<ETrainingLevel::Enum, REAL32>();
        for(INT32 j = ETrainingLevel::Recruit; j < ETrainingLevel::ItemCount; j++)
            m_mMilitaryUpkeepPercentages[l_eCategory][static_cast<ETrainingLevel::Enum>(j)] = 1.f;

        gassert(m_mMilitaryUpkeepPercentages[l_eCategory].size() == ETrainingLevel::ItemCount, L"Unexpected number of training levels");
    }
    gassert(m_mMilitaryUpkeepPercentages.size() == EUnitCategory::Nuclear, L"Unexpected number of unit categories");

    for(INT32 i = EUnitType::Infantry; i < EUnitType::TotalCount; i++)
    {
        const EUnitType::Enum l_eType = static_cast<EUnitType::Enum>(i);
        m_mReinforcedUnitTypesToArenaEdge[l_eType] = false;
    }

    GDZLOG(EDZLogLevel::Exit, L"");
}

bool GHdmServerConfig::LoadConfigFile()
{
    GDZLOG(EDZLogLevel::Entry, L"");

    bool l_bResult = false;

    do
    {
        const GString l_sConfigFilename = L"hdm_cfg_server.xml";
        GFile l_XMLFile;
        if(!g_ServerDAL.File(l_sConfigFilename, l_XMLFile))
        {
            g_Joshua.Log(SP2::ERROR_CANT_FIND_FILE + l_sConfigFilename, MSGTYPE_WARNING);
            break;
        }

        // Extract the file to a temp string so it can be parsed as XML
        GString l_sTempConfigFileText;
        if(!l_XMLFile.Extract(l_sTempConfigFileText))
        {
            g_Joshua.Log(SP2::ERROR_EXTRACT_FILE + l_XMLFile.Name(), MSGTYPE_WARNING);
            break;
        }

        GXMLParser l_XmlParser;
        GTree<GXMLNode>* const l_pXmlData = l_XmlParser.Parse(l_sTempConfigFileText);
        l_XMLFile.Unextract();
        if(l_pXmlData == nullptr)
        {
            g_Joshua.Log(SP2::ERROR_PARSING_XML_FILE + l_XMLFile.Name(), MSGTYPE_WARNING);
            break;
        }

        CTreeXMLNode* const l_pXmlRoot = l_pXmlData->Root();
        if(!l_pXmlRoot->HasChild())
        {
            g_Joshua.Log(SP2::ERROR_CANT_PROCESS_FILE + l_XMLFile.Name(), MSGTYPE_WARNING);
            break;
        }

        for(UINT32 i = 0; i < l_pXmlRoot->NbChilds(); i++)
        {
            // Helper for options per government type
            typedef pair<GString,EGovernmentType::Enum> GStringGvtTypePair;
            static const GStringGvtTypePair l_pGvtTypes[] =
            {
                GStringGvtTypePair(L"com", EGovernmentType::Communist),
                GStringGvtTypePair(L"mil", EGovernmentType::MilitaryDictatorship),
                GStringGvtTypePair(L"mon", EGovernmentType::Monarchy),
                GStringGvtTypePair(L"mpd", EGovernmentType::MultiPartyDemocracy),
                GStringGvtTypePair(L"spd", EGovernmentType::SinglePartyDemocracy),
                GStringGvtTypePair(L"the", EGovernmentType::Theocracy),
            };
            static const map<GString,EGovernmentType::Enum> l_mGvtTypes(l_pGvtTypes, l_pGvtTypes + sizeof(l_pGvtTypes) / sizeof(GStringGvtTypePair));

            const GTreeNode<GXMLNode>* const l_pNode = l_pXmlRoot->Child(i);
            const GXMLNode& l_NodeData = l_pNode->Data();
            const GString l_sNodeName  = l_NodeData.m_sName;
            const GString l_sNodeValue = l_NodeData.m_value;

            if(l_sNodeName == L"Add_Current_Region_Owner_Claims")
            {
                m_bAddCurrentRegionOwnerClaims = l_sNodeValue == L"true";
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bAddCurrentRegionOwnerClaims));
            }
            else if(l_sNodeName == L"databaseWarningsEnabled")
            {
                m_bDatabaseWarningsEnabled = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bDatabaseWarningsEnabled));
            }
            else if(l_sNodeName == L"allowAIAssumeDebt")
            {
                m_bAllowAIAssumeDebt = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bAllowAIAssumeDebt));
            }
            else if(l_sNodeName == L"allowDefenderAttackAttackerTerritory")
            {
                m_bAllowDefenderAttackAttackerTerritory = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bAllowDefenderAttackAttackerTerritory));
            }
            else if(l_sNodeName == L"anarchy")
            {
                for(UINT32 j = 0; j < l_pNode->NbChilds(); j++)
                {
                    const GXMLNode& l_AnarchyNodeData = l_pNode->Child(j)->Data();
                    const GString l_sAnarchyNodeName  = l_AnarchyNodeData.m_sName;
                    const GString l_sAnarchyNodeValue = l_AnarchyNodeData.m_value;

                    GString l_sValueString;
                    if(l_sAnarchyNodeName == L"chanceDueToStability")
                    {
                        m_AnarchyConfig.m_fChanceDueToStability = l_sAnarchyNodeValue.ToREAL32() / 100.f;
                        l_sValueString = GString::FormatNumber(m_AnarchyConfig.m_fChanceDueToStability, 2);
                    }
                    else if(l_sAnarchyNodeName == L"expectedStabilityLowerLimit")
                    {
                        m_AnarchyConfig.m_fExpectedStabilityLowerLimit = l_sAnarchyNodeValue.ToREAL32() / 100.f;
                        l_sValueString = GString::FormatNumber(m_AnarchyConfig.m_fExpectedStabilityLowerLimit, 2);
                    }
                    else if(l_sAnarchyNodeName == L"expectedStabilityUpperLimit")
                    {
                        m_AnarchyConfig.m_fExpectedStabilityUpperLimit = l_sAnarchyNodeValue.ToREAL32() / 100.f;
                        l_sValueString = GString::FormatNumber(m_AnarchyConfig.m_fExpectedStabilityUpperLimit, 2);
                    }
                    else if(l_sAnarchyNodeName == L"stabilityLowerLimit")
                    {
                        m_AnarchyConfig.m_fStabilityLowerLimit = l_sAnarchyNodeValue.ToREAL32() / 100.f;
                        l_sValueString = GString::FormatNumber(m_AnarchyConfig.m_fStabilityLowerLimit, 2);
                    }
                    else
                        g_Joshua.Log(L"Unknown HDM anarchy config option: " + l_sAnarchyNodeName, MSGTYPE_WARNING);

                    g_Joshua.Log(l_sNodeName + L"." + l_sAnarchyNodeName + L": " + l_sValueString);
                }
            }
            else if(l_sNodeName == L"annexationRelationLossPercent")
            {
                m_fAnnexationRelationLossPercent = l_sNodeValue.ToREAL32() / 100.f;
                g_Joshua.Log(l_sNodeName + L": " + GString(m_fAnnexationRelationLossPercent));
            }
            else if(l_sNodeName == L"autoCovertMissions")
            {
                m_bAutoCovertMissions = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bAutoCovertMissions));
            }
            else if(l_sNodeName == L"civilWar")
            {
                for(UINT32 j = 0; j < l_pNode->NbChilds(); j++)
                {
                    const GXMLNode& l_CivilWarNode = l_pNode->Child(j)->Data();
                    const GString l_sCivilWarNodeName  = l_CivilWarNode.m_sName;
                    const GString l_sCivilWarNodeValue = l_CivilWarNode.m_value;

                    GString l_sValueString;
                    if(l_sCivilWarNodeName == L"chance")
                    {
                        m_CivilWarConfig.m_fChance = l_sCivilWarNodeValue.ToREAL32() / 100.f;
                        l_sValueString = GString::FormatNumber(m_CivilWarConfig.m_fChance, 2);
                    }
                    else if(l_sCivilWarNodeName == L"period")
                    {
                        m_CivilWarConfig.m_fPeriod = l_sCivilWarNodeValue.ToREAL32();
                        l_sValueString = GString::FormatNumber(m_CivilWarConfig.m_fPeriod, 2);
                    }
                    else if(l_sCivilWarNodeName == L"controlChance")
                    {
                        m_CivilWarConfig.m_fControlChance = l_sCivilWarNodeValue.ToREAL32() / 100.f;
                        l_sValueString = GString::FormatNumber(m_CivilWarConfig.m_fControlChance, 2);
                    }
                    else if(l_sCivilWarNodeName == L"annexChance")
                    {
                        m_CivilWarConfig.m_fAnnexChance = l_sCivilWarNodeValue.ToREAL32() / 100.f;
                        l_sValueString = GString::FormatNumber(m_CivilWarConfig.m_fAnnexChance, 2);
                    }
                    else
                        g_Joshua.Log(L"Unknown HDM civil war config option: " + l_sCivilWarNodeName, MSGTYPE_WARNING);

                    g_Joshua.Log(l_sNodeName + L"." + l_sCivilWarNodeName + L": " + l_sValueString);
                }
            }
            else if(l_sNodeName == L"combatRangeDegrees")
            {
                const REAL32 l_fCombatRangeDegrees = l_sNodeValue.ToREAL32();
                m_fCombatThresholdSquare = l_fCombatRangeDegrees*l_fCombatRangeDegrees;
                g_Joshua.Log(l_sNodeName + L": " + GString::FormatNumber(l_fCombatRangeDegrees, 2));
            }
            else if(l_sNodeName == L"compatibleSaveFormat")
            {
                m_bCompatibleSaveFormat = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bCompatibleSaveFormat));
            }
            else if(l_sNodeName == L"continueAfterEconomicFailure")
            {
                m_bContinueAfterEconomicFailure = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bContinueAfterEconomicFailure));
            }
            else if(l_sNodeName == L"continueAfterElectionLoss")
            {
                m_bContinueAfterElectionLoss = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bContinueAfterElectionLoss));
            }
            else if(l_sNodeName == L"countryNeedsRegions")
            {
                m_bCountryNeedsRegions = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bCountryNeedsRegions));
            }
            else if(l_sNodeName == L"dedicatedServerAutosavePeriod")
            {
                m_fDedicatedServerAutosavePeriod = l_sNodeValue.ToREAL32();
                g_Joshua.Log(l_sNodeName + L": " + GString::FormatNumber(m_fDedicatedServerAutosavePeriod, 2));
            }
            else if(l_sNodeName == L"dedicatedServerAutosaveToJoshuaFolder")
            {
                m_bDedicatedServerAutosaveToJoshuaFolder = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bDedicatedServerAutosaveToJoshuaFolder));
            }
            else if(l_sNodeName == L"disableNuclearOnOccupy")
            {
                m_bDisableNuclearOnOccupy = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bDisableNuclearOnOccupy));
            }
            else if(l_sNodeName == L"disbandAMDSOnOccupy")
            {
                m_bDisbandAMDSOnOccupy = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bDisbandAMDSOnOccupy));
            }
            else if(l_sNodeName == L"Entity_Save_Period")
            {
                m_fEntitySavePeriod = l_sNodeValue.ToREAL64();
                g_Joshua.Log(l_sNodeName + L": " + GString(m_fEntitySavePeriod));
            }
            else if(l_sNodeName == L"globalTaxLimit")
            {
                GlobalTaxLimit(l_sNodeValue.ToREAL32() / 100.f);
                g_Joshua.Log(l_sNodeName + L": " + GString::FormatNumber(m_fGlobalTaxLimit, 3));
            }
            else if(l_sNodeName == L"globalTaxSpecials")
            {
                for(UINT32 j = 0; j < l_pNode->NbChilds(); j++)
                {
                    const GTreeNode<GXMLNode>* const l_GovernmentNode = l_pNode->Child(j);

                    const GString l_sGovernmentNodeName = l_GovernmentNode->Data().m_sName;
                    EGlobalTaxSpecialType::Enum l_eGlobalTaxSpecial = EGlobalTaxSpecialType::ItemCount;
                    if(l_sGovernmentNodeName == L"exportAll")
                        l_eGlobalTaxSpecial = EGlobalTaxSpecialType::ExportAll;
                    else if(l_sGovernmentNodeName == L"meetDomestic")
                        l_eGlobalTaxSpecial = EGlobalTaxSpecialType::MeetDomestic;
                    else if(l_sGovernmentNodeName == L"importAll")
                        l_eGlobalTaxSpecial = EGlobalTaxSpecialType::ImportAll;
                    else if(l_sGovernmentNodeName == L"privatizeAll")
                        l_eGlobalTaxSpecial = EGlobalTaxSpecialType::PrivatizeAll;
                    else
                        g_Joshua.Log(L"Unknown HDM global tax config option: " + l_sGovernmentNodeName, MSGTYPE_WARNING);

                    m_GlobalTaxSpecials[l_eGlobalTaxSpecial] = l_GovernmentNode->Data().m_value.ToINT32();
                    g_Joshua.Log(l_sNodeName + L"[" + l_sGovernmentNodeName + L"]: " + GString(m_GlobalTaxSpecials[l_eGlobalTaxSpecial]));
                }
            }
            else if(l_sNodeName == L"gvtTypeProductionModifiers")
            {
                for(UINT32 j = 0; j < l_pNode->NbChilds(); j++)
                {
                    const GTreeNode<GXMLNode>* l_GovernmentNode = l_pNode->Child(j);

                    const GString l_sGovernmentNodeName = l_GovernmentNode->Data().m_sName;
                    const EGovernmentType::Enum l_eGovernmentType = l_mGvtTypes.at(l_sGovernmentNodeName);
                    m_mGvtTypeProductionModifiers[l_eGovernmentType] = l_GovernmentNode->Data().m_value.ToREAL64() / 100.0;
                    g_Joshua.Log(l_sNodeName + L"[" + l_sGovernmentNodeName + L"]: " + GString::FormatNumber(m_mGvtTypeProductionModifiers[l_eGovernmentType], 3));
                }
            }
            else if(l_sNodeName == L"incomeTaxLimits")
            {
                for(UINT32 j = 0; j < l_pNode->NbChilds(); j++)
                {
                    const GTreeNode<GXMLNode>* l_GovernmentNode = l_pNode->Child(j);

                    const GString l_sGovernmentNodeName = l_GovernmentNode->Data().m_sName;
                    const EGovernmentType::Enum l_eGovernmentType = l_mGvtTypes.at(l_sGovernmentNodeName);
                    IncomeTaxLimit(l_eGovernmentType, l_GovernmentNode->Data().m_value.ToREAL64() / 100.0);
                    g_Joshua.Log(l_sNodeName + L"[" + l_sGovernmentNodeName + L"]: " + GString::FormatNumber(m_IncomeTaxLimits[l_eGovernmentType], 3));
                }
            }
            else if(l_sNodeName == L"incomeTaxRateAffectsGrowth")
            {
                m_bIncomeTaxRateAffectsGrowth = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bIncomeTaxRateAffectsGrowth));
            }
            else if(l_sNodeName == L"increaseDeathRateForAgingPopulation")
            {
                m_bIncreaseDeathRateForAgingPopulation = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bIncreaseDeathRateForAgingPopulation));
            }
            else if(l_sNodeName == L"logBankruptcies")
            {
                m_bLogBankruptcies = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bLogBankruptcies));
            }
            else if(l_sNodeName == L"maximumCellsInForeignCountry")
            {
                MaximumCellsInForeignCountry(l_sNodeValue.ToINT32());
                g_Joshua.Log(l_sNodeName + L": " + GString(m_iMaximumCellsInForeignCountry));
            }
            else if(l_sNodeName == L"message")
            {
                m_sMessage = l_sNodeValue;
                g_Joshua.Log(l_sNodeName + L": " + m_sMessage);
            }
            else if(l_sNodeName == L"militaryProductionCapacityPercent")
            {
                m_fMilitaryProductionCapacityPercent = l_sNodeValue.ToREAL32() / 100.f;
                g_Joshua.Log(l_sNodeName + L": " + GString::FormatNumber(m_fMilitaryProductionCapacityPercent, 3));
            }
            else if(l_sNodeName == L"militaryUpkeepPercentages")
            {
                // Upkeep for all unit categories, including nuclear, are together in the XML
                // But nuclear upkeep will be stored separately from the others in the GHdmServerConfig object
                for(UINT32 j = 0; j < EUnitCategory::ItemCount; j++)
                {
                    const GTreeNode<GXMLNode>* const l_CategoryNode = l_pNode->Child(j);

                    const GString l_sCategoryName = l_CategoryNode->Data().m_sName;
                    EUnitCategory::Enum l_eUnitCategory = EUnitCategory::ItemCount;
                    if(l_sCategoryName == L"inf")
                        l_eUnitCategory = EUnitCategory::Infantry;
                    else if(l_sCategoryName == L"gro")
                        l_eUnitCategory = EUnitCategory::Ground;
                    else if(l_sCategoryName == L"air")
                        l_eUnitCategory = EUnitCategory::Air;
                    else if(l_sCategoryName == L"nav")
                        l_eUnitCategory = EUnitCategory::Naval;
                    else if(l_sCategoryName == L"nuc")
                        l_eUnitCategory = EUnitCategory::Nuclear;
                    else
                        g_Joshua.Log(L"Unknown HDM military upkeep config option: " + l_sCategoryName, MSGTYPE_WARNING);

                    const UINT32 l_iNbCNChildren = l_CategoryNode->NbChilds();

                    switch(l_eUnitCategory)
                    {
                    case EUnitCategory::Nuclear:
                        gassert(l_iNbCNChildren == 0, L"Nuclear node has  " + GString(l_iNbCNChildren) + L" children, expected 0");
                        m_fNuclearUpkeepPercentage = l_CategoryNode->Data().m_value.ToREAL32() / 100.f;
                        g_Joshua.Log(L"militaryUpkeepPercentages[" + l_sCategoryName + L"]: " + GString::FormatNumber(m_fNuclearUpkeepPercentage, 3));
                        break;

                    default:
                        for(UINT32 k = 0; k<ETrainingLevel::ItemCount; k++)
                        {
                            const GTreeNode<GXMLNode>* const l_pTrainingNode = l_CategoryNode->Child(k);

                            const GString l_sTrainingName = l_pTrainingNode->Data().m_sName;
                            ETrainingLevel::Enum l_eTrainingLevel = ETrainingLevel::ItemCount;
                            if(l_sTrainingName == L"rec")
                                l_eTrainingLevel = ETrainingLevel::Recruit;
                            else if(l_sTrainingName == L"reg")
                                l_eTrainingLevel = ETrainingLevel::Regular;
                            else if(l_sTrainingName == L"vet")
                                l_eTrainingLevel = ETrainingLevel::Veteran;
                            else if(l_sTrainingName == L"eli")
                                l_eTrainingLevel = ETrainingLevel::Elite;

                            m_mMilitaryUpkeepPercentages[l_eUnitCategory][l_eTrainingLevel] = l_pTrainingNode->Data().m_value.ToREAL32() / 100.f;
                            g_Joshua.Log(l_sNodeName + L"[" + l_sCategoryName + L"][" + l_sTrainingName + L"]: " +
                            GString::FormatNumber(m_mMilitaryUpkeepPercentages[l_eUnitCategory][l_eTrainingLevel], 3));
                        }
                        break;
                    }
                }
            }
            else if(l_sNodeName == L"navalRuleEnabled")
            {
                m_bNavalRuleEnabled = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L"navalRuleEnabled: " + GString(m_bNavalRuleEnabled));
            }
            else if(l_sNodeName == L"nuclearMissileRangePercentage")
            {
                m_fNuclearMissileRangePercentage = l_sNodeValue.ToREAL32() / 100.f;
                g_Joshua.Log(l_sNodeName + L": " + GString::FormatNumber(m_fNuclearMissileRangePercentage, 2));
            }
            else if(l_sNodeName == L"occupiedRegionPercentageForNuclear")
            {
                m_fOccupiedRegionPercentageForNuclear = l_sNodeValue.ToREAL32() / 100.f;
                g_Joshua.Log(l_sNodeName + L": " + GString::FormatNumber(m_fOccupiedRegionPercentageForNuclear, 2));
            }
            else if(l_sNodeName == L"Player_Limits")
            {
                for(UINT32 j = 0; j < l_pNode->NbChilds(); j++)
                {
                    const GTreeNode<GXMLNode>* const l_pLimitNode = l_pNode->Child(j);
                    const GXMLNode& l_LimitData = l_pLimitNode->Data();
                    const GString l_sLimit = l_LimitData.m_sName;
                    if(l_sLimit == L"Admins_Affected")
                    {
                        m_PlayerLimitsConfig.m_bAdminsAffected = l_LimitData.m_value == L"true";
                        g_Joshua.Log(l_sNodeName + L": " + GString(m_PlayerLimitsConfig.m_bAdminsAffected));
                    }
                    else if(l_sLimit == L"Days_Before_War")
                    {
                        m_PlayerLimitsConfig.m_iDaysBeforeWar = l_LimitData.m_value.ToINT32();
                        g_Joshua.Log(l_sNodeName + L": " + GString(m_PlayerLimitsConfig.m_iDaysBeforeWar));
                    }
                }
            }
            else if(l_sNodeName == L"productionLossOnAnnex")
            {
                m_fProductionLossOnAnnex = l_sNodeValue.ToREAL32() / 100.f;
                g_Joshua.Log(l_sNodeName + L": " + GString::FormatNumber(m_fProductionLossOnAnnex, 3));
            }
            else if(l_sNodeName == L"Reinforced_Unit_Types_To_Arena_Edge")
            {
                for(UINT32 j = 0; j < l_pNode->NbChilds(); j++)
                {
                    //Helper for node names per unit type
                    typedef pair<GString, EUnitType::Enum> GStringUnitTypePair;
                    static const GStringUnitTypePair c_UnitTypes[] =
                    {
                        GStringUnitTypePair(L"Infantry",                    EUnitType::Infantry),
                        GStringUnitTypePair(L"Infantry_Vehicle",            EUnitType::InfantryVehicle),
                        GStringUnitTypePair(L"Air_Defense",                 EUnitType::AirDefense),
                        GStringUnitTypePair(L"Mobile_Launcher",             EUnitType::MobileLauncher),
                        GStringUnitTypePair(L"Tank",                        EUnitType::Tank),
                        GStringUnitTypePair(L"Artillery_Gun",               EUnitType::ArtilleryGun),
                        GStringUnitTypePair(L"Attack_Helicopter",           EUnitType::AttackHelicopter),
                        GStringUnitTypePair(L"Transport_Helicopter",        EUnitType::TransportHelicopter),
                        GStringUnitTypePair(L"ASW_Helicopter",              EUnitType::ASWHelicopter),
                        GStringUnitTypePair(L"Fighter_Aircraft",            EUnitType::FighterAircraft),
                        GStringUnitTypePair(L"Attack_Aircraft",             EUnitType::AttackAircraft),
                        GStringUnitTypePair(L"Bomber",                      EUnitType::Bomber),
                        GStringUnitTypePair(L"Patrol_Craft",                EUnitType::PatrolCraft),
                        GStringUnitTypePair(L"Corvette",                    EUnitType::Corvette),
                        GStringUnitTypePair(L"Frigate",                     EUnitType::Frigate),
                        GStringUnitTypePair(L"Destroyer",                   EUnitType::Destroyer),
                        GStringUnitTypePair(L"Attack_Submarine",            EUnitType::AttackSubmarine),
                        GStringUnitTypePair(L"Ballistic_Missile_Submarine", EUnitType::BallisticMissileSubmarine),
                        GStringUnitTypePair(L"Aircraft_Carrier",            EUnitType::AircraftCarrier),
                        GStringUnitTypePair(L"Ballistic_Missile",           EUnitType::BallisticMissile),
                    };
                    static const map<GString, EUnitType::Enum> c_mUnitTypes(c_UnitTypes, c_UnitTypes + sizeof(c_UnitTypes) / sizeof(GStringUnitTypePair));

                    const GTreeNode<GXMLNode>* const l_pTypeNode = l_pNode->Child(j);
                    const GString l_sTypeName = l_pTypeNode->Data().m_sName;
                    if(c_mUnitTypes.count(l_sTypeName) >= 1)
                    {
                        bool& l_bConfigValue = m_mReinforcedUnitTypesToArenaEdge[c_mUnitTypes.at(l_sTypeName)];
                        l_bConfigValue = l_pTypeNode->Data().m_value == L"true";
                        g_Joshua.Log(l_sNodeName + L"[" + l_sTypeName + L"]: " + GString(l_bConfigValue));
                    }
                    else
                        g_Joshua.Log(L"Unknown " + l_sNodeName + L" unit type name " + l_sTypeName);
                }
            }
            else if(l_sNodeName == L"resourceTaxLimit")
            {
                ResourceTaxLimit(l_sNodeValue.ToREAL32() / 100.f);
                g_Joshua.Log(l_sNodeName + L": " + GString::FormatNumber(m_fResourceTaxLimit, 3));
            }
            else if(l_sNodeName == L"Server_Chats_Enabled")
            {
                m_bServerChatsEnabled = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bServerChatsEnabled));
            }
            else if(l_sNodeName == L"tributePercent")
            {
                m_fTributePercent = l_sNodeValue.ToREAL32() / 100.f;
                g_Joshua.Log(l_sNodeName + L": " + GString::FormatNumber(m_fTributePercent, 2));
            }
            else if(l_sNodeName == L"useNewExportMechanics")
            {
                m_bUseNewExportMechanics = (l_sNodeValue.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bUseNewExportMechanics));
            }
            else if(l_sNodeName == L"useNewResourceTaxFormula")
            {
                m_bUseNewResourceTaxFormula = (l_sNodeName.ToINT32() != 0);
                g_Joshua.Log(l_sNodeName + L": " + GString(m_bUseNewResourceTaxFormula));
            }
            else
                g_Joshua.Log(L"Unknown HDM config option: " + l_sNodeName, MSGTYPE_WARNING);
        }

        l_bResult = true;
    } while(false);

    GDZLOG(EDZLogLevel::Exit, L"Returning " + l_bResult);
    return l_bResult;
}

bool GHdmServerConfig::AddCurrentRegionOwnerClaims() const
{
    return m_bAddCurrentRegionOwnerClaims;
}

bool GHdmServerConfig::DatabaseWarningsEnabled() const
{
    return m_bDatabaseWarningsEnabled;
}

bool GHdmServerConfig::AllowAIAssumeDebt() const
{
    return m_bAllowAIAssumeDebt;
}

bool GHdmServerConfig::AllowDefenderAttackAttackerTerritory() const
{
    return m_bAllowDefenderAttackAttackerTerritory;
}

GAnarchyConfig GHdmServerConfig::AnarchyConfig() const
{
    return m_AnarchyConfig;
}

REAL32 GHdmServerConfig::AnnexationRelationLossPercent() const
{
    return m_fAnnexationRelationLossPercent;
}

bool GHdmServerConfig::AutoCovertMissions() const
{
    return m_bAutoCovertMissions;
}

GCivilWarConfig GHdmServerConfig::CivilWarConfig() const
{
    return m_CivilWarConfig;
}

REAL32 GHdmServerConfig::CombatThresholdSquare() const
{
    return m_fCombatThresholdSquare;
}

bool GHdmServerConfig::CompatibleSaveFormat() const
{
    return m_bCompatibleSaveFormat;
}

bool GHdmServerConfig::ContinueAfterEconomicFailure() const
{
    return m_bContinueAfterEconomicFailure;
}

bool GHdmServerConfig::ContinueAfterElectionLoss() const
{
    return m_bContinueAfterElectionLoss;
}

bool GHdmServerConfig::CountryNeedsRegions() const
{
    return m_bCountryNeedsRegions;
}

REAL32 GHdmServerConfig::DedicatedServerAutosavePeriod() const
{
    return m_fDedicatedServerAutosavePeriod;
}

bool GHdmServerConfig::DedicatedServerAutosaveToJoshuaFolder() const
{
    return m_bDedicatedServerAutosaveToJoshuaFolder;
}

bool GHdmServerConfig::DisableNuclearOnOccupy() const
{
    return m_bDisableNuclearOnOccupy;
}

bool GHdmServerConfig::DisbandAMDSOnOccupy() const
{
    return m_bDisbandAMDSOnOccupy;
}

REAL64 GHdmServerConfig::EntitySavePeriod() const
{
    return m_fEntitySavePeriod;
}

REAL32 GHdmServerConfig::GlobalTaxLimit() const
{
    return m_fGlobalTaxLimit;
}

void GHdmServerConfig::GlobalTaxLimit(const REAL32 in_fGlobalTaxLimit)
{
    m_fGlobalTaxLimit = in_fGlobalTaxLimit;

    for(ENTITY_ID i = 1; i <= g_ServerDAL.NbCountry(); i++)
        g_ServerDCL.ChangeGlobalModTax(i, min(g_ServerDAL.CountryData(i)->GlobalTaxMod(), m_fGlobalTaxLimit));
}

INT32 GHdmServerConfig::GlobalTaxSpecial(EGlobalTaxSpecialType::Enum in_eGlobalTaxSpecial) const
{
    return m_GlobalTaxSpecials.at(in_eGlobalTaxSpecial);
}

REAL64 GHdmServerConfig::GvtTypeProductionModifier(const EGovernmentType::Enum in_iGvtType) const
{
    return m_mGvtTypeProductionModifiers.at(in_iGvtType);
}

REAL64 GHdmServerConfig::IncomeTaxLimit(const EGovernmentType::Enum in_eGovernmentType) const
{
    return m_IncomeTaxLimits.at(in_eGovernmentType);
}

void GHdmServerConfig::IncomeTaxLimit(const EGovernmentType::Enum in_eGovernmentType, const REAL64 in_fIncomeTaxLimit)
{
    m_IncomeTaxLimits[in_eGovernmentType] = in_fIncomeTaxLimit;

    for(ENTITY_ID i = 1; i <= g_ServerDAL.NbCountry(); i++)
    {
        GCountryData* const l_pData = g_ServerDAL.CountryData(i);
        if(l_pData->GvtType() == in_eGovernmentType)
        {
            const REAL64 l_fOldTaxRate = l_pData->PersonalIncomeTax();
            g_ServerDCL.ChangePersonalIncomeTax(i, l_fOldTaxRate, min(l_fOldTaxRate, m_IncomeTaxLimits[in_eGovernmentType]));
        }
    }
}

bool GHdmServerConfig::IncomeTaxRateAffectsGrowth() const
{
    return m_bIncomeTaxRateAffectsGrowth;
}

bool GHdmServerConfig::IncreaseDeathRateForAgingPopulation() const
{
    return m_bIncreaseDeathRateForAgingPopulation;
}

bool GHdmServerConfig::LogBankruptcies() const
{
    return m_bLogBankruptcies;
}

INT32 GHdmServerConfig::MaximumCellsInForeignCountry() const
{
    return m_iMaximumCellsInForeignCountry;
}

void GHdmServerConfig::MaximumCellsInForeignCountry(const INT32 in_iMaxCells)
{
    m_iMaximumCellsInForeignCountry = in_iMaxCells;

    if(m_iMaximumCellsInForeignCountry > 0)
    {
        for(ENTITY_ID i = 1; i <= g_ServerDAL.NbCountry(); i++)
        {
            GCountryData* const l_pData = g_ServerDAL.CountryData(i);

            vector<GCovertActionCell>& l_vCells = l_pData->CovertActionCells();
            if(l_vCells.size() <= static_cast<size_t>(m_iMaximumCellsInForeignCountry))
                continue;

            //\todo Possible cleanup. #130
            map<ENTITY_ID, INT32> l_mCellCount;
            for(auto l_It = l_vCells.begin(); l_It < l_vCells.end(); ++l_It)
            {
                const ENTITY_ID l_iAssignedCountry     = l_It->AssignedCountry();

                if(l_mCellCount.count(l_iAssignedCountry) == 0)
                    l_mCellCount[l_iAssignedCountry] = 1;
                else if(l_mCellCount[l_iAssignedCountry] == m_iMaximumCellsInForeignCountry)
                    g_ServerDCL.ForceCovertActionCellTravel(l_iAssignedCountry, i, *l_It);
                else
                    l_mCellCount[l_iAssignedCountry]++;

                if(l_It->ActualState() == ECovertActionsCellState::InTransit)
                {
                    const ENTITY_ID l_iNextAssignedCountry = l_It->NextAssignedCountry();

                    if(l_mCellCount.count(l_iNextAssignedCountry) == 0)
                        l_mCellCount[l_iNextAssignedCountry] = 1;
                    else if(l_mCellCount[l_iNextAssignedCountry] == m_iMaximumCellsInForeignCountry)
                        g_ServerDCL.ForceCovertActionCellTravel(l_iNextAssignedCountry, i, *l_It);
                    else
                        l_mCellCount[l_iNextAssignedCountry]++;
                }
            }

            l_pData->NationalSecurity(g_ServerDCL.FindNationalSecurity(i));
        }
    }
}

GString GHdmServerConfig::Message() const
{
    return m_sMessage;
}

REAL32 GHdmServerConfig::MilitaryProductionCapacityPercent() const
{
    return m_fMilitaryProductionCapacityPercent;
}

REAL32 GHdmServerConfig::MilitaryUpkeepPercentages(const EUnitCategory::Enum in_eUnitCategory, const ETrainingLevel::Enum in_eTrainingLevel) const
{
    return m_mMilitaryUpkeepPercentages.at(in_eUnitCategory).at(in_eTrainingLevel);
}

bool GHdmServerConfig::NavalRuleEnabled() const
{
    return m_bNavalRuleEnabled;
}

REAL32 GHdmServerConfig::NuclearMissileRangePercentage() const
{
    return m_fNuclearMissileRangePercentage;
}

REAL32 GHdmServerConfig::NuclearUpkeepPercentage() const
{
    return m_fNuclearUpkeepPercentage;
}

REAL32 GHdmServerConfig::OccupiedRegionPercentageForNuclear() const
{
    return m_fOccupiedRegionPercentageForNuclear;
}

GPlayerLimitsConfig GHdmServerConfig::PlayerLimitsConfig() const
{
    return m_PlayerLimitsConfig;
}

REAL32 GHdmServerConfig::ProductionLossOnAnnex() const
{
    return m_fProductionLossOnAnnex;
}

bool GHdmServerConfig::ReinforcedUnitTypesToArenaEdge(const EUnitType::Enum in_eUnitType) const
{
    return m_mReinforcedUnitTypesToArenaEdge.at(in_eUnitType);
}

REAL32 GHdmServerConfig::ResourceTaxLimit() const
{
    return m_fResourceTaxLimit;
}

void GHdmServerConfig::ResourceTaxLimit(const REAL32 in_fResourceTaxLimit)
{
    m_fResourceTaxLimit = in_fResourceTaxLimit;

    for(ENTITY_ID i = 1; i <= g_ServerDAL.NbCountry(); i++)
    {
        GCountryData* const l_pData = g_ServerDAL.CountryData(i);

        for(INT32 j = 0; j < EResources::ItemCount; j++)
        {
            const EResources::Enum l_eResource = static_cast<EResources::Enum>(j);
            l_pData->ResourceTaxes(l_eResource, min(l_pData->ResourceTaxes(l_eResource), m_fResourceTaxLimit));
        }
    }
}

bool GHdmServerConfig::ServerChatsEnabled() const
{
    return m_bServerChatsEnabled;
}

REAL32 GHdmServerConfig::TributePercent() const
{
    return m_fTributePercent;
}

bool GHdmServerConfig::UseNewExportMechanics() const
{
    return m_bUseNewExportMechanics;
}

bool GHdmServerConfig::UseNewResourceTaxFormula() const
{
    return m_bUseNewResourceTaxFormula;
}
