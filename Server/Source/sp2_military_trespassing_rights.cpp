#include "golem_pch.hpp"


using namespace EHEActions;


GMilitaryTrespassingRights* GMilitaryTrespassingRights::Clone() const
{
   return new GMilitaryTrespassingRights( *this ); 
}


ENTITY_ID GMilitaryTrespassingRights::FindTarget() const
{
    //GDZLOG(EDZLogLevel::Entry, L"");
	
	//Get the list of enemies
	set<UINT32> l_Enemies;
	UINT32 l_iNbCountry = (UINT32)g_ServerDAL.NbCountry();
	for(UINT32 i=1; i<= l_iNbCountry; i++)
	{		
		if(m_iSource == i || !g_ServerDAL.CountryIsValid(i))
			continue;
			
		if(g_ServerDAL.RelationBetweenCountries(m_iSource,i) < SP2::c_fRelationsEnnemy)
			l_Enemies.insert(i);		
	}
	 
	if(l_Enemies.size() == 0)
    {
        //GDZLOG(EDZLogLevel::Exit, L"Returning 0");
		return 0;
    }
	
	//Find starting region, for checks
	UINT32 l_iSourceRegion = 0;
	const SCityInfo* l_pCapitalSource = g_ServerDAL.CapitalCity(m_iSource);
	if(l_pCapitalSource)
		l_iSourceRegion = l_pCapitalSource->m_iRegionId;
	else if(g_ServerDAL.CountryPoliticalControl(m_iSource).size())
		l_iSourceRegion = *g_ServerDAL.CountryPoliticalControl(m_iSource).begin();
	else if(g_ServerDAL.CountryMilitaryControl(m_iSource).size())
		l_iSourceRegion = *g_ServerDAL.CountryMilitaryControl(m_iSource).begin();
	else
    {
        //GDZLOG(EDZLogLevel::Exit, L"Returning 0");
		return 0;
    }
	
    ENTITY_ID l_iThirdCountry = 0;
	for(set<UINT32>::iterator it = l_Enemies.begin(); it != l_Enemies.end(); it++)
	{
		//Does this country has access to a region of the enemy?		
		UINT32 l_iDestinationRegion = 0;
		const SCityInfo* l_pCapital = g_ServerDAL.CapitalCity(*it);
		if(l_pCapital)
			l_iDestinationRegion = l_pCapital->m_iRegionId;
		else if(g_ServerDAL.CountryPoliticalControl(*it).size())
			l_iDestinationRegion = *g_ServerDAL.CountryPoliticalControl(*it).begin();
		else if(g_ServerDAL.CountryMilitaryControl(*it).size())
			l_iDestinationRegion = *g_ServerDAL.CountryMilitaryControl(*it).begin();
		else
			continue;

		if(!g_ServerDCL.MoveHostileFuturePossible(m_iSource,l_iSourceRegion,l_iDestinationRegion))
		{
            //Won't have access to enemy. We need to find a way to go to this enemy via third country
            l_iThirdCountry = g_ServerDCL.CountryToPassThroughTo(m_iSource, l_iSourceRegion, l_iDestinationRegion);

            //Do we at least have pending access treaty with third country?
            if(g_ServerDAL.CountriesHavePendingAccessTreaty(m_iSource, l_iThirdCountry))
                l_iThirdCountry = 0; //Yes, no use proposing access again, check another enemy (and hopefully another third country)
            else
                break; //No, propose access to third country
		}
	}

    //GDZLOG(EDZLogLevel::Exit, L"Returning " + GString(l_iThirdCountry));
	return l_iThirdCountry;
}

void GMilitaryTrespassingRights::Execute()
{
	if(m_iTarget && !g_ServerDAL.CountriesHavePendingAccessTreaty(m_iSource, m_iTarget))
	{
		GString l_sNewTreatyName = g_ServerDAL.TreatyName(ETreatyType::MilitaryAccess,g_ServerDAL.GetString(g_ServerDAL.CountryData(m_iSource)->NameID()));
        GDZLOG(EDZLogLevel::Info1, L"AI-controlled " + g_ServerDAL.CountryData(m_iSource)->NameAndIDForLog() + L" offers to " + g_ServerDAL.CountryData(m_iTarget)->NameAndIDForLog());
		UINT32 l_Conditions[ETreatyConditions::ItemCount];
		Memory::Fill<UINT32>(l_Conditions,0,ETreatyConditions::ItemCount);
		set<UINT32> l_SideAMembers;
		set<UINT32> l_SideBMembers;
		set<UINT32> l_SidePressure;
		l_SideBMembers.insert(m_iSource);
		l_SideAMembers.insert(m_iTarget);
		g_ServerDCL.CreateNewTreaty(m_iSource,l_SideAMembers,l_SideBMembers,l_SidePressure,ETreatyType::MilitaryAccess,true,l_sNewTreatyName,l_Conditions);
	}	
}

bool GMilitaryTrespassingRights::CheckHardConditions() const
{	
	set<UINT32> l_Enemies;
	UINT32 l_iNbCountry = (UINT32)g_ServerDAL.NbCountry();
	for(UINT32 i=1; i<= l_iNbCountry; i++)
	{		
		if(m_iSource == i || !g_ServerDAL.CountryIsValid(i))
			continue;
			
		if(g_ServerDAL.RelationBetweenCountries(m_iSource,i) < SP2::c_fRelationsEnnemy)
			l_Enemies.insert(i);		
	}
	 
	if(l_Enemies.size() == 0)
		return false;
	return true;
}

