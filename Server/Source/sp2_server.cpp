/**************************************************************
*
* sp2_server.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Owner
* =====
*  Mathieu Tremblay & Frederic O'Reilly
*
* Copyright  (C) 2004, Laboratoires Golemlabs Inc.
**************************************************************/
#include "golem_pch.hpp"

#include "../../includes/common_lib//sp2_event_history_markers.h"

#pragma warning (disable:4407)

GServer::GServer() :
m_DAL(),
m_DCL(&m_DAL),
m_WorldBehavior(),
m_bFirstAIIteration(true),
m_iIteratedAICount(0),
m_fTimeOfLastAutosave(0),
m_LastHistoricalEventsDate(),
m_fLastEntitySaveTime(0.0)
{
   GDZLOG(EDZLogLevel::Entry,L"");

   g_Joshua.FileManager()->File(L"\\data\\StringTable.english.gst",m_StrTableFile);
   m_StrTable.Open(m_StrTableFile);
   m_fPreviousSyncDataUpdate = -1;
   m_bGameStarted    = false; //!<When the game is really started, Iterate
   m_bGameLoaded     = false;
   m_fHalfSecondClock = -1;
   m_fFiveSecondsClock  = -1;
	m_iMilitaryRankCountryToIterate = 1;
   m_fNextCombatIterationTime = 0.0f;
   m_iNuclearMissilesSubmarineCountryToReload = 1;	

	m_fAIAggressiveness = 0.5f;
   m_bPendingLoad = false;
   m_bPendingNew = false;
   m_bUpdateStaticOnly = false;

   GDZLOG(EDZLogLevel::Exit,L"");
}

GServer::~GServer()
{
   GDZLOG(EDZLogLevel::Entry,L"");
   m_StrTable.Close();
   GDZLOG(EDZLogLevel::Exit,L"");
}


//! Initialize the SP2 Server
/*!
 *  Initializes the server.  Called by GJoshua::Init_As_Server
 *    This function does:
 *       - Register game event handlers
 *       - Initialize the database
 *       - Create the country List
 *       - Create the Active player list (as AI players)
 *
 * @return SDK::GAME_MSG: A Status message, SDK::GAME_OK if no error, != SDK::GAME_OK otherwise (@see game_process_itf.h)
 **/
SDK::GAME_MSG GServer::Initialize()
{
   GDZLOG(EDZLogLevel::Entry, L"");

   GString  l_sTemp;
   GString  sCombatIterationPeriod;
	GString  l_sAIAggressiveness;

   g_Joshua.GameTimeSpeedSynchroEnable(true, 1);

   // Initialize Mode
   // JMercier : This will be changed later by reading a xml file or a menu.
   ModName(GHdmRelease::c_sName);

   GDALInterface::Instance = &m_DAL;
   GDCLInterface::Instance = &m_DCL;
   m_DCL.RegisterCreateCountryDataMethod(GCountryData::New);

   // load our sp2 specifics game options
   LoadGameOptions();

   // Game event registration
   {
      REGISTER_GAME_EVENT(SP2::Event::GNotifyClient,              NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GGetRegionsCharacteristicResult, NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GStartGame,                 NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GRandomEvent,					NULL, NULL);
      REGISTER_GAME_EVENT_AI(SP2::Event::GStrategicWarfareNotify,    
         NULL, 
         NULL,
         &GAIGeneralEventHandler::HandleNukeLaunchNotify,
         &m_AIGeneralEventHandler);
      REGISTER_GAME_EVENT_AI(SP2::Event::GWarDeclarationNotify,    
         NULL, 
         NULL,
         &GAIGeneralEventHandler::HandleWarDeclaration,
         &m_AIGeneralEventHandler);	
      REGISTER_GAME_EVENT_AI(SP2::Event::GTreatyOffer,    
         NULL, 
         NULL,
         &GAIGeneralEventHandler::HandleTreatyOffer,
         &m_AIGeneralEventHandler);
      REGISTER_GAME_EVENT_AI(SP2::Event::GTreatyJoin,    
         &GTreatyEventHandler::HandleTreatyJoin, 
         &m_TreatyGameEventHandler,
         &GAIGeneralEventHandler::HandleTreatyJoin,
         &m_AIGeneralEventHandler);
      REGISTER_GAME_EVENT_AI(SP2::Event::GTreatyLeave,    
         &GTreatyEventHandler::HandleTreatyLeave, 
         &m_TreatyGameEventHandler,
         &GAIGeneralEventHandler::HandleTreatyLeave,
         &m_AIGeneralEventHandler);
      REGISTER_GAME_EVENT_AI(SP2::Event::GTreatyPeaceSigned,    
         NULL, 
         NULL,
         &GAIGeneralEventHandler::HandleTreatyPeaceSigned,
         &m_AIGeneralEventHandler);	

      REGISTER_GAME_EVENT(SP2::Event::GSendData,                  NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GReceiveCountryList,        NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GReceiveCountryStaticData,  NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GErrorCountryStaticData,    NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GUpdateData,                NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GUpdateTreaty,              NULL, NULL);
	   REGISTER_GAME_EVENT(SP2::Event::GUpdateWar,						NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GUpdateUnitValue,           NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GRefuseMilitaryAction,      NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GConquerCountry,            NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GSendNews,		            NULL, NULL);	
      REGISTER_GAME_EVENT(SP2::Event::GHistoryMarkers,		      NULL, NULL);	
      REGISTER_GAME_EVENT(SP2::Event::GCombatOver,                NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GSynchronizeCountryData,    NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GHdmSynchronizeCountryDataEvent, nullptr, nullptr);
      REGISTER_GAME_EVENT(SP2::Event::GSendCovertOpsMissionDone,  NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GBombardmentNotification,   NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GSendPlan,		            &SP2::GAdvisorEventHandler::HandleAdvisorAnswer, &m_AdvisorEventHandler);

      REGISTER_GAME_EVENT(SP2::Event::GStartGameRequest,          &SP2::GGeneralEventHandler::HandleStartGameRequest,          &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GSendAvailableCountries,    &SP2::GGeneralEventHandler::HandleRequestAvailableCountries, &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GGetRegionsCharacteristic,  &SP2::GGeneralEventHandler::HandleGetRegionCharacteristic,   &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GSetPlayerInfo,             &SP2::GGeneralEventHandler::HandleSetPlayerInfo,             &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GHdmSetPlayerInfo,          &SP2::GGeneralEventHandler::HandleSetPlayerInfo,             &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GGetPlayersList,            &SP2::GGeneralEventHandler::HandleGetPlayersList,            &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GGetCountryRanks,           &SP2::GGeneralEventHandler::HandleGetCountryRanks,           &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GGetCountriesRanks,         &SP2::GGeneralEventHandler::HandleGetCountriesRanks,         &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GGameOver,                  &SP2::GGeneralEventHandler::HandleGameOverAnswer,            &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GObjectiveCompleted,        NULL,                                                       &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GSetGameObjectives,         &SP2::GGeneralEventHandler::HandleSetObjectives,             &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GChangeGameSpeed,           &SP2::GGeneralEventHandler::HandleChangeGameSpeed,           &m_EventHandler);
		REGISTER_GAME_EVENT(SP2::Event::GChangeAIAggressiveness,    &SP2::GGeneralEventHandler::HandleChangeAIAggressiveness,    &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GRelationHistoryFetch,      &SP2::GGeneralEventHandler::HandleRelationHistoryFetch,      &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GRequestCountryList,        &SP2::GGeneralEventHandler::HandleRequestCountryList,        &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GRequestCountryStaticData,  &SP2::GGeneralEventHandler::HandleRequestCountryStaticData,  &m_EventHandler);
		REGISTER_GAME_EVENT(SP2::Event::GEventFetchCountriesRelations, &SP2::GGeneralEventHandler::HandleFetchCountriesRelations,&m_EventHandler);
		
		REGISTER_GAME_EVENT(SP2::Event::GEventGetRegionsValues,		&SP2::GEconomicEventHandler::HandleGetRegionValues,				 &m_EconomicGameEventHandler);      
      REGISTER_GAME_EVENT(SP2::Event::GEventGetTechnologiesValues,&SP2::GEconomicEventHandler::HandleGetTechnologiesValues,     &m_EconomicGameEventHandler);
		REGISTER_GAME_EVENT(SP2::Event::GEventTradeProposal,        &SP2::GEconomicEventHandler::HandleTradeOffer,			       &m_EconomicGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GEconomicUpdate,            &SP2::GEconomicEventHandler::HandleEconomicWindowUpdate,      &m_EconomicGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GEconomicHealthUpdate,      &SP2::GEconomicEventHandler::HandleUpdateEconomicHealth,      &m_EconomicGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GResourcesUpdate,           &SP2::GEconomicEventHandler::HandleUpdateResources,           &m_EconomicGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GIncreaseProduction,        &SP2::GEconomicEventHandler::HandleIncreaseProduction,        &m_EconomicGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GBudgetUpdate,              &SP2::GEconomicEventHandler::HandleUpdateBudget,              &m_EconomicGameEventHandler);
	   REGISTER_GAME_EVENT(SP2::Event::GEventUpdateWarState,			&SP2::GMilitaryEventHandler::HandleUpdateWarState,				 &m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GPerformOffshoreBombardment,&SP2::GMilitaryEventHandler::HandlePerformOffshoreBombardment,&m_MilitaryGameEventHandler);
      

      REGISTER_GAME_EVENT(SP2::Event::GStrategicWarfareLaunch,    &SP2::GMilitaryEventHandler::HandleStrategicWarfareLaunch,   &m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GRequestTroopsMove,         &SP2::GMilitaryEventHandler::HandleMoveUnit,                 &m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GRequestMilitaryStatusChange,&SP2::GMilitaryEventHandler::HandleStatusChange,            &m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GAddUnitDesign,             &SP2::GMilitaryEventHandler::HandleAddUnitDesign,            &m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GUnitResearchUpdate,        &SP2::GMilitaryEventHandler::HandleUnitResearchUpdateRequest,&m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GBattleOverviewUnitActionUpdate, &SP2::GMilitaryEventHandler::HandleBattleOverviewUnitActionUpdate, &m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GOneClickInvade,				&SP2::GMilitaryEventHandler::HandleOneClickInvade,				&m_MilitaryGameEventHandler);
      

      REGISTER_GAME_EVENT(SP2::Event::GPoliticUpdate,             &SP2::GPoliticEventHandler::HandlePoliticWindowUpdate,       &m_PoliticGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GInternalLawsUpdate,        &SP2::GPoliticEventHandler::HandleUpdateInternalLaws,        &m_PoliticGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GConstitutionalFormUpdate,  &SP2::GPoliticEventHandler::HandleUpdateConstitutionalForm,  &m_PoliticGameEventHandler);
		REGISTER_GAME_EVENT(SP2::Event::GEventTreatyRefusalDetails, NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GMilitaryMainUpdate,        &SP2::GMilitaryEventHandler::HandleMilitaryMainUpdateRequest,&m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GUpdateAdvisorSettings,     &SP2::GGeneralEventHandler::HandleUpdateAdvisorSettings,     &m_EventHandler);

      // Register event for UnitProductor
      REGISTER_GAME_EVENT(SP2::Event::GBuildUnitOrder,            &SP2::GUnitProductionEventHandler::HandleBuildUnitOrder,     &m_UnitProductionGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GChangeUnitOrder,           &SP2::GUnitProductionEventHandler::HandleChangeOrder,        &m_UnitProductionGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GRequestProductionQueue,    &SP2::GUnitProductionEventHandler::HandleRequestProductionQueue,&m_UnitProductionGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GDeployUnits,               &SP2::GUnitProductionEventHandler::HandleDeployUnits,        &m_UnitProductionGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GSplitUnits,                &SP2::GUnitProductionEventHandler::HandleSplitUnits,         &m_UnitProductionGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GCancelDeployement,         &SP2::GUnitProductionEventHandler::HandleCancelDeployement,  &m_UnitProductionGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GUpdateUnitOrderStatus,     NULL,                                                       NULL);
      REGISTER_GAME_EVENT(SP2::Event::GUpdateForeignBuildingCountryList,NULL,                                                 NULL);
      REGISTER_GAME_EVENT(SP2::Event::GRequestForeignBuildingCountryList, &SP2::GUnitProductionEventHandler::HandleRequestBuildingCountryList, &m_UnitProductionGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GRegisterAsBuildingCountry, &SP2::GUnitProductionEventHandler::HandleGRegisterAsBuildingCountry, &m_UnitProductionGameEventHandler);

      // Register event for Unit Training
      REGISTER_GAME_EVENT(SP2::Event::GTrainUnitRequest, &SP2::GUnitProductionEventHandler::HandleTrainUnitRequest, &m_UnitProductionGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GDisbandUnitRequest, &SP2::GUnitProductionEventHandler::HandleDisbandUnitRequest, &m_UnitProductionGameEventHandler);

      // Register event for unit market
      REGISTER_GAME_EVENT(SP2::Event::GRequestForSaleUnitList,&SP2::GUnitProductionEventHandler::HandleRequestForSaleUnitList,&m_UnitProductionGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GUpdateForSaleUnitList,NULL,NULL);
      REGISTER_GAME_EVENT(SP2::Event::GTransfertUnit,&SP2::GUnitProductionEventHandler::HandleUnitTransfert,&m_UnitProductionGameEventHandler);


      REGISTER_GAME_EVENT(SP2::Event::GTreatyDetailsTreatyAdd,    &SP2::GPoliticEventHandler::HandleTreatyAdd,     &m_PoliticGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GRequestTreatyConditionsCountries, &SP2::GTreatyEventHandler::HandleConditionsCountries, &m_TreatyGameEventHandler);

      REGISTER_GAME_EVENT(SP2::Event::GEventCountryInfo,          &SP2::GGeneralEventHandler::HandleCountryInfo,    &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GHdmEventCountryInfo,       &SP2::GGeneralEventHandler::HandleCountryInfo,    &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GEventCellCreation,         &SP2::GMilitaryEventHandler::HandleCellCreate,    &m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GEventCellUpdateStatus,     &SP2::GMilitaryEventHandler::HandleCellUpdate,    &m_MilitaryGameEventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GEventCellNewMission,       &SP2::GMilitaryEventHandler::HandleNewMission,    &m_MilitaryGameEventHandler);

      REGISTER_GAME_EVENT(SP2::Event::GGetCountryParties,         &SP2::GGeneralEventHandler::HandleGetCountryParties, &m_EventHandler);

      //! register acknowldgement event
      REGISTER_GAME_EVENT(SP2::Event::GAnswerAckGameEvent, NULL, NULL);      

      REGISTER_GAME_EVENT(SP2::Event::GAcknowledge, NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GProgress,    NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GResetGame,   &SP2::GGeneralEventHandler::HandleResetGame,   &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GNewGame,     &SP2::GGeneralEventHandler::HandleNewGame,     &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GNewComplete, NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GLoadGame,    &SP2::GGeneralEventHandler::HandleLoadGame,    &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GLoadComplete,NULL, NULL);
      REGISTER_GAME_EVENT(SP2::Event::GSaveGame,    &SP2::GGeneralEventHandler::HandleSaveGame,    &m_EventHandler);
      REGISTER_GAME_EVENT(SP2::Event::GSaveComplete,NULL, NULL);

      REGISTER_GAME_EVENT(SP2::Event::GEventGroupSplit, &SP2::GUnitProductionEventHandler::HandleGroupSplit, &m_UnitProductionGameEventHandler);
   }

   m_DAL.DataManager()->RegisterNode(this);
   IF_RETURN(!m_DAL.Initialize(SP2::SERVER_CONFIG),SDK::GAME_INIT_ERROR);
   IF_RETURN(!m_DAL.LoadServerConfigGameplayConstants(),SDK::GAME_INIT_ERROR);

   m_DAL.MapEngineInit(true);

   // set the combat iteration period
   if (m_DAL.XMLValue(SP2::COMBAT_ITERATION_PERIOD,sCombatIterationPeriod))
   {
      m_fCombatIterationPeriod = sCombatIterationPeriod.ToREAL64();        
   }
   else
   {
      g_Joshua.Log( L"Could not set AI combat iteration period", MSGTYPE_FATAL_ERROR );
      GDZLOG( EDZLogLevel::Exit, L"Returning SDK::GAME_INIT_ERROR (" + GString( SDK::GAME_INIT_ERROR ) + L")" );
      return SDK::GAME_INIT_ERROR;
   }	 

   // Setup the AI
   if ( ! InitAI() )
   {
      g_Joshua.Log( L"Couldn't Initialize the AI", MSGTYPE_FATAL_ERROR );
      GDZLOG( EDZLogLevel::Exit, L"Returning SDK::GAME_INIT_ERROR (" + GString( SDK::GAME_INIT_ERROR ) + L")" );
      return SDK::GAME_INIT_ERROR;
   }

   // World Behavior Initialisation
   m_WorldBehavior.Reset();
   IF_RETURN_LOG(!m_WorldBehavior.LoadConfigFile(L"behavior.xml", L""),SDK::GAME_INIT_ERROR,"Couldnt load behavior file");

   {
        m_sPlaintextPassword = g_Joshua.Password();

        // Load the HDM config file now, once countries are initialized
        const bool l_bHdmServerConfigLoaded = m_HdmServerConfig.LoadConfigFile();
        if(!l_bHdmServerConfigLoaded)
            g_Joshua.Log( L"Couldn't load HDM server config file", MSGTYPE_WARNING );
   }

   // Initialize the DCL (must be done before loading military data, since it uses the unit mover)
   m_DCL.Initialize();

   //Initialize the SDK::Combat Engine
   if(!CombatEngineInit())
   {
      g_Joshua.Log(L"Couldnt Initialize the SDK::Combat engine",MSGTYPE_FATAL_ERROR);
      GDZLOG(EDZLogLevel::Exit, L"Returning SDK::GAME_INIT_ERROR (" + GString(SDK::GAME_INIT_ERROR) + L")");
      return SDK::GAME_INIT_ERROR;
   }

   //Register the Unit constructors to the unit manager
   g_Joshua.UnitManager().RegisterUnitCreationFunc(SP2::GUnit::New);
   //Register the Unit Group constructors to the unit manager
   g_Joshua.UnitManager().RegisterUnitGroupCreationFunc(SP2::GUnitGroupEx::New);
   //Register the Unit Design constructors to the unit manager
   g_Joshua.UnitManager().RegisterUnitDesignCreationFunc(SP2::GUnitDesign::New);
   //Register the arena information construction functoin
   g_Joshua.UnitManager().RegisterArenaInfoCreationFunc(SP2::GArenaInfo::New);
   //Register Arena creation function
   g_CombatManager.RegisterArenaCreationFunc(SP2::GArena::New);

   //Attach the observer to the unit manager
   g_Joshua.UnitManager().Attach(&m_MilitaryGameEventHandler, SDK::Combat::c_notificationBeforeUnitKilled);

   IF_RETURN_LOG(!m_DAL.LoadUnitTypes(), SDK::GAME_INIT_ERROR, "Unable to load unit designs");

   m_ConsoleCommandHandler.Initialize();

   //Init the military plans vector
   g_CombatPlanner.Init();

   // Set the game speed as paused by default
   g_Joshua.GameSpeed(0);

   m_DAL.ResetUpdateData();
   m_DAL.ResetUpdateUnitValue();

   // Init some memory
   g_ServerDCL.InitCacheMemory();

   // Initialize the default game objectives
   {
      m_WorldBehavior.Attach(&g_ServerDCL.GameObjectiveEvaluator(), c_iNotificationLeadingPoliticalPartyChangeAfterElections);
      m_DCL          .Attach(&g_ServerDCL.GameObjectiveEvaluator(), c_iNotificationOnSuccessfulCoupEtat);
      m_DCL          .Attach(&g_ServerDCL.GameObjectiveEvaluator(), c_iNotificationOnNukeLaunched);
      m_DCL          .Attach(&g_ServerDCL.GameObjectiveEvaluator(), c_iWorldPeaceStatusChanged);
   }


   // Automatically start a new game on a dedicated server
   if(!g_Joshua.Dependant() )
   {
      m_sNewGameDB = c_sDatabaseLocation;
      m_iNewsource = 0;

      NewGame();
   }

   GDZLOG(EDZLogLevel::Exit, L"Returning SDK::GAME_OK (" + GString(SDK::GAME_OK) + L")");
   return SDK::GAME_OK;
}
 
/*!
* Load our mod command line parameters
**/
void GServer::LoadGameOptions()
{
   // Parse the joshua command line for extra parameters
   if(wcsstr(g_Joshua.ModCommandLine().c_str(), L"+nonuke"))
      m_DAL.GameOptions().NuclearAllowed(false);
   else
      m_DAL.GameOptions().NuclearAllowed(true);
}

//! Iterate as the server
/*!
* Iterate function of the server, called every iteration by the GJoshua engine.
**/
SDK::GAME_MSG GServer::Iterate(void* param)
{
   bool l_bUpdateGameTime = false;
   m_fLastSpeed = g_Joshua.GameSpeed();
   while(!m_vPendingSaves.empty() )
   {
      g_Joshua.GameSpeed(0);
      SaveGame(m_vPendingSaves.front() );

      m_vPendingSaves.pop_front();
      l_bUpdateGameTime = true;
   }

   if(l_bUpdateGameTime)
   {
      g_Joshua.GameSpeed(m_fLastSpeed);
      g_Joshua.GameTime(g_Joshua.GameTime() );
   }

   if(m_bPendingNew)
   {
      NewGame();
      m_bPendingNew = false;
   }
   else if(m_bPendingLoad)
   {
      LoadGame();
      m_bPendingLoad = false;
   }

   if(!GameStarted() )
   {
      return SDK::GAME_OK;
   }

   // Update all unit values
   g_ServerDAL.UpdateCountryUnitAndMissilesValues();

   m_DCL.UnitMover().Iterate();	 

   g_CombatManager.Iterate(g_Joshua.GameTime() );
   if (g_Joshua.Clock() >= m_fNextCombatIterationTime)
   {		
		if(m_iMilitaryRankCountryToIterate == 1)
		{
			//redo the ranks
			m_OrderCountriesByMilitaryRank.clear();
			for(UINT32 i=1; i<= g_ServerDAL.NbCountry() ; i++)
			{
				if(!g_ServerDAL.CountryIsValid(i))
					continue;
				m_OrderCountriesByMilitaryRank.insert(make_pair<INT32,UINT32>(g_ServerDAL.CountryData(i)->MilitaryRank(),i));
				m_IteratorMilitaryRank = m_OrderCountriesByMilitaryRank.begin();
			}
		}
      m_fNextCombatIterationTime = g_Joshua.Clock() + m_fCombatIterationPeriod;
      g_CombatPlanner.Iterate( m_IteratorMilitaryRank->second );
		m_IteratorMilitaryRank++;
		m_iMilitaryRankCountryToIterate++;
		if(m_iMilitaryRankCountryToIterate > m_OrderCountriesByMilitaryRank.size() )
		{
			m_iMilitaryRankCountryToIterate = 1;

		}
   }

   // Re-Update all unit values
   g_ServerDAL.UpdateCountryUnitAndMissilesValues();

   m_DCL.IterateCountryControl();

   m_WorldBehavior.GlobalIterate(g_Joshua.GameTime() );

   // Update region, city & capital data, if needed
   m_DAL.SendUpdateData();

   // Update historical markers
   m_DAL.SendHistory();

   //Perform every 1.5 seconds
   REAL64 l_fActualClock = g_Joshua.Clock();
   if((l_fActualClock - m_fPreviousSyncDataUpdate) >= 1.5f)
   {
      SyncronizeMilitary(SDK::Event::ESpecialTargets::BroadcastHumanPlayers,l_fActualClock);
      m_DAL.SendUpdateUnitValue();

      //Synchronize the country data of each human player
      {
         for(SDK::GPlayers::const_iterator l_HumanPlayersIt = g_Joshua.HumanPlayers().begin() ;
            l_HumanPlayersIt != g_Joshua.HumanPlayers().end();
            l_HumanPlayersIt++)
         {
            if(l_HumanPlayersIt->second->PlayerStatus() ==  SDK::PLAYER_STATUS_ACTIVE)
                g_SP2Server->SynchronizePlayerCountryData(*(l_HumanPlayersIt->second), false);
         }
      }//end of synchronize country data


      //Perform offshore bombardments (every 1.5 seconds)
      {
         m_DCL.UnitMover().PerformOffShoreBombardment();
      }

   }

   //Iterate every 0.5 secs
   if((g_Joshua.Clock() - m_fHalfSecondClock) >= 0.5f)
   {
		m_DAL.SyncronizeTreaties();
      m_DCL.GameObjectiveEvaluator().EvaluateObjectives();
      m_DCL.IterateCovertActionCells();
      m_DAL.UpdateRelationsHistory();		
		m_DAL.IterateRegionSynchronization();

      m_DCL.IterateUnitProduction();

      m_DCL.IterateNukes();      
      m_fHalfSecondClock= g_Joshua.Clock();

      //Cycle the countries to reload the nuclear submarines nuclear missiles
      {
         g_ServerDAL.CountryData(m_iNuclearMissilesSubmarineCountryToReload++)->ReloadSubmarinesNuclearMissiles();
         if(m_iNuclearMissilesSubmarineCountryToReload > g_ServerDAL.NbCountry())
         {
            m_iNuclearMissilesSubmarineCountryToReload = 1;
         }
      }
   }

   //Iterate every 5 seconds
   if((g_Joshua.Clock() - m_fFiveSecondsClock) >= 5.f)
   {
      m_fFiveSecondsClock = g_Joshua.Clock();
      m_DCL.IterateResearch(g_Joshua.GameTime() );
   }

   // Update all unit values
   g_ServerDAL.UpdateCountryUnitAndMissilesValues();

   // Iterate autosave
   const REAL32 l_fDedicatedServerAutosavePeriod = m_HdmServerConfig.DedicatedServerAutosavePeriod();
   if((l_fDedicatedServerAutosavePeriod > 0) && ((g_Joshua.Clock() - m_fTimeOfLastAutosave) >= (l_fDedicatedServerAutosavePeriod * 60.f)))
   {
       m_fTimeOfLastAutosave = g_Joshua.Clock();
       
       const bool l_bDedicatedServerAutosaveToJoshuaFolder = m_HdmServerConfig.DedicatedServerAutosaveToJoshuaFolder();
       if(!l_bDedicatedServerAutosaveToJoshuaFolder)
           m_DCL.CreateSaveDirectory();

       GSaveRequest l_Request;
       const GString& l_sAutosaveName = L"Autosave";
       l_Request.m_iSourceID = 0;
       l_Request.m_sSaveName = l_sAutosaveName;
       l_Request.m_sSaveFile = (l_bDedicatedServerAutosaveToJoshuaFolder ? L"" : (g_Joshua.CurrentMod().m_sPath + c_sSaveGameLocation)) +
                               l_sAutosaveName + c_sSaveExtServer;
       m_vPendingSaves.push_back(l_Request);
   }

   // Query and execute historical events each game day
   if((m_DAL.ActualDate() - m_LastHistoricalEventsDate).Days() >= 1.0)
   {
      const GDateTime l_CurrentDate = m_DAL.ActualDate();
      GDZLOG(EDZLogLevel::Info2, L"Iterating events by date " + l_CurrentDate.ToString(GDateTime::etfYYYYMMDD));
      auto l_DateEventItPair = m_DAL.m_HistoricalEvents.GetEventsWithinDates(m_LastHistoricalEventsDate, l_CurrentDate);
      for(auto l_DateEventIt = l_DateEventItPair.first; l_DateEventIt != l_DateEventItPair.second; ++l_DateEventIt)
      {
         const GDateTime& l_DateTime = l_DateEventIt->first;
         const GStringVec l_vEvents = l_DateEventIt->second;
         for(auto l_EventIt = l_vEvents.cbegin(); l_EventIt != l_vEvents.cend(); ++l_EventIt)
         {
            const GString l_sOutput = m_ConsoleCommandHandler.HandleCommand(*l_EventIt);
            g_Joshua.Log(l_DateTime.ToString(GDateTime::etfYYYYMMDD) + L": " + *l_EventIt + L": " + l_sOutput, MSGTYPE_MSG);
         }
      }
      m_LastHistoricalEventsDate = l_CurrentDate;
   }

   const REAL64 l_fEntitySavePeriod = m_HdmServerConfig.EntitySavePeriod();
   if((l_fEntitySavePeriod > 0.0) && ((g_Joshua.Clock() - m_fLastEntitySaveTime) >= (l_fEntitySavePeriod * 60.0)))
   {
      m_fLastEntitySaveTime = g_Joshua.Clock();
      SaveEntities();
   }

   return SDK::GAME_OK;
}


//! Shutdown the game server
/*!
* Shutdown the game server, the database, deletes the event handler, clears the country list
* etc.
* @return SDK::GAME_MSG: SDK::GAME_OK if no error, != SDK::GAME_OK otherwise
**/
SDK::GAME_MSG GServer::Shutdown()
{
   GDZLOG(EDZLogLevel::Entry, L"");

   if(!SaveEntities())
   {
      g_Joshua.Log( L"Could not save entities into XML file", MSGTYPE_FATAL_ERROR );
      GDZLOG( EDZLogLevel::Exit, L"Returning SDK::GAME_FATAL_ERROR (" + GString( SDK::GAME_FATAL_ERROR ) + L")" );
      return SDK::GAME_FATAL_ERROR;
   }

   //Shutdown the DAL
   m_DAL.Shutdown();

   //Clear the country list
   m_Countries.clear();

   m_WorldBehavior.Reset();

   //Shutdown the SDK::Combat engine
   CombatEngineShutdown();

   //Detach
   m_WorldBehavior.Detach(&g_ServerDCL.GameObjectiveEvaluator());

   GDZLOG(EDZLogLevel::Exit, L"Returning SDK::GAME_OK (" + GString(SDK::GAME_OK) + L")");
   return SDK::GAME_OK;
}


void GServer::SendCountryList(INT32 in_PlayerDestinationId)
{
   GDZLOG(EDZLogLevel::Entry, L"in_PlayerDestinationId = " + GDZDebug::FormatHex(in_PlayerDestinationId));

   //HACK - PROBLEM - (jmercier)
   //This does not send the list of available countries, but the list of unavailable countries.
   //Total opposite

   SDK::GGameEventSPtr l_pEvent = CREATE_GAME_EVENT(SP2::Event::GSendAvailableCountries);
   Event::GSendAvailableCountries* l_pAvailableCountryEvent = (Event::GSendAvailableCountries*)l_pEvent.get();
   l_pEvent->m_iTarget = in_PlayerDestinationId;
   l_pEvent->m_iSource = SDK::Event::ESpecialTargets::Server;

   vector<UINT16> l_vAvailableCountries;

   // Iterate over player and send choosen(taken) country
   for (SDK::GPlayers::const_iterator itr =  g_Joshua.HumanPlayers().begin(); itr != g_Joshua.HumanPlayers().end(); itr++)
   {
      // If the player is active(playing) or ready to play, thus country, is unavailable.
      SDK::GPlayer *l_pPlayer = itr->second;
      if(l_pPlayer->PlayerStatus() == SDK::PLAYER_STATUS_READY || 
         l_pPlayer->PlayerStatus() == SDK::PLAYER_STATUS_ACTIVE)
      {
         UINT16 l_iChosenCountry = (UINT16)l_pPlayer->ModID();
         l_vAvailableCountries.push_back(l_iChosenCountry);
      }
   }

   //Add the Inactive countries (most likely dead - annexed)
   for(UINT16 i = 1 ; i <= g_ServerDAL.NbCountry() ; i++)
   {
      if(!g_ServerDAL.CountryData(i)->Activated())
      {
#ifdef GOLEM_DEBUG
         vector<UINT16>::iterator l_AvCountryIt = find(l_vAvailableCountries.begin(),l_vAvailableCountries.end(),i);
         gassert(l_AvCountryIt == l_vAvailableCountries.end(),"Inactive country should not be controlled by a human player,GServer::SendCountryList ");
#endif

         l_vAvailableCountries.push_back(i);
      }
   }





   // Set country list in event
   l_pAvailableCountryEvent->AvailableCountries(l_vAvailableCountries);

   // Fire Event
   g_Joshua.RaiseEvent(l_pEvent);

   GDZLOG(EDZLogLevel::Exit, L"");
}

void GServer::SendPlayersList(INT32 in_PlayerDestinationId)
{
   GDZLOG(EDZLogLevel::Entry, L"in_PlayerDestinationId = " + GDZDebug::FormatHex(in_PlayerDestinationId));

   if (in_PlayerDestinationId == 0)
      return;

   SDK::GGameEventSPtr l_Event = CREATE_GAME_EVENT(SP2::Event::GGetPlayersList);
   Event::GGetPlayersList & gpl = *(Event::GGetPlayersList *)l_Event.get();

   gpl.m_iSource = SDK::Event::ESpecialTargets::Server;
   gpl.m_iTarget = in_PlayerDestinationId;

   gpl.AdminPlayerID(g_Joshua.AdminPlayerID());

   if (in_PlayerDestinationId == SDK::Event::ESpecialTargets::BroadcastHumanPlayers)
      g_Joshua.Log(L"Broadcasting Players List");
   else
      g_Joshua.Log(L"Sending Players List to client");


   for (SDK::GPlayers::const_iterator itr =  g_Joshua.HumanPlayers().begin(); itr != g_Joshua.HumanPlayers().end(); itr++)
   {
      Event::GGetPlayersList::Player player;
      SDK::GPlayer *l_pPlayer = itr->second;

      player.ClientID         = l_pPlayer->Id();
      player.PlayerName       = l_pPlayer->Name();
      player.CountryID        = l_pPlayer->ModID();
      player.PlayerStatus     = l_pPlayer->PlayerStatus();
      GDZLOG(EDZLogLevel::Info1, L"Player: Client ID = " + GString(player.ClientID) + L", name = " + player.PlayerName + L", country ID = " + GString(player.CountryID) + L", status = " + GString(player.PlayerStatus));

      gpl.PlayersList.push_back(player);
   }

   GDZLOG(EDZLogLevel::Exit, L"");
   g_Joshua.RaiseEvent(l_Event);
}

//!Function called when a player has joined
/*!
* Called by the Joshua engine when a new player has just joined the
* game.  At the moment this function is called, the new player is already
* in the IDLE player list, has an Id assigned.
**/
void GServer::OnNewPlayerJoined(SDK::GPlayer* in_pPlayer)
{
   g_ServerDAL.AddHumanPlayer(in_pPlayer);
}

/*!
* Handle disconnections
**/
void GServer::OnPlayerDisconnect(SDK::GPlayer* in_pPlayer)
{
	g_Joshua.Log("GServer::OnPlayerDisconnect");
   //Accept all pending plans, because an AI would accept it
	g_Joshua.Log("Player Disconnected: " + in_pPlayer->Name());
	if (in_pPlayer->ModID() != -1)
	{
		g_Joshua.Log("Player Remove Advisor: " + in_pPlayer->Name());
		g_ServerDAL.CountryData(in_pPlayer->ModID())->RemoveAdvisor();
	}
   // a player as left
   m_mPlayerData.erase(in_pPlayer->Id());
   InformPlayerLeft(in_pPlayer);

   // A new player list was generated, send it
   SendPlayersList();

   return;
}

SDK::GStringTable* GServer::StrTable()
{
   return &m_StrTable;
}

/*!
* Get a vector containing the IDs of the human players
* @return vector<INT32>: A constant reference to the vector of human player ids
**//*
const vector<UINT32>& GServer::HumanPlayers()
{
return m_vHumanPlayersID;
}
*/
/*!
* Initialize the AI (register actions, objectives and entities)
* @return true if successfully initialized, false otherwise
**/
bool GServer::InitAI()
{
   GDZLOG(EDZLogLevel::Entry, L"");

   // register objectives
   RegisterObjectives();

   // register actions
   RegisterActions();

   GManager::RegisterPlanCreationFunc(SP2::GPlan::New);

   // load entities from xml file
   GString     sFileName;
   GString     sTmpFileName;
   GFile       File;
   if ( ! m_DAL.XMLValue( ENTITIES_FILE, sFileName )  ||
      ! m_DAL.File( sFileName, File )               ||
      ! File.Extract( sTmpFileName )                ||
      ! GManager::LoadEntities( sTmpFileName ) )
   {
      g_Joshua.Log( L"Could not load entities into EHE", MSGTYPE_FATAL_ERROR );
      GDZLOG( EDZLogLevel::Exit, L"Returning false" );
      return false;
   }
   File.Unextract();  // destroy the temporary file

   // set iteration period to 0 at first
   // after iterating each AI once, AI class will set actual period from config
   m_bFirstAIIteration = true;
   g_Joshua.AIIterationPeriod( 0.0f );

   GManager::RegisterStartIterationHandler((GEventHandler*)this,(EHE_ITERATION_FUNC_PTR)&SP2::GAI::ExecuteNewAIIteration);
   GManager::RegisterEndIterationHandler((GEventHandler*)this,(EHE_ITERATION_FUNC_PTR)&SP2::GAI::ExecuteFinishAIIteration);

   GDZLOG(EDZLogLevel::Exit, L"Returning true");
   return true;
}

/*!
* Register objectives in EHE
**/
void GServer::RegisterObjectives()
{
   GManager::RegisterObjective( EHEObjectives::Ids::POPULATION_SUPPORT,
      EHEObjectives::Names::POPULATION_SUPPORT,
      false,
      EHEObjectives::GPopulationSupport() );

   GManager::RegisterObjective( EHEObjectives::Ids::HAVE_SPACE,
      EHEObjectives::Names::HAVE_SPACE,
      false,
      EHEObjectives::GHaveSpace() );

   GManager::RegisterObjective( EHEObjectives::Ids::GOVERNMENT_STABILITY,
      EHEObjectives::Names::GOVERNMENT_STABILITY,
      false,
      EHEObjectives::GGovernmentStability() );

   GManager::RegisterObjective( EHEObjectives::Ids::ECONOMY_TOO_LOW,
      EHEObjectives::Names::ECONOMY_TOO_LOW,
      false,
      EHEObjectives::GEconomyTooLow() );

   GManager::RegisterObjective( EHEObjectives::Ids::ECONOMY_TOO_HIGH,
      EHEObjectives::Names::ECONOMY_TOO_HIGH,
      false,
      EHEObjectives::GEconomyTooHigh() );

   GManager::RegisterObjective( EHEObjectives::Ids::HAVE_POSITIVE_BUDGET,
      EHEObjectives::Names::HAVE_POSITIVE_BUDGET,
      false,
      EHEObjectives::GHaveMoney() );

   GManager::RegisterObjective( EHEObjectives::Ids::MEET_RESOURCE_DEMAND,
      EHEObjectives::Names::MEET_RESOURCE_DEMAND,
      false,
      EHEObjectives::GIndustrialStrength() );   

   GManager::RegisterObjective( EHEObjectives::Ids::HIGH_SELF_RELATIONS,
      EHEObjectives::Names::HIGH_SELF_RELATIONS,
      false,
      EHEObjectives::GHighSelfRelations() );

   GManager::RegisterObjective( EHEObjectives::Ids::HAVE_MILITARY_ALLIES,
      EHEObjectives::Names::HAVE_MILITARY_ALLIES,
      false,
      EHEObjectives::GHaveMilitaryAllies() );

   GManager::RegisterObjective( EHEObjectives::Ids::HIGH_NATIONAL_SECURITY,
      EHEObjectives::Names::HIGH_NATIONAL_SECURITY,
      false,
      EHEObjectives::GHaveHighNationalSecurity() );

   GManager::RegisterObjective( EHEObjectives::Ids::HIGH_HUMAN_DEVELOPMENT,
      EHEObjectives::Names::HIGH_HUMAN_DEVELOPMENT,
      false,
      EHEObjectives::GHaveHighHumanDevelopment() );

   GManager::RegisterObjective( EHEObjectives::Ids::HIGH_INFRASTRUCTURE,
      EHEObjectives::Names::HIGH_INFRASTRUCTURE,
      false,
      EHEObjectives::GHaveHighInfrastructure() );

   GManager::RegisterObjective( EHEObjectives::Ids::HIGH_TELECOM_LEVEL,
      EHEObjectives::Names::HIGH_TELECOM_LEVEL,
      false,
      EHEObjectives::GHaveHighTelecomLevel() );

   GManager::RegisterObjective( EHEObjectives::Ids::HIGH_ECONOMIC_HEALTH,
      EHEObjectives::Names::HIGH_ECONOMIC_HEALTH,
      false,
      EHEObjectives::GHaveHighEconomicHealth() );

   GManager::RegisterObjective( EHEObjectives::Ids::HAVE_NO_DEBT,
      EHEObjectives::Names::HAVE_NO_DEBT,
      false,
      EHEObjectives::GHaveNoDebt() );

   GManager::RegisterObjective( EHEObjectives::Ids::STRONG_AIR_FORCES,
      EHEObjectives::Names::STRONG_AIR_FORCES,
      false,
      EHEObjectives::GStrongAir() );

   GManager::RegisterObjective( EHEObjectives::Ids::STRONG_GROUND_FORCES,
      EHEObjectives::Names::STRONG_GROUND_FORCES,
      false,
      EHEObjectives::GStrongGround() );

   GManager::RegisterObjective( EHEObjectives::Ids::STRONG_NAVAL_FORCES,
      EHEObjectives::Names::STRONG_NAVAL_FORCES,
      false,
      EHEObjectives::GStrongNaval() );

   GManager::RegisterObjective( EHEObjectives::Ids::STRONG_INFANTRY_FORCES,
      EHEObjectives::Names::STRONG_INFANTRY_FORCES,
      false,
      EHEObjectives::GStrongInfantry() );

   GManager::RegisterObjective( EHEObjectives::Ids::STRONG_NUCLEAR_FORCES,
      EHEObjectives::Names::STRONG_NUCLEAR_FORCES,
      false,
      EHEObjectives::GStrongNuclear() );

   GManager::RegisterObjective( EHEObjectives::Ids::HAVE_AMDS,
      EHEObjectives::Names::HAVE_AMDS,
      false,
      EHEObjectives::GHaveAMDS() );

   GManager::RegisterObjective( EHEObjectives::Ids::GOOD_RESEARCH_LEVEL,
      EHEObjectives::Names::GOOD_RESEARCH_LEVEL,
      false,
      EHEObjectives::GHaveGoodResearchLevel() );

   GManager::RegisterObjective( EHEObjectives::Ids::MILITARY_ACCESS,
      EHEObjectives::Names::MILITARY_ACCESS,
      false,
      EHEObjectives::GHaveMilitaryAccess() );

   GManager::RegisterObjective( EHEObjectives::Ids::ENEMIES_LOW_STABILITY,
      EHEObjectives::Names::ENEMIES_LOW_STABILITY,
      false,
      EHEObjectives::GEnemiesLowStability() );

   GManager::RegisterObjective( EHEObjectives::Ids::ENEMIES_NO_MONEY,
      EHEObjectives::Names::ENEMIES_NO_MONEY,
      false,
      EHEObjectives::GEnemiesNoMoney() );

   GManager::RegisterObjective( EHEObjectives::Ids::ENEMIES_NO_MILITARY_FORCES,
      EHEObjectives::Names::ENEMIES_NO_MILITARY_FORCES,
      false,
      EHEObjectives::GEnemiesNoMilitary() );

   GManager::RegisterObjective( EHEObjectives::Ids::LOW_CORRUPTION_LEVEL,
      EHEObjectives::Names::LOW_CORRUPTION_LEVEL,
      false,
      EHEObjectives::GLowCorruptionLevel() );


}

/*!
* Register actions in EHE
**/
void GServer::RegisterActions()
{
   GManager::RegisterAction( EHEActions::Ids::RAISE_TAXES,
      EHEActions::Names::RAISE_TAXES,
      EHEActions::GRaiseTaxes() );

   GManager::RegisterAction( EHEActions::Ids::LOWER_TAXES,
      EHEActions::Names::LOWER_TAXES,
      EHEActions::GLowerTaxes() );

   GManager::RegisterAction( EHEActions::Ids::RAISE_INTEREST_LEVEL,
      EHEActions::Names::RAISE_INTEREST_LEVEL,
      EHEActions::GRaiseInterestRate() );

   GManager::RegisterAction( EHEActions::Ids::LOWER_INTEREST_LEVEL,
      EHEActions::Names::LOWER_INTEREST_LEVEL,
      EHEActions::GLowerInterestRate() );

   GManager::RegisterAction( EHEActions::Ids::ENROLL_INFANTRY,
      EHEActions::Names::ENROLL_INFANTRY,
      EHEActions::GEnrollMen() );

   GManager::RegisterAction( EHEActions::Ids::INCREASE_PRODUCTION,
      EHEActions::Names::INCREASE_PRODUCTION,
      EHEActions::GIncreaseProduction() );

   GManager::RegisterAction( EHEActions::Ids::WAR_DECLARATION,
      EHEActions::Names::WAR_DECLARATION,
      EHEActions::GDeclareWar() );

   GManager::RegisterAction( EHEActions::Ids::TRAIN_SPECOPS,
      EHEActions::Names::TRAIN_SPECOPS,
      EHEActions::GTrainSpecops() );

   GManager::RegisterAction( EHEActions::Ids::DISBAND_NUKE,
      EHEActions::Names::DISBAND_NUKE,
      EHEActions::GDisbandNuke() );

   GManager::RegisterAction( EHEActions::Ids::LAUNCH_NUKE,
      EHEActions::Names::LAUNCH_NUKE,
      EHEActions::GLaunchNuke() );

   GManager::RegisterAction( EHEActions::Ids::NUCLEARIZE_COUNTRY,
      EHEActions::Names::NUCLEARIZE_COUNTRY,
      EHEActions::GNuclearizeCountry() );

   GManager::RegisterAction( EHEActions::Ids::RAISE_RESOURCE_TAXES,
      EHEActions::Names::RAISE_RESOURCE_TAXES,
      EHEActions::GRaiseResourceTaxes() );

   GManager::RegisterAction( EHEActions::Ids::LOWER_RESOURCE_TAXES,
      EHEActions::Names::LOWER_RESOURCE_TAXES,
      EHEActions::GLowerResourceTaxes() );

   GManager::RegisterAction( EHEActions::Ids::DECLARE_MARTIAL_LAW,
      EHEActions::Names::DECLARE_MARTIAL_LAW,
      EHEActions::GDeclareMartialLaw() );

   GManager::RegisterAction( EHEActions::Ids::CHANGE_TO_DEMOCRACY,
      EHEActions::Names::CHANGE_TO_DEMOCRACY,
      EHEActions::GChangeToDemocracy() );

   GManager::RegisterAction( EHEActions::Ids::CHANGE_TO_COMMUNISM,
      EHEActions::Names::CHANGE_TO_COMMUNISM,
      EHEActions::GChangeToCommunism() );

   GManager::RegisterAction( EHEActions::Ids::CHANGE_TO_DICTATORSHIP,
      EHEActions::Names::CHANGE_TO_DICTATORSHIP,
      EHEActions::GChangeToDictatorship() );

   GManager::RegisterAction( EHEActions::Ids::CHANGE_TO_MONARCHY,
      EHEActions::Names::CHANGE_TO_MONARCHY,
      EHEActions::GChangeToMonarchy() );

   GManager::RegisterAction( EHEActions::Ids::CHANGE_TO_THEOCRACY,
      EHEActions::Names::CHANGE_TO_THEOCRACY,
      EHEActions::GChangeToTheocracy() );

   GManager::RegisterAction( EHEActions::Ids::GIVE_BETTER_STATUS_REL_LANG,
      EHEActions::Names::GIVE_BETTER_STATUS_REL_LANG,
      EHEActions::GImproveStatusRelLang() );

   GManager::RegisterAction( EHEActions::Ids::GIVE_WORSE_STATUS_REL_LANG,
      EHEActions::Names::GIVE_WORSE_STATUS_REL_LANG,
      EHEActions::GDecreaseStatusRelLang() );

   GManager::RegisterAction( EHEActions::Ids::LEGAL_FREEDOM_SPEECH,
      EHEActions::Names::LEGAL_FREEDOM_SPEECH,
      EHEActions::GLegalFreedomOfSpeech() );

   GManager::RegisterAction( EHEActions::Ids::ILLEGAL_FREEDOM_SPEECH,
      EHEActions::Names::ILLEGAL_FREEDOM_SPEECH,
      EHEActions::GIllegalFreedomOfSpeech() );

   GManager::RegisterAction( EHEActions::Ids::LEGAL_FREEDOM_DEMONSTRATION,
      EHEActions::Names::LEGAL_FREEDOM_DEMONSTRATION,
      EHEActions::GLegalFreedomOfDemonstration() );

   GManager::RegisterAction( EHEActions::Ids::ILLEGAL_FREEDOM_DEMONSTRATION,
      EHEActions::Names::ILLEGAL_FREEDOM_DEMONSTRATION,
      EHEActions::GIllegalFreedomOfDemonstration() );

   GManager::RegisterAction( EHEActions::Ids::ILLIMITED_NB_CHILDREN,
      EHEActions::Names::ILLIMITED_NB_CHILDREN,
      EHEActions::GIllimitedNbChildren() );

   GManager::RegisterAction( EHEActions::Ids::LIMITED_NB_CHILDREN,
      EHEActions::Names::LIMITED_NB_CHILDREN,
      EHEActions::GLimitedNbChildren() );

   GManager::RegisterAction( EHEActions::Ids::LEGAL_ABORTION,
      EHEActions::Names::LEGAL_ABORTION,
      EHEActions::GLegalAbortion() );

   GManager::RegisterAction( EHEActions::Ids::ILLEGAL_ABORTION,
      EHEActions::Names::ILLEGAL_ABORTION,
      EHEActions::GIllegalAbortion() );

   GManager::RegisterAction( EHEActions::Ids::LEGAL_CONTRACEPTION,
      EHEActions::Names::LEGAL_CONTRACEPTION,
      EHEActions::GLegalContraception() );

   GManager::RegisterAction( EHEActions::Ids::ILLEGAL_CONTRACEPTION,
      EHEActions::Names::LIMITED_NB_CHILDREN,
      EHEActions::GIllegalContraception() );

   GManager::RegisterAction( EHEActions::Ids::ALLIANCE,
      EHEActions::Names::ALLIANCE,
      EHEActions::GAlliance() );

   GManager::RegisterAction( EHEActions::Ids::ANNEXATION,
      EHEActions::Names::ANNEXATION,
      EHEActions::GAnnexation() );

   GManager::RegisterAction( EHEActions::Ids::FREE_REGION,
      EHEActions::Names::FREE_REGION,
      EHEActions::GFreeRegion() );

   GManager::RegisterAction( EHEActions::Ids::MILITARY_TRESPASSING_RIGHT,
      EHEActions::Names::MILITARY_TRESPASSING_RIGHT,
      EHEActions::GMilitaryTrespassingRights() );

   GManager::RegisterAction( EHEActions::Ids::CULTURAL_EXCHANGES,
      EHEActions::Names::CULTURAL_EXCHANGES,
      EHEActions::GCulturalExchanges() );

   GManager::RegisterAction( EHEActions::Ids::NOBLE_CAUSE,
      EHEActions::Names::NOBLE_CAUSE,
      EHEActions::GNobleCause() );

   GManager::RegisterAction( EHEActions::Ids::RESEARCH_PARTNERSHIP,
      EHEActions::Names::RESEARCH_PARTNERSHIP,
      EHEActions::GResearchPartnership() );

   GManager::RegisterAction( EHEActions::Ids::HUMAN_DEVELOPMENT_COLLABORATION,
      EHEActions::Names::HUMAN_DEVELOPMENT_COLLABORATION,
      EHEActions::GHumanDevelopmentCollaboration() );

   GManager::RegisterAction( EHEActions::Ids::ECONOMIC_PARTNERSHIP,
      EHEActions::Names::ECONOMIC_PARTNERSHIP,
      EHEActions::GEconomicPartnership() );

   GManager::RegisterAction( EHEActions::Ids::COMMON_MARKET,
      EHEActions::Names::COMMON_MARKET,
      EHEActions::GCommonMarket() );

   GManager::RegisterAction( EHEActions::Ids::ASSUME_FOREIGN_DEBT_GIVE,
      EHEActions::Names::ASSUME_FOREIGN_DEBT_GIVE,
      EHEActions::GAssumeForeignDebtGive() );

   GManager::RegisterAction( EHEActions::Ids::ASSUME_FOREIGN_DEBT_RECEIVE,
      EHEActions::Names::ASSUME_FOREIGN_DEBT_RECEIVE,
      EHEActions::GAssumeForeignDebtReceive() );

   GManager::RegisterAction( EHEActions::Ids::ECONOMIC_AID_GIVE,
      EHEActions::Names::ECONOMIC_AID_GIVE,
      EHEActions::GEconomicAidGive() );

   GManager::RegisterAction( EHEActions::Ids::ECONOMIC_AID_RECEIVE,
      EHEActions::Names::ECONOMIC_AID_RECEIVE,
      EHEActions::GEconomicAidReceive() );

   GManager::RegisterAction( EHEActions::Ids::ECONOMIC_EMBARGO,
      EHEActions::Names::ECONOMIC_EMBARGO,
      EHEActions::GEconomicEmbargo() );

   GManager::RegisterAction( EHEActions::Ids::WEAPON_TRADE,
      EHEActions::Names::WEAPON_TRADE,
      EHEActions::GWeaponTrade() );

   GManager::RegisterAction( EHEActions::Ids::WEAPON_TRADE_EMBARGO,
      EHEActions::Names::WEAPON_TRADE_EMBARGO,
      EHEActions::GWeaponTradeEmbargo() );

   GManager::RegisterAction( EHEActions::Ids::RESEARCH_AMDS,
      EHEActions::Names::RESEARCH_AMDS,
      EHEActions::GResearchAMDS() );

   GManager::RegisterAction( EHEActions::Ids::TRAIN_INFANTRY,
      EHEActions::Names::TRAIN_INFANTRY,
      EHEActions::GTrainInfantry() );

   GManager::RegisterAction( EHEActions::Ids::DISBAND_INFANTRY,
      EHEActions::Names::DISBAND_INFANTRY,
      EHEActions::GDisbandInfantry() );

   GManager::RegisterAction( EHEActions::Ids::BUILD_AIR_UNITS,
      EHEActions::Names::BUILD_AIR_UNITS,
      EHEActions::GBuildAirForces() );

   GManager::RegisterAction( EHEActions::Ids::TRAIN_AIR_UNITS,
      EHEActions::Names::TRAIN_AIR_UNITS,
      EHEActions::GTrainAirForces() );

   GManager::RegisterAction( EHEActions::Ids::DISBAND_AIR_UNITS,
      EHEActions::Names::DISBAND_AIR_UNITS,
      EHEActions::GDisbandAirForces() );

   GManager::RegisterAction( EHEActions::Ids::BUILD_NAVAL_UNITS,
      EHEActions::Names::BUILD_NAVAL_UNITS,
      EHEActions::GBuildNavalForces() );

   GManager::RegisterAction( EHEActions::Ids::TRAIN_NAVAL_UNITS,
      EHEActions::Names::TRAIN_NAVAL_UNITS,
      EHEActions::GTrainNavalForces() );

   GManager::RegisterAction( EHEActions::Ids::DISBAND_NAVAL_UNITS,
      EHEActions::Names::DISBAND_NAVAL_UNITS,
      EHEActions::GDisbandAirForces() );

   GManager::RegisterAction( EHEActions::Ids::BUILD_GROUND_UNITS,
      EHEActions::Names::BUILD_GROUND_UNITS,
      EHEActions::GBuildGroundForces() );

   GManager::RegisterAction( EHEActions::Ids::TRAIN_GROUND_UNITS,
      EHEActions::Names::TRAIN_GROUND_UNITS,
      EHEActions::GTrainGroundForces() );

   GManager::RegisterAction( EHEActions::Ids::DISBAND_GROUND_UNITS,
      EHEActions::Names::DISBAND_GROUND_UNITS,
      EHEActions::GDisbandGroundForces() );

   GManager::RegisterAction( EHEActions::Ids::PRIVATE_CONTROL_RESOURCE,
      EHEActions::Names::PRIVATE_CONTROL_RESOURCE,
      EHEActions::GPrivateResourceControl() );

   GManager::RegisterAction( EHEActions::Ids::PUBLIC_CONTROL_RESOURCE,
      EHEActions::Names::PUBLIC_CONTROL_RESOURCE,
      EHEActions::GPublicResourceControl() );

   GManager::RegisterAction( EHEActions::Ids::ASSASSINATION,
      EHEActions::Names::ASSASSINATION,
      EHEActions::GAssassination() );

   GManager::RegisterAction( EHEActions::Ids::ESPIONAGE_MILITARY,
      EHEActions::Names::ESPIONAGE_MILITARY,
      EHEActions::GEspionageMilitary() );

   GManager::RegisterAction( EHEActions::Ids::ESPIONAGE_CIVILIAN,
      EHEActions::Names::ESPIONAGE_CIVILIAN,
      EHEActions::GEspionageCivilian() );

   GManager::RegisterAction( EHEActions::Ids::SABOTAGE_MILITARY,
      EHEActions::Names::SABOTAGE_MILITARY,
      EHEActions::GSabotageMilitary() );

   GManager::RegisterAction( EHEActions::Ids::SABOTAGE_CIVILIAN,
      EHEActions::Names::SABOTAGE_CIVILIAN,
      EHEActions::GSabotageCivilian() );

   GManager::RegisterAction( EHEActions::Ids::TERRORISM,
      EHEActions::Names::TERRORISM,
      EHEActions::GTerrorism() );

   GManager::RegisterAction( EHEActions::Ids::COUP_ETAT,
      EHEActions::Names::COUP_ETAT,
      EHEActions::GCoupEtat() );

   GManager::RegisterAction( EHEActions::Ids::CREATE_SPECOPS,
      EHEActions::Names::CREATE_SPECOPS,
      EHEActions::GCreateSpecOpsCells() );

   GManager::RegisterAction( EHEActions::Ids::DISBAND_SPECOPS,
      EHEActions::Names::DISBAND_SPECOPS,
      EHEActions::GDisbandSpecOpsCells() );

   GManager::RegisterAction( EHEActions::Ids::CLOSE_EMIGRATION,
      EHEActions::Names::CLOSE_EMIGRATION,
      EHEActions::GCloseEmigration() );

   GManager::RegisterAction( EHEActions::Ids::CLOSE_IMMIGRATION,
      EHEActions::Names::CLOSE_IMMIGRATION,
      EHEActions::GCloseImmigration() );

   GManager::RegisterAction( EHEActions::Ids::OPEN_EMIGRATION,
      EHEActions::Names::OPEN_EMIGRATION,
      EHEActions::GOpenEmigration() );

   GManager::RegisterAction( EHEActions::Ids::OPEN_IMMIGRATION,
      EHEActions::Names::OPEN_IMMIGRATION,
      EHEActions::GOpenImmigration() );

   GManager::RegisterAction( EHEActions::Ids::TRADE_IN_REGION,
      EHEActions::Names::TRADE_IN_REGION,
      EHEActions::GTradeInRegion() );

   GManager::RegisterAction( EHEActions::Ids::TRADE_IN_RESEARCH,
      EHEActions::Names::TRADE_IN_RESEARCH,
      EHEActions::GTradeInResearch() );

}

bool GServer::ChangeAdminPlayer(SDK::GPlayer* in_pPlayer)
{
    bool l_bAdminPlayerChanged = false;

    if(in_pPlayer != NULL && g_Joshua.AdminPlayerID() != in_pPlayer->Id())
    {
        g_Joshua.AdminPlayerID(in_pPlayer->Id());
        SendPlayersList();
        l_bAdminPlayerChanged = true;

        g_Joshua.Log(L"Admin player changed to " + in_pPlayer->Name() + L", " +
                     m_DAL.CountryData(in_pPlayer->ModID())->Name());
    }

    return l_bAdminPlayerChanged;
}

/*!
* Initialize the combat engine, register types etc.
* @return true if successfully initialized, false otherwise
**/
bool GServer::CombatEngineInit()
{
   //Register the handler to the SDK::Combat over event
   g_CombatManager.RegisterOnCombatOverHandler((CALLBACK_HANDLER_void_int_pvoid)&SP2::GMilitaryEventHandler::HandleCombatOver, &m_MilitaryGameEventHandler);

   return true;
}

/*!
* Shutdown the SDK::Combat engine
* @return true if successfully shutted down, false otherwise
**/
bool GServer::CombatEngineShutdown()
{
   return true;
}

void GServer::TryStartGame(void)
{
   GDZLOG(EDZLogLevel::Entry, L"");

   // Get Player list
   const SDK::GPlayers & l_players = g_Joshua.HumanPlayers();
   SDK::GPlayers::const_iterator l_itrPlayer = l_players.begin();

   bool l_bAllPlayerReady = true;

   while(l_bAllPlayerReady && l_itrPlayer != l_players.end())
   {
      l_bAllPlayerReady = (l_itrPlayer->second->PlayerStatus() == SDK::PLAYER_STATUS_READY);
      l_itrPlayer++;
   }

   GDZLOG(EDZLogLevel::Info1, L"All players ready: " + GString(l_bAllPlayerReady));

   // If every player are ready, start the game
   if(l_bAllPlayerReady)
   {
      //1st synchronisation of the country data of each human player, make sure everything is sent
      {
         for(SDK::GPlayers::const_iterator l_HumanPlayersIt = g_Joshua.HumanPlayers().begin() ;
            l_HumanPlayersIt != g_Joshua.HumanPlayers().end();
            l_HumanPlayersIt++)
         {
            if(l_HumanPlayersIt->second->PlayerStatus() ==  SDK::PLAYER_STATUS_READY)
                g_SP2Server->SynchronizePlayerCountryData(*(l_HumanPlayersIt->second), true);
         }
      }//end of synchronize country data

      StartGame();
   }

   GDZLOG(EDZLogLevel::Exit, L"");
}

void GServer::ActivateReadyHumanPlayers(void)
{

   const SDK::GPlayers & l_players = g_Joshua.HumanPlayers();
   SDK::GPlayers::const_iterator l_itrPlayer = l_players.begin();

   while(l_itrPlayer != l_players.end())
   {
      SDK::GPlayer *l_pPlayer = l_itrPlayer->second;

      // Activate the player that are ready
      if(l_pPlayer->PlayerStatus() == SDK::PLAYER_STATUS_READY)
      {
         // Add this guy to the active list
         g_Joshua.ActivatePlayer(l_pPlayer);

         // Send him initial data
         SendInitialDataToPlayer(l_pPlayer);

         SetPlayerJoinDate(l_itrPlayer->first);
      }

      // Iterate
      l_itrPlayer++;
   }
}

void GServer::ActivateAIPlayers(void)
{


   // Iterate over countries and activate an AI for those without players...
   GCountries l_Countries = Countries();
   GCountries::iterator l_itrCountry = l_Countries.begin();

   while(l_itrCountry != l_Countries.end())
   {
      // Try to get the player for this country
      SDK::GPlayer* l_pPlayer = g_Joshua.ActivePlayerByModID(l_itrCountry->Id());

      // If the country doesnt have a player create an ai for it
      if(!l_pPlayer)
      {
         //Create a player and a context, assign it a mod id, the Mod Id equals the Country Id
         l_pPlayer =            new SDK::GPlayer() ;
         l_pPlayer->Id         (g_Joshua.NextID() );
         l_pPlayer->ModID      (l_itrCountry->Id());

         //Create an AI Client and associate it with the player
         SDK::GClientAI* l_pAIClient = new SDK::GClientAI();
         l_pAIClient->Id(l_pPlayer->Id());
         l_pPlayer->Client(l_pAIClient,true);

         // Activate thise player
         g_Joshua.ActivatePlayer(l_pPlayer);

         GCountryData* const l_pCountryData = m_DAL.CountryData(l_itrCountry->Id());
         if(l_pCountryData->Advisor() != nullptr)
         {
            GDZLOG(EDZLogLevel::Info1, L"Removing saved advisor for formerly-human-controlled " + l_pCountryData->NameAndIDForLog());
            m_DAL.CountryData(l_itrCountry->Id())->RemoveAdvisor();
         }
      }
      l_itrCountry++;
   }
}

void GServer::SendInitialDataToPlayer(SDK::GPlayer* in_pPlayer)
{
   //! \todo Set an advisor for that player based on the settings asked by the player
   gassert(in_pPlayer->ModID(),"GServer::SendInitialDataToPlayer invalid player, has no mod id");
   g_ServerDAL.CountryData( in_pPlayer->ModID() )->AddAdvisor(
      EAdvisorStatus::Manual,EAdvisorStatus::Manual,EAdvisorStatus::Manual);

   // inform other players that a new one joined
   InformPlayerJoined(in_pPlayer);

   // Send data through a SendData event
   SDK::GGameEventSPtr l_Event = CREATE_GAME_EVENT(SP2::Event::GSendData);
   SP2::Event::GSendData* l_pSendData = (SP2::Event::GSendData*)l_Event.get();
   g_ServerDAL.FillInitialData(l_pSendData,in_pPlayer->ModID());
   l_Event->m_iSource    = SDK::Event::ESpecialTargets::Server;
   l_Event->m_iTarget    = in_pPlayer->Id();
   g_Joshua.RaiseEvent(l_Event);

   //Send the initial game objectives
   SDK::GGameEventSPtr l_EvtObj = CREATE_GAME_EVENT(SP2::Event::GSetGameObjectives);
   SP2::Event::GSetGameObjectives* l_pSetGameObj = (SP2::Event::GSetGameObjectives*)l_EvtObj.get();
   l_pSetGameObj->m_iSource = SDK::Event::ESpecialTargets::Server;
   l_pSetGameObj->m_iTarget = in_pPlayer->Id();
   l_pSetGameObj->m_vObjectives = g_ServerDAL.GameObjectives();
   g_Joshua.RaiseEvent(l_EvtObj);
}

void GServer::SendGameStartEvent(INT32 in_pPlayerID)
{
   GDZLOG(EDZLogLevel::Entry, L"in_pPlayerID = " + GDZDebug::FormatHex(in_pPlayerID));

   g_Joshua.Log("   Sending start game event");

   //Broadcast a Start Game Game event to all player
   SDK::GGameEventSPtr l_eventGameStart = CREATE_GAME_EVENT(SP2::Event::GStartGame);
   l_eventGameStart->m_iSource   = SDK::Event::ESpecialTargets::Server;
   l_eventGameStart->m_iTarget   = in_pPlayerID;

   SP2::Event::GStartGame* l_pSG = (SP2::Event::GStartGame*)l_eventGameStart.get();
   l_pSG->StartDate  = g_ServerDAL.ZeroDate().Serialize();
   g_Joshua.RaiseEvent(l_eventGameStart);

   GDZLOG(EDZLogLevel::Exit, L"");
}

bool GServer::GameStarted()
{
   return m_bGameStarted;
}

void GServer::GameStarted(bool in_bGameStarted)
{
   if(in_bGameStarted)
   {
      if(GameLoaded() )
      {
         // Start the AI iteration
         g_Joshua.MustIterateAI(true);
         m_bGameStarted = true;
      }
   }
   else
   {
      // Stop AI from iterating
      g_Joshua.MustIterateAI(false);
      g_Joshua.DeleteAllPlayers();
      m_bGameStarted = false;
   }
}

/*!
* Function that will receive the UDP Packets
**/
void GServer::OnUDPPacketReception(int in_iPacketSize,void* in_pPointer)
{
   return;
}

void GServer::SendCombatInfo(UINT32 in_iPlayerID)
{
   //Send all the Combat::Information
   {
      SDK::GGameEventSPtr l_CInfo = CREATE_GAME_EVENT(SDK::Event::GSyncCombatDataFirst);
      l_CInfo->m_iSource                        = SDK::Event::ESpecialTargets::Server;
      l_CInfo->m_iTarget                        = in_iPlayerID;
      SDK::Event::GSyncCombatDataFirst* l_pSync = (SDK::Event::GSyncCombatDataFirst*)l_CInfo.get();

      l_pSync->m_iNbCountries = m_DAL.NbCountry();

      //Added modified designs
      {
         hash_map<UINT32,SDK::Combat::Design::GUnit*>::const_iterator l_It = g_Joshua.UnitManager().RegisteredUnitDesigns().begin();
         while(l_It != g_Joshua.UnitManager().RegisteredUnitDesigns().end())
         {
            l_pSync->m_vAddedModifiedDesigns.insert(make_pair(l_It->second->Id(), l_It->second));
            l_It++;
         }
      }


      {//Send all the units
         hash_map<UINT32,SDK::Combat::GUnit*>::const_iterator l_It = g_Joshua.UnitManager().Units().begin();
         while(l_It != g_Joshua.UnitManager().Units().end())
         {
            l_pSync->m_vAddedModifiedUnits.insert(make_pair(l_It->second->Id(), l_It->second));
            l_It++;
         }
      }

      {//Send all the unit groups
         hash_map<UINT32,SDK::Combat::GUnitGroup*>::const_iterator l_It = g_Joshua.UnitManager().UnitGroups().begin();
         while(l_It != g_Joshua.UnitManager().UnitGroups().end())
         {
            l_pSync->m_vAddedModifiedUnitGroups.insert(make_pair(l_It->second->Id(), l_It->second));
            l_It++;
         }
      }

      {//Send all the arenas info
         hash_map<UINT32,SDK::Combat::GArenaInfo*>::const_iterator l_It = g_Joshua.UnitManager().ArenaInfos().begin();
         while(l_It != g_Joshua.UnitManager().ArenaInfos().end())
         {
            l_pSync->m_vAddedModifiedArenas.insert(make_pair(l_It->second->m_iId, l_It->second));
            l_It++;
         }
      }
      g_Joshua.RaiseEvent(l_CInfo);
   }

   g_Joshua.Log(L"... done (sending combat information)");
}


void GServer::SyncronizeMilitary(INT32 in_iTarget,REAL64 in_fActualClock)
{
   SDK::GGameEventSPtr l_CInfo           = CREATE_GAME_EVENT(SDK::Event::GSyncCombatData);
   l_CInfo->m_iSource                    = SDK::Event::ESpecialTargets::Server;
   l_CInfo->m_iTarget                    = in_iTarget;
   SDK::Event::GSyncCombatData* l_pSync  = (SDK::Event::GSyncCombatData*)l_CInfo.get();

   SDK::Combat::GUnitManager& l_UnitManager = g_Joshua.UnitManager();
   l_pSync->m_vAddedModifiedUnits        = l_UnitManager.ModifiedUnits();
   {
      auto l_It = l_pSync->m_vAddedModifiedUnits.cbegin();
      while(l_It != l_pSync->m_vAddedModifiedUnits.cend())
      {
         if(l_UnitManager.Unit(l_It->first) != nullptr)
            ++l_It;
         else
         {
            GDZLOG(EDZLogLevel::Error, L"Nonexistent modified unit " + GString(l_It->first));
            l_pSync->m_vAddedModifiedUnits.erase(l_It++);
         }
      }
   }

   l_pSync->m_vAddedModifiedDesigns      = l_UnitManager.ModifiedUnitDesigns();
   for_each(l_pSync->m_vAddedModifiedDesigns.cbegin(), l_pSync->m_vAddedModifiedDesigns.cend(), [](const hash_map<UINT32, SDK::Combat::Design::GUnit*>::value_type& in_Pair) { gassert(in_Pair.second != nullptr,"Nonexistent design"); });

   l_pSync->m_vDeletedUnits              = l_UnitManager.RemovedUnits();

   l_pSync->m_vAddedModifiedUnitGroups   = l_UnitManager.ModifiedUnitGroups();
   {
      auto l_It = l_pSync->m_vAddedModifiedUnitGroups.cbegin();
      while(l_It != l_pSync->m_vAddedModifiedUnitGroups.cend())
      {
         if(l_UnitManager.UnitGroup(l_It->first) != nullptr)
            ++l_It;
         else
         {
            GDZLOG(EDZLogLevel::Error, L"Nonexistent modified unit group " + GString(l_It->first));
            l_pSync->m_vAddedModifiedUnitGroups.erase(l_It++);
         }
      }
   }

   l_pSync->m_vDeletedUnitGroups         = l_UnitManager.RemovedUnitGroups();

   l_pSync->m_vAddedModifiedArenas       = l_UnitManager.ModifiedArenaInfos();
   {
      auto l_It = l_pSync->m_vAddedModifiedArenas.cbegin();
      while(l_It != l_pSync->m_vAddedModifiedArenas.cend())
      {
         if(l_UnitManager.ArenaInfo(l_It->first) != nullptr)
            ++l_It;
         else
         {
            GDZLOG(EDZLogLevel::Error, L"Nonexistent modified arena " + GString(l_It->first));
            l_pSync->m_vAddedModifiedArenas.erase(l_It++);
         }
      }
   }

   l_pSync->m_vDeletedArenas             = l_UnitManager.RemovedArenaInfos();
   g_Joshua.RaiseEvent(l_CInfo); 


   m_fPreviousSyncDataUpdate = in_fActualClock;
   l_UnitManager.ClearModifications();
}

// When saving a game, the DB used it create it is also saved, 
// so that at load time, if DB is not the same, 
// the static part of the game is cleaned 
// and a new game is created with only static data read from the DB.
//
// I.E. In th following scenario
// New game (Data1.DB)
// Save game (Save1.sav)
// New game (Data2.DB)
// Load game (Save1.sav)
//
// The game loaded uses Data1.DB, but Data2.DB is the current DB for the game. 
// Since only variable data is saved, static data (cities, regions & country list) 
// via a new game with a condition: do not create game variable data 
// (units, nukes, ressources, population)
//
// Finally, when loading the clean mechanism only 
// cleans the variable portion of objects, 
// and when creating a new game all data is cleaned (static & variable).

bool GServer::OnSave(GIBuffer& io_Buffer)
{
   GDZLOG(EDZLogLevel::Entry, L"io_Buffer.Size() = " + GString(io_Buffer.Size()));

   // Save egnine info
   io_Buffer << g_Joshua.Clock()
             << m_fLastSpeed
             << g_Joshua.GameTime()
             << g_Joshua.ServerName()
             << g_Joshua.MaxPlayers()
             << m_sPlaintextPassword
             << g_Joshua.Private()
             << g_Joshua.MinPing();

   GDZLOG(EDZLogLevel::Info1, L"After saving engine info and before saving server data, io_Buffer.Size() = " + GString(io_Buffer.Size()));

   // Save server data
   io_Buffer << m_sCurrentDB
             << m_fPreviousSyncDataUpdate
             << m_fHalfSecondClock
             << m_fFiveSecondsClock
             << m_fNextCombatIterationTime
             << m_iNuclearMissilesSubmarineCountryToReload;

   GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Size() = " + GString(io_Buffer.Size()));

   GDZLOG(EDZLogLevel::Exit, L"Returning true");
   return true;
}

bool GServer::OnLoad(GOBuffer& io_Buffer)
{
   GDZLOG(EDZLogLevel::Entry, L"io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()));

   g_Joshua.GameSpeed(0);

   GString l_SavedDB, l_sServerName, l_sPassword;
   REAL64 l_fLoadClock = g_Joshua.Clock();
   REAL64 l_fSaveTimeClock, l_fGameTime;
   INT32 l_iMaxPlayers, l_iMinPing;
   bool l_bPrivate;

   // Load egnine info
   io_Buffer >> l_fSaveTimeClock
             >> m_fNextGameSpeed
             >> l_fGameTime
             >> l_sServerName
             >> l_iMaxPlayers
             >> l_sPassword
             >> l_bPrivate
             >> l_iMinPing;

   GDZLOG(EDZLogLevel::Info1, L"After loading engine info and before loading server data, io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()));

   g_Joshua.GameTime(l_fGameTime);
   g_Joshua.ServerName(l_sServerName);
   g_Joshua.MaxPlayers(l_iMaxPlayers);

   SetPassword(l_sPassword);

   g_Joshua.Private(l_bPrivate);
   g_Joshua.MinPing(l_iMinPing);

   // Load AI
   InitAI();

   // Load server data
   io_Buffer >> l_SavedDB
             >> m_fPreviousSyncDataUpdate
             >> m_fHalfSecondClock
             >> m_fFiveSecondsClock
             >> m_fNextCombatIterationTime
             >> m_iNuclearMissilesSubmarineCountryToReload;

   GDZLOG(EDZLogLevel::Info1, L"After loading server data, io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()));

   m_fLoadClockAdjust = l_fLoadClock - l_fSaveTimeClock;

   // Make sure times are relative to current program time
   if(m_fPreviousSyncDataUpdate >= 0)
      m_fPreviousSyncDataUpdate += m_fLoadClockAdjust;

   if(m_fHalfSecondClock >= 0)
      m_fHalfSecondClock += m_fLoadClockAdjust;

   if(m_fFiveSecondsClock >= 0)
      m_fFiveSecondsClock += m_fLoadClockAdjust;

   if(m_fNextCombatIterationTime >= 0)
      m_fNextCombatIterationTime += m_fLoadClockAdjust;

   // Change DB static content only if needed
   if(m_sCurrentDB != l_SavedDB)
   {
      m_bCleanVariableOnly = false;
      m_DAL.DataManager()->CleanGame();

      m_bUpdateStaticOnly = true;
      GDatabase* l_pDB = m_DAL.LoadDatabase(l_SavedDB);
      if(m_DAL.DataManager()->NewGame(l_pDB) == SDK::EGameDataError::NoError)
      {
         m_sCurrentDB = l_SavedDB;
      }
      else
      {
         return false;
      }
   }
   else
   {
      m_bCleanVariableOnly = true;
      m_DAL.DataManager()->CleanGame();
   }

	m_DAL.LoadRelationTable();

    GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()));

   GDZLOG(EDZLogLevel::Exit, L"Returning true");
   return true;
}

bool GServer::OnNew(GDatabase* in_pDatabase)
{
   GDZLOG(EDZLogLevel::Entry, L"in_pDatabase = " + GDZDebug::FormatPtr(in_pDatabase));

   if(!UpdateStaticOnly() )
   {
      g_Joshua.GameSpeed(0);
      g_Joshua.GameTime(0);
   }

   // Load AI
   InitAI();

   g_ServerDAL.ZeroDate(g_ServerDAL.ConfigZeroDate() );

   // Update historical events date now that ZeroDate is initialized, making ActualDate usable
   m_LastHistoricalEventsDate = m_DAL.ActualDate();

   m_fNextGameSpeed = c_fSP2NormalTimeSpeed;

   m_fPreviousSyncDataUpdate = -1;
   m_fHalfSecondClock = -1;
   m_fFiveSecondsClock  = -1;
   m_fNextCombatIterationTime = 0.0f;
   m_fLastEntitySaveTime = 0.0;
   m_iNuclearMissilesSubmarineCountryToReload = 1;

   //Create the country list
   GTable l_Table;
   m_DAL.DBGetCountryList(l_Table);

   m_DAL.NbCountry( (INT16) l_Table.RowCount());
   const size_t l_iEntityCount = GManager::Entities().size();
   const UINT16 l_iNbCountry = m_DAL.NbCountry();
   if(l_iEntityCount < l_iNbCountry)
       g_Joshua.Log(L"Loading " + GString(l_iNbCountry) + L" countries, but loaded only " + GString(l_iEntityCount) + L" AI entities earlier. Some countries might not have functional AI, and game AI training won't be saved. Consider reloading game or using a different entities file", MSGTYPE_WARNING);

   //Create the player list and country listing
   g_Joshua.Log("Creating the player list and country listing");
   m_Countries.resize(l_Table.RowCount());
   for(UINT32 i = 0; i < l_Table.RowCount(); i++)
   {
      INT32       l_ID              = 0;
      INT32       l_CountryNameID   = 0;
      GSString    l_CountryCode        ;
      GString     l_sCountryName       ;
      GSString    l_sCountryFlag       ;

      //Fetch data from the table
      l_Table.Row(i)->Cell(0)->GetData(l_ID);
      l_Table.Row(i)->Cell(1)->GetData(l_CountryNameID);
      l_Table.Row(i)->Cell(2)->GetData(l_CountryCode);            
      l_Table.Row(i)->Cell(3)->GetData(l_sCountryFlag);            
      m_StrTable.Get_String(l_CountryNameID, l_sCountryName);

      //Fill the country
      GCountry l_CountryTemp;
      l_CountryTemp.Id( (INT16) l_ID);
      l_CountryTemp.Name(l_sCountryName);
      // only the first 3 chars will make the code !
      l_CountryTemp.Code(l_CountryCode.substr(0, 3));
      l_CountryTemp.Flag(l_sCountryFlag);
      m_Countries[i] = l_CountryTemp; //Add the country to the list
   }

   GDZLOG(EDZLogLevel::Exit, L"Returning true");
   return true;
}

void GServer::OnClean()
{
   GDZLOG(EDZLogLevel::Entry, L"");

   if(!CleanVariableOnly() )
   {
      m_Countries.clear();
   }

	//Restart 
	m_iMilitaryRankCountryToIterate = 1;

   // De-activate human players, destroy AI players
   g_Joshua.DeleteAllPlayers();

   GDZLOG(EDZLogLevel::Exit, L"");
}

bool GServer::GameLoaded() const
{
   return m_bGameLoaded;
}

void GServer::GameLoaded(bool in_bGameStarted)
{
   m_bGameLoaded = in_bGameStarted;
   if(m_bGameLoaded)
   {
      GameStarted(false);
   }
}

void GServer::RequestNewGame(const GString& in_sNewGameDB)
{
   GDZLOG(EDZLogLevel::Entry, L"in_sNewGameDB = " + in_sNewGameDB);
   m_bPendingNew = true;
   m_sNewGameDB = in_sNewGameDB;
   m_iNewsource = 0;
   GDZLOG(EDZLogLevel::Exit, L"");
}

void GServer::RequestNewGame(const GString& in_sNewGameDB, UINT32 in_iSource)
{
   GDZLOG(EDZLogLevel::Entry, L"in_sNewGameDB = " + in_sNewGameDB + L", in_iSource = " + GString(in_iSource));
   m_bPendingNew = true;
   m_sNewGameDB = in_sNewGameDB;
   m_iNewsource = in_iSource;
   GDZLOG(EDZLogLevel::Exit, L"");
}

void GServer::RequestLoadGame(const GString& in_sLoadGameFile)
{
   GDZLOG(EDZLogLevel::Entry, L"in_sLoadGameFile = " + in_sLoadGameFile);
   m_bPendingLoad = true;
   m_iLoadsource = 0;
   m_sLoadFileName = in_sLoadGameFile;
   GDZLOG(EDZLogLevel::Exit, L"");
}

void GServer::RequestLoadGame(const GString& in_sLoadName, UINT32 in_iSource)
{
   GDZLOG(EDZLogLevel::Entry, L"in_sLoadName = " + in_sLoadName + L", in_iSource = " + GString(in_iSource));
   m_bPendingLoad = true;
   m_sLoadName = in_sLoadName;
   m_iLoadsource = in_iSource;
   m_sLoadFileName = g_Joshua.CurrentMod().m_sPath + c_sSaveGameLocation + in_sLoadName + c_sSaveExtServer;
   GDZLOG(EDZLogLevel::Exit, L"");
}


void GServer::RequestSomeKickingAction(const GString& in_sPlayerNameToKick)
{
   // Find a player with corresponding name and kick him/her
   SDK::GPlayers::const_iterator l_player = g_Joshua.HumanPlayers().begin();
   while(l_player!=g_Joshua.HumanPlayers().end())
   {
      if(l_player->second->Name() == in_sPlayerNameToKick)
      {
         UINT32 l_playerTokick = l_player->first;
         RequestSomeKickingAction(l_playerTokick);
         break;
      }
      ++l_player;
   }
}

void GServer::RequestSomeKickingAction(UINT32 in_iPlayerID)
{
   g_Joshua.SendDisconnectionReasonToPlayer(in_iPlayerID,SDK::ConnectionLostReason::KickedFromServer);
   // Get out of here
   g_Joshua.RemoveHumanPlayer(in_iPlayerID);
}


void GServer::RequestSomeBanningAction(const GString& in_sPlayerNameToBan)
{
   // Find a player with corresponding name and kick him/her
   SDK::GPlayers::const_iterator l_player = g_Joshua.HumanPlayers().begin();
   while(l_player!=g_Joshua.HumanPlayers().end())
   {
      if(l_player->second->Name() == in_sPlayerNameToBan)
      {
         UINT32 l_playerTokick = l_player->first;
         RequestSomeBanningAction(l_playerTokick);
         break;
      }
      ++l_player;
   }
}

void GServer::RequestSomeBanningAction(UINT32 in_iPlayerID)
{

   // Ban this player
   g_Joshua.Banned(in_iPlayerID);

   // Send disconect reason
   g_Joshua.SendDisconnectionReasonToPlayer(in_iPlayerID,SDK::ConnectionLostReason::BanFromServer);
   
   // Get out of here
   g_Joshua.RemoveHumanPlayer(in_iPlayerID);
}


void GServer::RequestSaveGame(const GString& in_sSaveGameFile)
{
   GSaveRequest l_Request;
   l_Request.m_iSourceID = 0;
   l_Request.m_sSaveName = in_sSaveGameFile;
   l_Request.m_sSaveFile = in_sSaveGameFile;

   m_vPendingSaves.push_back(l_Request);
}

void GServer::RequestSaveGame(const GString& in_sSaveName, UINT32 in_iSource)
{
   m_DCL.CreateSaveDirectory();

   GSaveRequest l_Request;
   l_Request.m_iSourceID = in_iSource;
   l_Request.m_sSaveName = in_sSaveName;
   l_Request.m_sSaveFile = g_Joshua.CurrentMod().m_sPath + c_sSaveGameLocation + in_sSaveName + c_sSaveExtServer;

   m_vPendingSaves.push_back(l_Request);
}


bool GServer::StartGame()
{
   GDZLOG(EDZLogLevel::Entry, L"");

   //Raise the StartGameRequest Game Event
   if(GameLoaded() )
   {
      SDK::GGameEventSPtr l_evtGameStartRequest = CREATE_GAME_EVENT(SP2::Event::GStartGameRequest);
      l_evtGameStartRequest->m_iSource = SDK::Event::ESpecialTargets::Server;
      l_evtGameStartRequest->m_iTarget = SDK::Event::ESpecialTargets::Server;
      g_Joshua.RaiseEvent(l_evtGameStartRequest);

      GDZLOG(EDZLogLevel::Exit, L"Returning true");
      return true;
   }

   GDZLOG(EDZLogLevel::Exit, L"Returning false");
   return false;
}

UINT32 GServer::IsCountryChoosen(UINT16 in_iCountryID)
{
   // Iterate over human player and make sure no one has this one...
   SDK::GPlayers::const_iterator l_PlayerIt = g_Joshua.HumanPlayers().begin();

   while(l_PlayerIt != g_Joshua.HumanPlayers().end())
   {
      if(l_PlayerIt->second->ModID() == in_iCountryID)
         return l_PlayerIt->first;
      l_PlayerIt++; 
   }

   return 0;
}

void GServer::SaveGame(const GSaveRequest& in_SaveInfo)
{
   GDZLOG(EDZLogLevel::Entry, L"in_SaveInfo.m_iSourceID = " + GString(in_SaveInfo.m_iSourceID) + L", in_SaveInfo.m_sSaveName = " + in_SaveInfo.m_sSaveName +  +L", in_SaveInfo.m_sSaveFile = " + in_SaveInfo.m_sSaveFile);

   SDK::EGameDataError::Enum l_eResult = (SDK::EGameDataError::Enum) c_iGameSaveLoadErrorNotAdmin;
   // Only server itself (via console) and admin player can save the game
   if( (in_SaveInfo.m_iSourceID == 0) || 
       (in_SaveInfo.m_iSourceID == (UINT32) g_Joshua.AdminPlayerID() ) )
   {
      REAL64 l_fCurTime = g_Joshua.TimeCounter().GetPrecisionTime();
      g_Joshua.Log(GString(L"Saving game...") );
      l_eResult = m_DAL.DataManager()->SaveGame(in_SaveInfo.m_sSaveFile, c_iSP2GameType, c_iSP2DataVersion);
      if(l_eResult == SDK::EGameDataError::NoError)
      {
         g_Joshua.Log("...done");
         g_Joshua.Log(GString(L"Saving game time : ") + GString(g_Joshua.TimeCounter().GetPrecisionTime() - l_fCurTime) );
      }
      else
      {
         g_Joshua.Log("Error while saving game");
      }
   }

   // Just to show the client that the game saves...
   Sleep(1000);

   if(in_SaveInfo.m_iSourceID)
   {
      SDK::GGameEventSPtr l_SaveCompleteEvt = CREATE_GAME_EVENT(SP2::Event::GSaveComplete);
      l_SaveCompleteEvt->m_iSource = SDK::Event::ESpecialTargets::Server;
      l_SaveCompleteEvt->m_iTarget = in_SaveInfo.m_iSourceID;
      SP2::Event::GSaveComplete* l_pSaveComplete = (SP2::Event::GSaveComplete*) l_SaveCompleteEvt.get();
      l_pSaveComplete->m_sName = in_SaveInfo.m_sSaveName;
      l_pSaveComplete->m_eResult = l_eResult;

      g_Joshua.RaiseEvent(l_SaveCompleteEvt);
   }

   GDZLOG(EDZLogLevel::Exit, L"");
}

void GServer::LoadGame()
{
   GDZLOG(EDZLogLevel::Entry, L"");

   SDK::EGameDataError::Enum l_eResult = (SDK::EGameDataError::Enum) c_iGameSaveLoadErrorNotAdmin;
   // Only server itself (via console) and admin player can load the game
   if( (m_iLoadsource == 0) || 
       (m_iLoadsource == (UINT32) g_Joshua.AdminPlayerID() ) )
   {
      g_Joshua.Log(GString(L"Loading game...") );
      l_eResult = m_DAL.DataManager()->LoadGame(m_sLoadFileName, c_iSP2GameType, c_iSP2DataVersion);
      if(l_eResult == SDK::EGameDataError::NoError)
      {
         g_Joshua.Log("...done");
         GameLoaded(true);
      }
      else
      {
         g_Joshua.Log("Error while loading game");
      }

      g_Joshua.UnitManager().ClearModifications();
		GCountryDataItf::m_fHumanDevelopmentAvg = g_ServerDCL.AverageHumanDevelopment();
		

      if(m_iLoadsource)
      {
         SDK::GGameEventSPtr l_LoadCompleteEvt = CREATE_GAME_EVENT(SP2::Event::GLoadComplete);
         l_LoadCompleteEvt->m_iSource = SDK::Event::ESpecialTargets::Server;
         l_LoadCompleteEvt->m_iTarget = m_iLoadsource;
         SP2::Event::GLoadComplete* l_pLoadComplete = (SP2::Event::GLoadComplete*) l_LoadCompleteEvt.get();
         l_pLoadComplete->m_sName = m_sLoadName;
         l_pLoadComplete->m_eResult = l_eResult;

         g_Joshua.RaiseEvent(l_LoadCompleteEvt);
      }
   }

   m_sLoadFileName = "";

   GDZLOG(EDZLogLevel::Exit, L"");
}

void GServer::NewGame()
{
   GDZLOG(EDZLogLevel::Entry, L"");
   SDK::EGameDataError::Enum l_eResult = (SDK::EGameDataError::Enum) c_iGameSaveLoadErrorNotAdmin;
   // Only server itself (via console) and admin player can create a new game
   if( (m_iNewsource == 0) || 
       (m_iNewsource == (UINT32) g_Joshua.AdminPlayerID() ) )
   {
      // Create new game using DB
      m_bCleanVariableOnly = false;
      m_DAL.DataManager()->CleanGame();

      m_bUpdateStaticOnly = false;
      g_Joshua.Log(GString(L"Creating new game...") );
      GDatabase* l_pDB = m_DAL.LoadDatabase(m_sNewGameDB);
      if(l_pDB)
      {
         l_eResult = m_DAL.DataManager()->NewGame(l_pDB);
         if(l_eResult == SDK::EGameDataError::NoError)
         {
            g_Joshua.Log("...done");
            m_sCurrentDB = m_sNewGameDB;
            GameLoaded(true);
         }
         else
         {
            g_Joshua.Log("Error while creating new game");
         }
      }
      else
      {
         g_Joshua.Log("DB failed to initialize");
         l_eResult = SDK::EGameDataError::NewGameError;
      }

      g_Joshua.UnitManager().ClearModifications();

      if(m_iNewsource)
      {
         SDK::GGameEventSPtr l_NewCompleteEvt = CREATE_GAME_EVENT(SP2::Event::GNewComplete);
         l_NewCompleteEvt->m_iSource = SDK::Event::ESpecialTargets::Server;
         l_NewCompleteEvt->m_iTarget = m_iNewsource;
         SP2::Event::GNewComplete* l_pNewComplete = (SP2::Event::GNewComplete*) l_NewCompleteEvt.get();
         l_pNewComplete->m_sDBFileName = m_sNewGameDB;
         l_pNewComplete->m_eResult = l_eResult;

         g_Joshua.RaiseEvent(l_NewCompleteEvt);
      }
   }

   m_sNewGameDB = "";
   GDZLOG(EDZLogLevel::Exit, L"");
}

/*!
* Inform game lobby of player arrival
**/
void GServer::InformPlayerJoined(SDK::GPlayer* in_pPlayer)
{
   const UINT32 c_StrIdPlayerJoined = 102304;
   g_SP2Server->SendChatMessage(in_pPlayer->Id(), SDK::Event::ESpecialTargets::BroadcastActiveHumanPlayers, g_ServerDAL.GetString(c_StrIdPlayerJoined), true);
}

/*!
* Inform game lobby of player departure
**/
void GServer::InformPlayerLeft(SDK::GPlayer* in_pPlayer)
{
   const UINT32 c_StrIdPlayerLeft = 102305;
   g_SP2Server->SendChatMessage(in_pPlayer->Id(), SDK::Event::ESpecialTargets::BroadcastHumanPlayers, g_ServerDAL.GetString(c_StrIdPlayerLeft), true);
}


/*!
* Return the AI Aggressiveness, 1 being the most aggressive
**/
REAL32 GServer::AIAggressiveness()
{
	return m_fAIAggressiveness;
}
void GServer::AIAggressiveness(REAL32 in_fAIAggressiveness)
{
	m_fAIAggressiveness = in_fAIAggressiveness;
		
}

void GServer::SetIteratedAI()
{
    //GDZLOG(EDZLogLevel::Entry, L"");

    if(m_bFirstAIIteration)
    {
        if(m_iIteratedAICount < m_DAL.NbCountry())
            m_iIteratedAICount++;
        else
        {
            //No longer the 1st iteration of some AI
            //Set AI iteration period from config
            GString l_sIterationPeriod;
            if(g_ServerDAL.XMLValue(ITERATION_PERIOD, l_sIterationPeriod))
            {
                GDZLOG(EDZLogLevel::Info1, L"Setting AI iteration period " + l_sIterationPeriod);
                g_Joshua.AIIterationPeriod(l_sIterationPeriod.ToREAL64());
            }
            else
                g_Joshua.Log(L"Could not set AI iteration period from server config",MSGTYPE_WARNING);

            m_bFirstAIIteration = false;
            m_iIteratedAICount = 0;
        }
    }

    //GDZLOG(EDZLogLevel::Exit, L"");
}

void GServer::SendChatMessage(const INT32 in_iSource, const INT32 in_iTarget, const GString& in_sMessage, const bool in_bPrivate) const
{
    GDZLOG(EDZLogLevel::Entry, L"in_iSource = " + GDZDebug::FormatHex(in_iSource) + L", in_iTarget = " + GDZDebug::FormatHex(in_iTarget) + L", in_sMessage = " + in_sMessage + ", in_bPrivate = " + GString(in_bPrivate));

    if(m_HdmServerConfig.ServerChatsEnabled())
    {
        SDK::GGameEventSPtr l_pEvent = CREATE_GAME_EVENT(SDK::Event::GChatEvent);

        // reinterpret_cast, CREATE_GAME_EVENT doesn't "actually" create a GChatEvent object for some reason
        SDK::Event::GChatEvent* const l_pChatEvent = reinterpret_cast<SDK::Event::GChatEvent*>(l_pEvent.get());
        l_pChatEvent->Message(in_sMessage);

        // Have automated chat messages appear green by setting such messages Private
        l_pChatEvent->Private(true);

        l_pEvent->m_iSource = in_iSource;
        l_pEvent->m_iTarget = in_iTarget;

        g_Joshua.RaiseEvent(l_pEvent);
    }

    GDZLOG(EDZLogLevel::Exit, L"");
}

void GServer::SynchronizePlayerCountryData(SDK::GPlayer& in_Player, const bool in_bSendAll) const
{
    //GDZLOG(EDZLogLevel::Entry, L"in_Player.Id = " + GString(in_Player.Id()) + L", in_Player.ModID = " + GString(in_Player.ModID()) + L", in_bSendAll = " + GString(in_bSendAll));

    //Check if the country data of that human player must be synchronized, if so, send it
    GCountryData* const l_pCountryData = g_ServerDAL.CountryData(in_Player.ModID());
    gassert(l_pCountryData != nullptr, "Country Data does not exist for human player");
    
    if(in_bSendAll)
    {
        //Make sure everything is sent so set everything dirty
        l_pCountryData->m_bBudgetDataDirty           = true;
        l_pCountryData->m_bCovertActionCellsDirty    = true;
        l_pCountryData->m_bEconomicDataDirty         = true;
        l_pCountryData->m_bNuclearMissilesDataDirty  = true;
        l_pCountryData->m_bResourceDataDirty         = true;
        l_pCountryData->m_bOtherDataDirty            = true;
    }
    else if(!l_pCountryData->m_bCovertActionCellsDirty)
    {
        const vector<GCovertActionCell>& l_vCells = l_pCountryData->CovertActionCells();
        l_pCountryData->m_bCovertActionCellsDirty = any_of(l_vCells.cbegin(), l_vCells.cend(), [](const GCovertActionCell& in_Cell) { return in_Cell.Dirty(); });
        if(l_pCountryData->m_bCovertActionCellsDirty)
        {
            GDZLOG(EDZLogLevel::Info1, L"At least 1 cell dirty but cells not dirty overall, setting cells dirty overall");
        }
    }

    GDZLOG(EDZLogLevel::Info2, L"l_pCountryData->Dirty = " + GString(l_pCountryData->Dirty()));

    if(l_pCountryData->Dirty())
    {
        const GCovertActionCellsCountryDataMap l_mCellMap = l_pCountryData->CovertActionCellSpecialData();
        const bool l_bUseHdmSyncEvent = (GHdmRelease(11, 4) <= m_mPlayerData.at(in_Player.Id()).m_Hdm) && l_pCountryData->m_bCovertActionCellsDirty && !l_mCellMap.empty();
        SDK::GGameEventSPtr l_SyncEvt = l_bUseHdmSyncEvent ? CREATE_GAME_EVENT(SP2::Event::GHdmSynchronizeCountryDataEvent) : CREATE_GAME_EVENT(SP2::Event::GSynchronizeCountryData);

        l_SyncEvt->m_iSource = SDK::Event::ESpecialTargets::Server;
        l_SyncEvt->m_iTarget = in_Player.Id();
        SP2::Event::GSynchronizeCountryData* const l_pEvent = dynamic_cast<SP2::Event::GSynchronizeCountryData*>(l_SyncEvt.get());
        l_pEvent->CountryData(l_pCountryData);
        gassert(l_pEvent->CountryData() != nullptr, L"Couldn't add country data to GSynchronizeCountryData event");

        if(l_bUseHdmSyncEvent)
        {
            GDZLOG(EDZLogLevel::Info1, L"Sending cell map special data");
            dynamic_cast<SP2::Event::GHdmSynchronizeCountryDataEvent*>(l_pEvent)->m_mCellMap = l_pCountryData->CovertActionCellSpecialData();
        }

        g_Joshua.RaiseEvent(l_SyncEvt);

        l_pCountryData->ClearModifications();
    }

    //GDZLOG(EDZLogLevel::Exit, L"");
}

const GHdmServerConfig& GServer::HdmServerConfig() const
{
    return m_HdmServerConfig;
}

void GServer::AddPlayer(const INT32 in_iId, const UINT32 in_iPassword, const GHdmRelease& in_Hdm)
{
    GDZLOG(EDZLogLevel::Entry, L"in_iId = " + GString(in_iId) + L", in_iPassword = " + GDZDebug::FormatHex(in_iPassword) + L", in_Hdm.ToString() = " + in_Hdm.ToString());

    if(m_mPlayerData.find(in_iId) == m_mPlayerData.cend())
    {
        m_mPlayerData[in_iId].m_iInternalPasswordFromClient = in_iPassword;
        m_mPlayerData[in_iId].m_Hdm = in_Hdm;
        GDZLOG(EDZLogLevel::Info1, L"Player successfully added");
    }
    else
        GDZLOG(EDZLogLevel::Error, L"Player already added with password " + GDZDebug::FormatHex(m_mPlayerData[in_iId].m_iInternalPasswordFromClient));
    
    GDZLOG(EDZLogLevel::Exit, L"");
}

bool GServer::PlayerExists(const INT32 in_iId) const
{
    return m_mPlayerData.find(in_iId) != m_mPlayerData.cend();
}

GHdmRelease GServer::PlayerHdm(const INT32 in_iId) const
{
    gassert(PlayerExists(in_iId), L"Player ID " + GString(in_iId) + L" doesn't exist");
    return m_mPlayerData.at(in_iId).m_Hdm;
}

UINT32 GServer::InternalPasswordFromPlayer(const INT32 in_iId) const
{
    return m_mPlayerData.at(in_iId).m_iInternalPasswordFromClient;
}

void GServer::SetPlayerJoinDate(const INT32 in_iId)
{
    GDZLOG(EDZLogLevel::Entry, L"in_iId = " + GString(in_iId));
    const REAL64 l_fGameTime = g_Joshua.GameTime();
    GDZLOG(EDZLogLevel::Info1, L"Setting player " + g_Joshua.HumanPlayer(in_iId)->Name() + L" join date to " + GString(l_fGameTime));
    m_mPlayerData[in_iId].m_fJoinGameDate = l_fGameTime;
    GDZLOG(EDZLogLevel::Exit, L"");
}

bool GServer::PlayerCanDeclareWar(INT32 in_iId) const
{
    bool l_bCanDeclareWar = true;
    do
    {
        if(g_Joshua.ActivePlayer(in_iId)->AIControlled())
            break;

        gassert(m_mPlayerData.count(in_iId) == 1,"Human player missing");
        const GPlayerLimitsConfig& l_PlayerLimits = m_HdmServerConfig.PlayerLimitsConfig();
        if(!l_PlayerLimits.m_bAdminsAffected && (in_iId == g_Joshua.AdminPlayerID()))
            break;

        const REAL64 l_fGameTime = g_Joshua.GameTime();
        const REAL64 l_fJoinGameDate = m_mPlayerData.at(in_iId).m_fJoinGameDate;
        gassert(l_fGameTime >= l_fJoinGameDate,"Existing player's join date is in future");
        if(l_fGameTime - l_fJoinGameDate >= l_PlayerLimits.m_iDaysBeforeWar)
            break;

        l_bCanDeclareWar = false;
    } while(false);
    return l_bCanDeclareWar;
}

void GServer::SetPassword(const GString& in_sPlaintextPassword)
{
    GDZLOG(EDZLogLevel::Entry, L"in_sPlaintextPassword = " + in_sPlaintextPassword);

    m_sPlaintextPassword = in_sPlaintextPassword;
    g_Joshua.Password(m_sPlaintextPassword);

    GDZLOG(EDZLogLevel::Exit, L"");
}

bool GServer::SaveEntities()
{
    GDZLOG(EDZLogLevel::Entry, L"");

    GFile l_File;
    bool l_bSuccess = false;
    do
    {
        const size_t l_iEntityCount = GManager::Entities().size();
        const UINT16 l_iNbCountry = m_DAL.NbCountry();
        if(l_iEntityCount < l_iNbCountry)
        {
            g_Joshua.Log(L"Only " + GString(l_iEntityCount) + L" AI entities exist for " + GString(l_iNbCountry) + L" countries, skipping saving entities as new entity data would be incomplete", MSGTYPE_WARNING);
            break;
        }

        GString l_sFileName;
        if(!m_DAL.XMLValue(ENTITIES_FILE, l_sFileName))
        {
            g_Joshua.Log(L"Couldn't find valid entities file name", MSGTYPE_WARNING);
            break;
        }
        GDZLOG(EDZLogLevel::Info1, L"Entities file name: " + l_sFileName);

        if(!m_DAL.File(l_sFileName, l_File))
        {
            g_Joshua.Log(L"Couldn't load entities file " + l_sFileName, MSGTYPE_WARNING);
            break;
        }
        GDZLOG(EDZLogLevel::Info1, L"Current entities file bytes size: " + GString(l_File.Size()));

        GString l_sTmpFileName;
        if(!l_File.Extract(l_sTmpFileName))
        {
            g_Joshua.Log(L"Couldn't load entities file " + l_sFileName, MSGTYPE_WARNING);
            break;
        }
        GDZLOG(EDZLogLevel::Info1, L"Extracted entities file name: " + l_sTmpFileName);

        const DWORD l_iFileAttributes = GetFileAttributes(l_sTmpFileName.c_str());
        if((l_iFileAttributes != INVALID_FILE_ATTRIBUTES) && ((l_iFileAttributes & FILE_ATTRIBUTE_READONLY) != 0x0))
        {
            g_Joshua.Log(L"Entities file " + l_sFileName + L" appears read-only, skipping saving entities", MSGTYPE_WARNING);
            break;
        }

        GManager::WriteEntities(l_sTmpFileName);

        //Always returns false
        l_File.Unextract();

        if(!m_DAL.File(l_sFileName, l_File))
        {
            g_Joshua.Log(L"Couldn't reload entities file " + l_sFileName, MSGTYPE_WARNING);
            break;
        }
        GDZLOG(EDZLogLevel::Info1, L"New entities file bytes size: " + GString(l_File.Size()));

        g_Joshua.Log(L"Successfully updated entities file", MSGTYPE_MSG);

        l_bSuccess = true;
    } while(false);

    //Always returns false
    l_File.Unextract();

    GDZLOG(EDZLogLevel::Exit, L"Returning " + GString(l_bSuccess));
    return l_bSuccess;
}
