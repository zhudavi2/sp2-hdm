/**************************************************************
*
* sp2_unit_group_ex.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Owner
* =====
*  Frederic O'Reilly
*
* Copyright  (C) 2004, Laboratoires Golemlabs Inc.
**************************************************************/
#include "golem_pch.hpp"

//#define __LOG_GROUP_MODIFICATIONS__


#ifdef __LOG_GROUP_MODIFICATIONS__
   #define LOG_MODIF(text) (g_Joshua.Log(text) );
#else //#ifdef __LOG_GROUP_MODIFICATIONS__
   #define LOG_MODIF(text) ;
#endif //#ifdef __LOG_GROUP_MODIFICATIONS__

SP2::GUnitGroupEx::GUnitGroupEx()
{
   m_iMoveRegionID     = 0;
   m_iPrevMoveRegionID = 0;
   m_iRegionID         = 0;
   m_ActionAtDest      = EMilitaryAction::Default;
   m_fLastMoveValue    = 0;
   m_eSavedNextStatus  = EMilitaryStatus::Parked;
}

void SP2::GUnitGroupEx::EnterCombat(SDK::Combat::GArena* in_pCombat)
{
   LOG_MODIF( GString(L"EnterCombat : ") + GString(Id() ) + GString(L" -> ") + GString(in_pCombat->Info()->m_iId) );

#ifdef GOLEM_DEBUG
   GDZLOG(EDZLogLevel::Info2, GString(L"Unit group enters combat ") + GString(Id() ));
#endif
   if(m_eStatus != EMilitaryStatus::Attacking)
   {
      m_eCombatStatus    = m_eStatus;
      m_eSavedNextStatus = m_eNextStatus;
      ChangeStatus(EMilitaryStatus::Attacking);
   }

   //If we were in the bombardment list, remove it from there
   {
      //If we were in the bombardment list
      bool l_bPresentInBombardmentList = false;
      {
         const hash_set<UINT32>& l_BombardingGroups = g_ServerDCL.UnitMover().BombardingGroupList();
         hash_set<UINT32>::const_iterator l_It = l_BombardingGroups.find(Id());
         if(l_It != l_BombardingGroups.end())
            l_bPresentInBombardmentList = true;
         else
            l_bPresentInBombardmentList = false;
      }      
      if(l_bPresentInBombardmentList)
      {
         g_ServerDCL.UnitMover().RemoveBombardingGroup(Id());

         //Make sure the flag that says the unit was supposed to bombard is there
         gassert(m_ActionAtDest & EMilitaryAction::BombardRegion,"Unit should be flagged as bombaring");
      }
   }

   m_pArenaInfo = in_pCombat->Info();
   g_Joshua.UnitManager().ModifyUnitGroup(this);
   VerifyData();
}

void SP2::GUnitGroupEx::LeaveCombat()
{
   GDZLOG(EDZLogLevel::Info2, GString(L"Unit group leaves combat ") + GString(Id() ) + L" status " + GString( (UINT32) m_eStatus) + L" combat status " + GString( (UINT32) m_eCombatStatus));

   if(m_eStatus == EMilitaryStatus::Attacking)
   {
      ChangeStatus(m_eCombatStatus);
      gassert(m_eStatus == m_eCombatStatus, L"Current status " + GString(m_eStatus) + L" wasn't properly updated to saved status " + GString(m_eCombatStatus) + L" before combat");
      
      if(g_ServerDCL.UnitMover().IsUnitGroupChangingStatus(Id()))
      {
          if(m_eStatus != m_eSavedNextStatus)
          {
              GDZLOG(EDZLogLevel::Info1, L"Unit group " + GString(Id()) + L", status " + GString(m_eStatus) + L", will have its NextStatus changed from " + GString(m_eNextStatus) + L" to " + GString(m_eSavedNextStatus) + L" to match pre-combat state");

              m_eNextStatus = m_eSavedNextStatus;
          }
      }
      else
          m_eNextStatus = m_eStatus;
   }

   //If unit was supposed to be bombarding (and can still bombard), resume bombardments
   if(m_ActionAtDest & EMilitaryAction::BombardRegion)
   {
      if(CanPerformOffShoreBombardment())
      {
         g_ServerDCL.UnitMover().AddBombardingGroup(Id());
      }
      else
      {
         m_ActionAtDest &= ~EMilitaryAction::BombardRegion;
      }
   }

   m_pArenaInfo = NULL;
   g_Joshua.UnitManager().ModifyUnitGroup(this);
   VerifyData();
}

void SP2::GUnitGroupEx::LeaveBombardment()
{
    GDZLOG(EDZLogLevel::Entry, L"");

    gassert(IsNaval(), L"Bombardment-incapable group trying to leave bombardment");
    gassert(m_ActionAtDest & EMilitaryAction::BombardRegion, L"Non-bombarding bombardment-capable group trying to leave bombardment");

    g_ServerDCL.UnitMover().RemoveBombardingGroup(Id());

    //Clear RegionToBombard to 0 after calling RemoveBombardingGroup, because RemoveBombardingGroup still needs to know what region the group was bombarding
    RegionToBombard(0);
    m_ActionAtDest &= ~EMilitaryAction::BombardRegion;
    g_Joshua.UnitManager().ModifyUnitGroup(this);

    VerifyData();

    GDZLOG(EDZLogLevel::Exit, L"");
}

bool SP2::GUnitGroupEx::SerializeToSave(GIBuffer& io_Buffer) const
{
    const bool l_bResult = Serialize(io_Buffer);
    if(l_bResult)
    {
        io_Buffer << m_iRegionID
                  << m_ActionAtDest
                  << m_iMoveRegionID
                  << m_iPrevMoveRegionID
                  << m_fLastMoveValue;
    }
    else
        GDZLOG(EDZLogLevel::Error, L"Group " + GString(Id()) + L" serialization failed");
    return l_bResult;
}

bool SP2::GUnitGroupEx::UnserializeFromSave(GOBuffer& io_Buffer)
{
    const bool l_bResult = Unserialize(io_Buffer);
    if(l_bResult)
    {
        io_Buffer >> m_iRegionID
                  >> m_ActionAtDest
                  >> m_iMoveRegionID
                  >> m_iPrevMoveRegionID
                  >> m_fLastMoveValue;
        GDZLOG(EDZLogLevel::Info2, L"In region " + g_ServerDAL.RegionNameAndIDForLog(m_iRegionID) + L", destination action " + GString(m_ActionAtDest) + L", move region " + g_ServerDAL.RegionNameAndIDForLog(m_iMoveRegionID) + L", previous move region " + g_ServerDAL.RegionNameAndIDForLog(m_iPrevMoveRegionID) + L", last move value " + GString(m_fLastMoveValue));

        GVector2D<REAL32> l_vfPosition = Position();
        if(_isnan(l_vfPosition.x) || _isnan(l_vfPosition.y))
        {
            if(m_iRegionID > 0)
            {
                const GVector2D<REAL32>& l_vfRegionLocation = g_ServerDCL.UnitMover().RegionLocation(m_iRegionID);
                GDZLOG(EDZLogLevel::Warning, L"Unit group " + GString(Id()) + L" in region " + GString(m_iRegionID) + L" with invalid position, setting to " + GString(l_vfRegionLocation.x) + L", " + GString(l_vfRegionLocation.y));
                Position(l_vfRegionLocation);
            }
            else
            {
                GDZLOG(EDZLogLevel::Warning, L"Unit group " + GString(Id()) + L" with invalid position, setting to 0, 0");
                Position(GVector2D<REAL32>(0.f, 0.f));
            }
        }
        else if((180.f < fabs(l_vfPosition.x)) || (180.f < fabs(l_vfPosition.y)))
        {
            GDZLOG(EDZLogLevel::Warning, L"Unit group " + GString(Id()) + L" with invalidly-saved position " + GString(l_vfPosition.x) + L", " + GString(l_vfPosition.y));
            l_vfPosition.x = GDataAccessLayerServer::ConvertCoordinateToWithinRange(l_vfPosition.x);
            l_vfPosition.y = GDataAccessLayerServer::ConvertCoordinateToWithinRange(l_vfPosition.y);
            Position(l_vfPosition);
        }

        const vector<SP2::GUnitPathPoint>& l_vPathPoints = m_Path.Points();
        for(auto it = l_vPathPoints.cbegin(); it < l_vPathPoints.cend(); ++it)
        {
            if(_isnan(it->m_fDistance))
            {
                GDZLOG(EDZLogLevel::Warning, L"Unit group " + GString(Id()) + L" has invalid movement path, cancelling movement");
                if(m_eStatus == EMilitaryStatus::Moving)
                    ChangeStatus(EMilitaryStatus::Ready);
                m_Path.Reset();
                m_iMoveRegionID = 0;
                break;
            }
        }
    }
    else
        GDZLOG(EDZLogLevel::Error, L"Group " + GString(Id()) + L" unserialization failed");

    VerifyData();
    return l_bResult;
}

void SP2::GUnitGroupEx::VerifyData() const
{
    const ENTITY_ID l_iOwner = OwnerId();
    gassert(0 < l_iOwner,"Owner ID too low");
    gassert(l_iOwner <= g_ServerDAL.NbCountry(), "Owner ID too high");

    const vector<SDK::Combat::GUnit*>& l_vpUnits = Units();
    const bool l_bIsDeploymentQueue = m_eStatus == EMilitaryStatus::ReadyToDeploy;

    {
        const GVector2D<REAL32>& l_vfPosition = Position();
        gassert(!_isnan(l_vfPosition.x), "NaN longitude");
        gassert(-180.f <= l_vfPosition.x, "Longitude too low");
        gassert(l_vfPosition.x <= 180.f, "Longitude too high");
        gassert(!_isnan(l_vfPosition.y), "NaN latitude");
        gassert(-180.f <= l_vfPosition.y, "Latitude too low");
        gassert(l_vfPosition.y <= 180.f, "Latitude too high");

        if(!l_vpUnits.empty() && !l_bIsDeploymentQueue)
        {
            const UINT32 l_iRegion = g_ServerDCL.EarthLocateRegion(l_vfPosition.x, l_vfPosition.y);
            gassert((m_eStatus == EMilitaryStatus::Moving) || (IsNaval() == (l_iRegion == 0)),"Ground group stopped on water, or naval group stopped on land");
        }
    }

    for_each(l_vpUnits.cbegin(), l_vpUnits.cend(), [this](const SDK::Combat::GUnit* const in_pUnit) { gassert(in_pUnit->Group() == this,"Group contains unit that doesn't think it belongs to this group"); });

    if(!l_bIsDeploymentQueue)
    {
        auto IsUnitNaval = [](const SDK::Combat::GUnit* const in_pUnit) { return dynamic_cast<const SP2::GUnit*>(in_pUnit)->IsNaval(); };
        if(IsNaval())
            gassert(all_of(l_vpUnits.cbegin(), l_vpUnits.cend(), IsUnitNaval),"Non-naval unit in naval group");
        else
            gassert(none_of(l_vpUnits.cbegin(), l_vpUnits.cend(), IsUnitNaval),"Naval unit in non-naval group");
    }

    const vector<SP2::GUnitPathPoint>& l_vPathPoints = m_Path.Points();
    gassert(l_vPathPoints.empty() || (m_eStatus <= EMilitaryStatus::CountVisible),"Non-deployed group is moving");
    for_each(l_vPathPoints.cbegin(), l_vPathPoints.cend(), [](const SP2::GUnitPathPoint& in_Point)
    {
        const GVector2D<REAL32>& l_vfPosition = in_Point.m_Position;
        gassert(!_isnan(l_vfPosition.x),"NaN longitude");
        gassert(-180.f <= l_vfPosition.x,"Longitude too low");
        gassert(l_vfPosition.x <= 180.f,"Longitude too high");
        gassert(!_isnan(l_vfPosition.y),"NaN latitude");
        gassert(-180.f <= l_vfPosition.y,"Latitude too low");
        gassert(l_vfPosition.y <= 180.f,"Latitude too high");

        gassert(!_isnan(in_Point.m_fDistance),"NaN distance");
        gassert(in_Point.m_iRegionID <= g_ServerDAL.NbRegion(),"Region ID too high");

        //Can't check if path points are all land or all water, as paths can legitimately contain both types of points as long as group doesn't stop at invalid point
    });

    //Check that path ends in valid position
    //But during early loading, there won't be any units, and therefore the group will think it's non-naval (by default). Only check path if there are units present
    if(!l_vPathPoints.empty() && !l_vpUnits.empty())
    {
        const GVector2D<REAL32>& l_vfLastPathPoint = l_vPathPoints.back().m_Position;
        gassert(IsNaval() == (g_ServerDCL.EarthLocateRegion(l_vfLastPathPoint.x, l_vfLastPathPoint.y) == 0), "Ground group will stop on water, or naval group will stop on land");
    }
}
