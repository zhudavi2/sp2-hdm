/**************************************************************
*
* sp2_hdm_database.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Copyright  (C) 2021, D. Z. (BlasterMillennia)
***************************************************************/
#include "golem_pch.hpp"

bool GHdmDatabase::Load()
{
    GDZLOG(EDZLogLevel::Entry, L"");

    bool l_bResult = false;

    do
    {
        static const GString c_sDatabaseFilename = L"hdm_database.xml";
        GFile l_XMLFile;
        if(!g_ServerDAL.File(c_sDatabaseFilename, l_XMLFile))
        {
            g_Joshua.Log(SP2::ERROR_CANT_FIND_FILE + c_sDatabaseFilename, MSGTYPE_WARNING);
            break;
        }

        //Extract the file to a temp string so it can be parsed as XML
        GString l_sTempConfigFileText;
        if(!l_XMLFile.Extract(l_sTempConfigFileText))
        {
            g_Joshua.Log(SP2::ERROR_EXTRACT_FILE + l_XMLFile.Name(), MSGTYPE_WARNING);
            break;
        }

        GXMLParser l_XmlParser;
        GTree<GXMLNode>* const l_pXmlData = l_XmlParser.Parse(l_sTempConfigFileText);
        l_XMLFile.Unextract();
        if(l_pXmlData == nullptr)
        {
            g_Joshua.Log(SP2::ERROR_PARSING_XML_FILE + l_XMLFile.Name(), MSGTYPE_WARNING);
            break;
        }

        CTreeXMLNode* const l_pXmlRoot = l_pXmlData->Root();
        if(!l_pXmlRoot->HasChild())
        {
            g_Joshua.Log(SP2::ERROR_CANT_PROCESS_FILE + l_XMLFile.Name(), MSGTYPE_WARNING);
            break;
        }

        for(UINT32 i = 0; i < l_pXmlRoot->NbChilds(); i++)
        {
            const GTreeNode<GXMLNode>* const l_pTable = l_pXmlRoot->Child(i);
            const GString l_sTableName  = l_pTable->Data().m_sName;

            if(l_sTableName == L"REBELS")
            {
                m_viRebelIds.clear();

                for(UINT32 j = 0; j < l_pTable->NbChilds(); j++)
                {
                    const ENTITY_ID l_iRebelId = l_pTable->Child(j)->Data().m_value.ToINT32();
                    if((l_iRebelId == 0) || (l_iRebelId > g_ServerDAL.NbCountry()))
                        g_Joshua.Log(L"Invalid rebel ID " + GString(l_iRebelId), MSGTYPE_WARNING);
                    else if(find(m_viRebelIds.cbegin(), m_viRebelIds.cend(), l_iRebelId) != m_viRebelIds.cend())
                        g_Joshua.Log(L"Duplicate rebel ID " + GString(l_iRebelId), MSGTYPE_WARNING);
                    else
                    {
                        g_Joshua.Log("Add rebel ID " + GString(l_iRebelId), MSGTYPE_MSG);
                        m_viRebelIds.push_back(l_iRebelId);
                    }
                }
            }
            else if(l_sTableName == L"REGION")
            {
                for(UINT32 r = 0; r < l_pTable->NbChilds(); r++)
                {
                    const CTreeXMLNode* const l_pRegionNode = l_pTable->Child(r);
                    GString l_sRegionId;
                    if(!l_pRegionNode->Data().Attribute(L"ID", l_sRegionId))
                    {
                        g_Joshua.Log("Invalid region element at index " + GString(r), MSGTYPE_WARNING);
                        continue;
                    }
                    const UINT32 l_iRegionId = l_sRegionId.ToINT32();
                    if((l_iRegionId == 0) || (l_iRegionId > g_ServerDAL.NbRegion()))
                    {
                        g_Joshua.Log("Invalid region ID " + l_sRegionId, MSGTYPE_WARNING);
                        continue;
                    }

                    for(UINT32 c = 0; c < l_pRegionNode->NbChilds(); c++)
                    {
                        const GXMLNode& l_Data = l_pRegionNode->Child(c)->Data();
                        if(l_Data.m_sName == L"CLAIMANT")
                        {
                            const ENTITY_ID l_iClaimantId = l_Data.m_value.ToINT32();
                            if((l_iClaimantId == 0) || (l_iClaimantId >= g_ServerDAL.NbCountry()))
                            {
                                g_Joshua.Log("Invalid claimant ID " + l_Data.m_value, MSGTYPE_WARNING);
                                continue;
                            }
                            GString l_sAggressive;
                            l_Data.Attribute(L"AGGRESSIVE", l_sAggressive);
                            g_ServerDAL.GetGRegion(l_iRegionId)->AddClaim(l_iClaimantId, l_sAggressive != L"F");
                        }
                    }
                }
            }
            else
                g_Joshua.Log(L"Unknown HDM database table " + l_sTableName, MSGTYPE_WARNING);
        }

        l_bResult = true;
    } while(false);

    GDZLOG(EDZLogLevel::Exit, L"Returning " + GString(l_bResult));
    return l_bResult;
}

const vector<ENTITY_ID>& GHdmDatabase::RebelIds() const
{
    return m_viRebelIds;
}
