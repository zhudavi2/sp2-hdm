/**************************************************************
*
* sp2_historical_events.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Copyright  (C) 2022, D. Z. (BlasterMillennia)
***************************************************************/
#include "golem_pch.hpp"

bool GHistoricalEvents::Load()
{
    GDZLOG(EDZLogLevel::Entry, L"");

    bool l_bResult = false;

    do
    {
        static const GString c_sDatabaseFilename = L"hdm_historical_events.xml";
        GFile l_XMLFile;
        if(!g_ServerDAL.File(c_sDatabaseFilename, l_XMLFile))
        {
            g_Joshua.Log(SP2::ERROR_CANT_FIND_FILE + c_sDatabaseFilename, MSGTYPE_WARNING);
            break;
        }

        //Extract the file to a temp string so it can be parsed as XML
        GString l_sTempConfigFileText;
        if(!l_XMLFile.Extract(l_sTempConfigFileText))
        {
            g_Joshua.Log(SP2::ERROR_EXTRACT_FILE + l_XMLFile.Name(), MSGTYPE_WARNING);
            break;
        }

        GXMLParser l_XmlParser;
        GTree<GXMLNode>* const l_pXmlData = l_XmlParser.Parse(l_sTempConfigFileText);
        l_XMLFile.Unextract();
        if(l_pXmlData == nullptr)
        {
            g_Joshua.Log(SP2::ERROR_PARSING_XML_FILE + l_XMLFile.Name(), MSGTYPE_WARNING);
            break;
        }

        CTreeXMLNode* const l_pXmlRoot = l_pXmlData->Root();
        if(!l_pXmlRoot->HasChild())
        {
            g_Joshua.Log(SP2::ERROR_CANT_PROCESS_FILE + l_XMLFile.Name(), MSGTYPE_WARNING);
            break;
        }

        m_mEvents.clear();

        for(UINT32 i = 0; i < l_pXmlRoot->NbChilds(); i++)
        {
            const GXMLNode& l_EventData = l_pXmlRoot->Child(i)->Data();
            GDateTime l_Date;
            {
                const vector<GXMLAttribute>& l_vAttributes = l_EventData.m_attributeList;
                const GString l_sYear = find_if(l_vAttributes.cbegin(), l_vAttributes.cend(), [](const GXMLAttribute& in_Attr) { return in_Attr.m_sName == L"Year"; })->m_value;
                const GString l_sMonth = find_if(l_vAttributes.cbegin(), l_vAttributes.cend(), [](const GXMLAttribute& in_Attr) { return in_Attr.m_sName == L"Month"; })->m_value;
                const GString l_sDay = find_if(l_vAttributes.cbegin(), l_vAttributes.cend(), [](const GXMLAttribute& in_Attr) { return in_Attr.m_sName == L"Day"; })->m_value;
                GDZLOG(EDZLogLevel::Info1, L"Year " + l_sYear + L", month " + l_sMonth + L", day " + l_sDay);
                l_Date = GDateTime(l_sYear.ToINT64(), l_sMonth.ToINT64(), l_sDay.ToINT64(), 0, 0, 0, 0);
            }

            const GString l_sFullCommand = l_EventData.m_value;
            GDZLOG(EDZLogLevel::Info1, L"Full command: " + l_sFullCommand);

            g_Joshua.Log(L"Historical event: " + l_Date.ToString(GDateTime::etfYYYYMMDD) + L": " + l_sFullCommand, MSGTYPE_MSG);
            m_mEvents[l_Date].push_back(l_sFullCommand);
        }

        l_bResult = true;
    } while(false);

    GDZLOG(EDZLogLevel::Exit, L"Returning " + GString(l_bResult));
    return l_bResult;
}

pair<GHistoricalEventCItr, GHistoricalEventCItr> GHistoricalEvents::GetEventsWithinDates(const GDateTime& in_DateA, const GDateTime& in_DateB) const
{
    GDZLOG(EDZLogLevel::Entry, L"in_DateA = " + in_DateA.ToString(GDateTime::etfYYYYMMDD) + L", in_DateB = " + in_DateB.ToString(GDateTime::etfYYYYMMDD));

    const auto l_BeginIt = find_if(m_mEvents.cbegin(), m_mEvents.cend(), [&in_DateA](const map<GDateTime, GStringVec>::value_type& in_EventPair) { return in_DateA <= in_EventPair.first; });
    if(l_BeginIt != m_mEvents.cend())
    {
        GDZLOG(EDZLogLevel::Info1, L"Actual start date: " + l_BeginIt->first.ToString(GDateTime::etfYYYYMMDD));
    }

    const auto l_EndIt = find_if(l_BeginIt, m_mEvents.cend(), [&in_DateB](const map<GDateTime, GStringVec>::value_type& in_EventPair) { return in_DateB < in_EventPair.first; });
    if(l_EndIt != m_mEvents.cend())
    {
        GDZLOG(EDZLogLevel::Info1, L"Actual (past) end date: " + l_EndIt->first.ToString(GDateTime::etfYYYYMMDD));
    }

    GDZLOG(EDZLogLevel::Exit, L"Returning");
    return pair<GHistoricalEventCItr, GHistoricalEventCItr>(l_BeginIt, l_EndIt);
}
