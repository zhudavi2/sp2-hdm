/*************************************************************
*
* sp2_hdm_save_load.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Copyright  (C) 2022, D.Z. (BlasterMillennia)
***************************************************************/
#include "golem_pch.hpp"

namespace SP2
{
    //! Markers for save data types
    namespace EHdmSaveDataType
    {
        enum Enum
        {
            End114,
            RegionClaims114,
            CovertActionCellCountries114,
        };
    }
}

void GHdmSaveLoad::Save(GIBuffer& io_Buffer)
{
    GDZLOG(EDZLogLevel::Entry, L"io_Buffer.Size() = " + GString(io_Buffer.Size()));

    do
    {
        if(!g_SP2Server->HdmServerConfig().CompatibleSaveFormat())
        {
            GDZLOG(EDZLogLevel::Info1, L"Non-compatible save format, not saving HDM data");
            break;
        }

        g_Joshua.Log(L"Saving HDM Data...", MSGTYPE_MSG);

        //Save markers
        io_Buffer << GHdmRelease::c_iGameType;
        GHdmRelease::c_HdmRelease.Serialize(io_Buffer);

        //10.9 HDI components and name
        for(ENTITY_ID i = 1; i <= g_ServerDAL.NbCountry(); i++)
        {
            const GCountryData* const l_pCountry = g_ServerDAL.CountryData(i);
            io_Buffer << l_pCountry->LifeExpectancy()
                      << l_pCountry->MeanYearsSchooling()
                      << l_pCountry->ExpectedYearsSchooling()
                      << l_pCountry->Name();
        }

        //11.1 Arena unit combat info (deprecated)
        const vector<SDK::Combat::GArena*>& l_vArenas = g_CombatManager.Arenas();
        GDZLOG(EDZLogLevel::Info1, L"Saving " + GString(l_vArenas.size()) + L" arena(s) for 11.1 compatibility");
        for(auto it = l_vArenas.cbegin(); it < l_vArenas.cend(); ++it)
        {
            const SP2::GArena* const l_pArena = dynamic_cast<const SP2::GArena*>(*it);
            io_Buffer << l_pArena->ActorUnitCbtInfo111();
        }

        //11.2 Civil war and new arena unit combat info
        const hash_set<UINT32>& l_viCivilWars = g_ServerDAL.CivilWars();
        GDZLOG(EDZLogLevel::Info1, L"Saving " + GString(l_viCivilWars.size()) + L" civil war(s)");
        io_Buffer << l_viCivilWars;

        const hash_map<ENTITY_ID, REAL64>& l_mCivilWarEndDates = g_ServerDAL.CivilWarEndDates();
        GDZLOG(EDZLogLevel::Info1, L"Saving " + GString(l_mCivilWarEndDates.size()) + L" civil war end date(s)");
        io_Buffer << l_mCivilWarEndDates;

        GDZLOG(EDZLogLevel::Info1, L"Saving " + GString(l_vArenas.size()) + L" arena(s)");
        for(auto it = l_vArenas.cbegin(); it < l_vArenas.cend(); ++it)
        {
            const SP2::GArena* const l_pArena = dynamic_cast<const SP2::GArena*>(*it);
            const hash_map<ENTITY_ID, SP2::GArena::GActorCbtInfo> l_mActorCbtInfo = l_pArena->ActorCbtInfo();
            
            //Can't save ActorCbtInfo hash_map directly
            //Save actor count
            io_Buffer << l_mActorCbtInfo.size();

            //Save actor ID and unit info
            for(auto it = l_mActorCbtInfo.cbegin(); it != l_mActorCbtInfo.cend(); ++it)
            {
                io_Buffer << it->first;
                io_Buffer << it->second.m_UnitInfo;
            }
        }

        //11.4, region claims
        {
            GDZLOG(EDZLogLevel::Info1, L"Saving region claims");
            io_Buffer << static_cast<UINT32>(EHdmSaveDataType::RegionClaims114);
            hash_map<UINT32, hash_map<ENTITY_ID, UINT8>> l_mSaveClaims;
            for(UINT32 i = 1; i <= g_ServerDAL.NbRegion(); i++)
            {
                const hash_map<ENTITY_ID, bool>& l_mRegionClaims = g_ServerDAL.GetGRegion(i)->Claims();
                for(auto it = l_mRegionClaims.cbegin(); it != l_mRegionClaims.cend(); ++it)
                    l_mSaveClaims[i][it->first] = it->second ? 1 : 0;
            }
            io_Buffer << l_mSaveClaims;
        }

        //11.4, covert action cell country data that SP2 doesn't save properly
        {
            //<Country ID, <Cell ID, Cell-related country IDs>>
            hash_map<ENTITY_ID, GCovertActionCellsCountryDataMap> l_mCountryCellMap;
            for(ENTITY_ID i = 1; i < g_ServerDAL.NbCountry(); i++)
            {
                GCovertActionCellsCountryDataMap l_mCellMap = g_ServerDAL.CountryData(i)->CovertActionCellSpecialData();
                if(!l_mCellMap.empty())
                    l_mCountryCellMap[i] = l_mCellMap;
            }

            if(!l_mCountryCellMap.empty())
            {
                GDZLOG(EDZLogLevel::Info1, L"Saving covert cell country data");
                io_Buffer << static_cast<UINT32>(EHdmSaveDataType::CovertActionCellCountries114)
                          << l_mCountryCellMap;
            }
        }

        // End marker
        io_Buffer << static_cast<UINT32>(EHdmSaveDataType::End114);
    } while(false);

    GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Size() = " + GString(io_Buffer.Size()));

    GDZLOG(EDZLogLevel::Exit, L"");
}

void GHdmSaveLoad::Load(GOBuffer& io_Buffer)
{
    GDZLOG(EDZLogLevel::Entry, L"io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()));

    do
    {
        if(!g_SP2Server->HdmServerConfig().CompatibleSaveFormat())
        {
            GDZLOG(EDZLogLevel::Info1, L"Non-compatible save format, not saving HDM data");
            break;
        }

        g_Joshua.Log(L"Loading HDM Data...", MSGTYPE_MSG);

        //Load markers
        if(io_Buffer.Remaining() < sizeof(UINT32) + sizeof(INT32) * 3)
        {
            g_Joshua.Log(L"Save file doesn't contain HDM markers, treating as non-HDM save and generating HDM data", MSGTYPE_MSG);
            GenerateHdmData();
            break;
        }
        
        UINT32 l_iSdkModGameType = 0;
        GHdmRelease l_Hdm;

        io_Buffer >> l_iSdkModGameType;
        l_Hdm.Unserialize(io_Buffer);

        if(l_iSdkModGameType != GHdmRelease::c_iGameType)
        {
            const GString l_sNonHdmSdkModGameType = L"Save file doesn't contain valid HDM markers, treating as non-HDM save";
            GDZLOG(EDZLogLevel::Always, l_sNonHdmSdkModGameType + L"; expected " + GDZDebug::FormatHex(GHdmRelease::c_iGameType) + L", got " + GDZDebug::FormatHex(l_iSdkModGameType));
            g_Joshua.Log(l_sNonHdmSdkModGameType, MSGTYPE_MSG);
            break;
        }

        g_Joshua.Log(L"HDM save file " + l_Hdm.ToString(), MSGTYPE_MSG);

        //10.9 HDI components and name
        if(l_Hdm >= GHdmRelease(10, 9))
        {
            for(ENTITY_ID i = 1; i <= g_ServerDAL.NbCountry(); i++)
            {
                REAL32 l_fLifeExpectancy = 0.f;
                REAL32 l_fMeanYearsSchooling = 0.f;
                REAL32 l_fExpectedYearsSchooling = 0.f;
                GString l_sName;

                io_Buffer >> l_fLifeExpectancy
                          >> l_fMeanYearsSchooling
                          >> l_fExpectedYearsSchooling
                          >> l_sName;

                GCountryData* const l_pCountry = g_ServerDAL.CountryData(i);

                l_fLifeExpectancy = !_isnan(l_fLifeExpectancy) ? max(0.f, l_fLifeExpectancy) : 0.f;
                l_pCountry->LifeExpectancy(l_fLifeExpectancy);

                l_fMeanYearsSchooling = !_isnan(l_fMeanYearsSchooling) ? max(0.f, l_fMeanYearsSchooling) : 0.f;
                l_pCountry->MeanYearsSchooling(l_fMeanYearsSchooling);

                l_fExpectedYearsSchooling = !_isnan(l_fExpectedYearsSchooling) ? max(0.f, l_fExpectedYearsSchooling) : 0.f;
                l_pCountry->ExpectedYearsSchooling(l_fExpectedYearsSchooling);

                l_pCountry->Name(l_sName);

                g_SP2Server->Countries().at(i - 1).Name(l_sName);
            }
        }

        //11.1 Arena unit combat info (deprecated)
        const vector<SDK::Combat::GArena*>& l_vArenas = g_CombatManager.Arenas();
        if(l_Hdm >= GHdmRelease(11, 1))
        {
            GDZLOG(EDZLogLevel::Info1, L"Loading (consuming) old unit combat info from " + GString(l_vArenas.size()) + L" arena(s)");
            for(auto it = l_vArenas.cbegin(); it < l_vArenas.cend(); ++it)
            {
                hash_map<ENTITY_ID,hash_map<UINT32, SP2::GArena::GUnitCbtInfo>> l_mActorUnitCbtInfo;
                io_Buffer >> l_mActorUnitCbtInfo;
            }
        }

        //11.2 Civil war and new arena unit combat info
        if(l_Hdm >= GHdmRelease(11, 2))
        {
            hash_set<UINT32> l_viCivilWars;
            io_Buffer >> l_viCivilWars;
            GDZLOG(EDZLogLevel::Info1, L"Loading " + GString(l_viCivilWars.size()) + L" civil war(s)");
            g_ServerDAL.CivilWars(l_viCivilWars);

            hash_map<ENTITY_ID, REAL64> l_mCivilWarEndDates;
            io_Buffer >> l_mCivilWarEndDates;
            GDZLOG(EDZLogLevel::Info1, L"Loading " + GString(l_mCivilWarEndDates.size()) + L" civil war end date(s)");
            g_ServerDAL.CivilWarEndDates(l_mCivilWarEndDates);
            
            if(io_Buffer.Remaining() > 0)
            {
                GDZLOG(EDZLogLevel::Info1, L"Loading unit combat info from " + GString(l_vArenas.size()) + L" arena(s)");
                for(auto it = l_vArenas.cbegin(); it < l_vArenas.cend(); ++it)
                {
                    //Load actor count
                    hash_map<ENTITY_ID, SP2::GArena::GActorCbtInfo>::size_type l_iActorCount;
                    io_Buffer >> l_iActorCount;

                    //Load actor ID and unit info
                    hash_map<ENTITY_ID, SP2::GArena::GActorCbtInfo> l_mActorCbtInfo;
                    for(UINT32 i = 0; i < l_iActorCount; i++)
                    {
                        ENTITY_ID l_iActorId;
                        io_Buffer >> l_iActorId;

                        SP2::GArena::GActorCbtInfo l_Info;
                        io_Buffer >> l_Info.m_UnitInfo;
                        l_mActorCbtInfo[l_iActorId] = l_Info;
                    }

                    SP2::GArena* const l_pArena = dynamic_cast<SP2::GArena*>(*it);
                    l_pArena->ActorCbtInfo(l_mActorCbtInfo);
                }
            }
        }

        //11.4 and later, process save data type markers
        bool l_bEndFound = false;
        while((io_Buffer.Remaining() > 0) && !l_bEndFound)
        {
            UINT32 l_iSaveDataType = 0;
            io_Buffer >> l_iSaveDataType;
            switch(l_iSaveDataType)
            {
            case EHdmSaveDataType::End114:
            {
                GDZLOG(EDZLogLevel::Info1, L"Found end marker " + GString(EHdmSaveDataType::End114) + L", stop loading");
                l_bEndFound = true;
                break;
            }
            case EHdmSaveDataType::RegionClaims114:
            {
                GDZLOG(EDZLogLevel::Info1, L"Found save data type " + GString(EHdmSaveDataType::RegionClaims114) + L", loading region claims");
                hash_map<UINT32, hash_map<ENTITY_ID, UINT8>> l_mSaveClaims;
                io_Buffer >> l_mSaveClaims;
                for(auto l_SaveRegionIt = l_mSaveClaims.cbegin(); l_SaveRegionIt != l_mSaveClaims.cend(); ++l_SaveRegionIt)
                {
                    const hash_map<ENTITY_ID, UINT8>& l_mSaveRegionClaims = l_SaveRegionIt->second;
                    for(auto l_SaveRegionClaimIt = l_mSaveRegionClaims.cbegin(); l_SaveRegionClaimIt != l_mSaveRegionClaims.cend(); ++l_SaveRegionClaimIt)
                        g_ServerDAL.GetGRegion(l_SaveRegionIt->first)->AddClaim(l_SaveRegionClaimIt->first, l_SaveRegionClaimIt->second == 1);
                }
                break;
            }
            case EHdmSaveDataType::CovertActionCellCountries114:
            {
                GDZLOG(EDZLogLevel::Info1, L"Found save data type " + GString(EHdmSaveDataType::CovertActionCellCountries114) + L", loading covert cell data");
                
                //<Country ID, <Cell ID, Cell-related country IDs>>
                hash_map<ENTITY_ID, hash_map<UINT32, GCovertActionCellCountryData>> l_mCellMap;
                io_Buffer >> l_mCellMap;
                for_each(l_mCellMap.cbegin(), l_mCellMap.cend(), [](const hash_map<ENTITY_ID, hash_map<UINT32, GCovertActionCellCountryData>>::value_type& in_CellMapPair)
                {
                    GHdmCovertActionCell::UpdateCountryDataWithCellMap(in_CellMapPair.second, *(g_ServerDAL.CountryData(in_CellMapPair.first)));
                    g_ServerDAL.CountryData(in_CellMapPair.first)->m_bCovertActionCellsDirty = true;
                });
                break;
            }
            default:
                GDZLOG(EDZLogLevel::Info1, L"Ignoring HDM save data marker value " + GString(l_iSaveDataType));
                break;
            }
        }
    } while(false);

    GDZLOG(EDZLogLevel::Info1, L"io_Buffer.Remaining() = " + GString(io_Buffer.Remaining()));

    GDZLOG(EDZLogLevel::Exit, L"");
}

void GHdmSaveLoad::GenerateHdmData()
{
    GDZLOG(EDZLogLevel::Entry, L"");

    for(ENTITY_ID i = 1; i <= g_ServerDAL.NbCountry(); i++)
    {
        if(g_ServerDAL.CountryValidityArray(i))
        {
            GCountryData* const l_pCountry = g_ServerDAL.CountryData(i);

            gassert(l_pCountry->Name().empty(),"Generating already-loaded name");
            l_pCountry->Name(g_SP2Server->Countries()[i - 1].Name());

            gassert(l_pCountry->LifeExpectancy() == 0, "Generating already-loaded LE");
            gassert(l_pCountry->MeanYearsSchooling() == 0, "Generating already-loaded MYS");
            gassert(l_pCountry->ExpectedYearsSchooling() == 0, "Generating already-loaded EYS");
            l_pCountry->InitHdiComponents();
        }
    }

    GDZLOG(EDZLogLevel::Exit, L"");
}
