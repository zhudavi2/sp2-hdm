/**************************************************************
*
* sp2_server_console_command_handler.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Copyright  (C) 2021, D. Z. (BlasterMillennia)
***************************************************************/

#include "golem_pch.hpp"

GServerConsoleCommandHandler::GServerConsoleCommandHandler() :
m_rTextRegex(L"(\\S+)")
{
}

SDK::GAME_MSG GServerConsoleCommandHandler::Initialize()
{
    GDZLOG(EDZLogLevel::Entry, L"");

    // Register server specific console commands
    // #277 Too-long command names can cause the "help" command to corrupt the heap and crash
    // Longest SP2 V1.5.x command, military_removal, is 16 characters
    // Longest HDM command that doesn't seem to cause heap corruption, transfer_units_in_region, is 24 characters
    {
        g_Joshua.RegisterConsoleCommand(
            L"print_server_name",
            L"",
            L"Used to check the name of the server",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"name",
            L"S",
            L"Used to change the name of the server",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"version",
            L"",
            L"Gives the current version # of the game.",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"new",
            L"",
            L"Create a new game.",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"newfrom",
            L"S",
            L"Create a new game from the specified database.",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"load",
            L"S",
            L"Load a game from the specified file.",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"save",
            L"S",
            L"Save the game to the specified file.",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            "kick",  
            L"S",
            L"Kick a player",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            "ban",  
            L"S",
            L"ban a player",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            "clear_ban_list",  
            L"",
            L"Clear the list of banned people",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            "set_admin_country",
            L"I",
            L"Set server admin player by country ID",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            "set_admin_player",
            L"I",
            L"Set server admin player by player ID",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            "print_players",
            L"",
            L"List all current players",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
             L"print_max_players",
             L"",
             L"Print maximum number of players in server",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
             L"set_max_players",
             L"I",
             L"Set maximum number of players in server",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
             L"print_military_control",
             L"I",
             L"List all regions under military control of country, as well as political and historical claims",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
             L"print_foreign_presence",
             L"I",
             L"List all foreign military units on country's territory",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
             L"print_regions_in_area",
             L"IIII",
             L"List all regions within given low and high latitudes, and low and high longitudes",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
             L"declare_peace",
             L"",
             L"Force all countries to declare peace, ending all wars",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
             L"set_password",
             L"S",
             L"Set server password",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
             L"load_hdm_cfg",
             L"",
             L"Load HDM server config file",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"load_hdm_database",
            L"",
            L"Load HDM database",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"load_hdm_historical_events",
            L"",
            L"Load HDM historical events",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
             L"print_bombardments",
             L"",
             L"List all units currently bombarding, and source and target countries",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
             this);
    }

    // Commands, or "cheats", that manipulate the game more greatly
    {
        g_Joshua.RegisterConsoleCommand(
            L"build",
            L"SSS",
            L"Used to build units, 3 Params needed: CountryID DesignID Qty",    
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"start_game",
            L"",
            L"Artificially start the game",    
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"fight",
            L"",
            L"Artificially setup a fight",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"status",
            L"II",
            L"Change status of a unit group",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"declare_war",
            L"SS",
            L"Declare war between the 2 sides.",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"start_civil_war",
            L"I",
            L"Cause civil war in country",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

		g_Joshua.RegisterConsoleCommand(
            L"print_wars",
            L"",
            L"Print the description of every wars currently going on.",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"sell_units",
            L"",
            L"Print, and sell, necessary units for every country",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"print_relations",
            L"I",
            L"Print relations of that country",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"embargo",
            L"II",
            L"First country make a trade embargo against second country.",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"occupy",
            L"II",
            L"First country will occupy second country.",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"military_removal",
            L"II",
            L"First country ask for second country to leave",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"annex",
            L"II",
            L"annex all possible regions of second country to first country",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"bombard",
            L"II",
            L"test bombardment, country1 tries to bombard country2",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"givemoney",
            L"II",
            L"give money to accelerate testing country, money amount",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"build_amds",
            L"I",
            L"force country to build AMDS",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"print_amds",
            L"I",
            L"print country's AMDS status",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"force_occupy",
            L"III",
            L"All second country's regions will become occupied by first country, test conquer",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"research_nuclear",
            L"I",
            L"force country to start nuclear research",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"print_nuclear_research",
            L"I",
            L"print country's general nuclear research level, and range, precision, and damage research levels",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
             L"force_client",
             L"II",
             L"first country creates client treaty with second country",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
             this);

        g_Joshua.RegisterConsoleCommand(
             L"force_anarchy",
             L"I",
             L"country falls into anarchy",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
             this);

        g_Joshua.RegisterConsoleCommand(
             L"set_stability",
             L"II",
             L"Set country stability, 2 Params needed: CountryID Stability%",
             (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"transfer_region",
            L"SIII",
            L"Force region transfer to new owner, 4 Params: RegionId NewOwnerId TransferMilitaryControl TransferMiliaryUnits",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"transfer_units_in_region",
            L"III",
            L"Force transfer of units from old to new owner, 3 Params: RegionId OldOwnerId NewOwnerId",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"transfer_country",
            L"III",
            L"Force country and military units transfer to new owner, 3 Params: OldOwnerId NewOwnerId TransferMilitaryControlIfOccupiedBy3rdCountry",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"transfer_claims",
            L"IIII",
            L"Force claimed region transfer to new owner, 4 Params: OldOwnerId NewOwnerId TransferMilitaryControl TransferMiliaryUnits",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"transfer_research",
            L"II",
            L"Force research transfer from old to new owner, 2 Params: OldOwnerId NewOwnerId",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"join_treaty",
            L"III",
            L"Force country to join treaty, 3 Params: CountryId TreatyId Side",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"leave_treaty",
            L"II",
            L"Force country to leave treaty, 2 Params: CountryId TreatyId",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand, this);

        g_Joshua.RegisterConsoleCommand(
            L"print_region_religions",
            L"I",
            L"print region's religions and population",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"print_region_languages",
            L"I",
            L"print region's languages and population",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"give_population",
            L"II",
            L"add population to country, 2 Params: CountryId Population",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"set_diplomatic_relations",
            L"II",
            L"country has given DR with all others, 2 Params: CountryId DiplomaticRelationsScore",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"copy_dr",
            L"II",
            L"Copy diplomatic relation scores from country A to B, 2 Params: CountryA CountryB",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"set_country_name",
            L"IS",
            L"2 Params: CountryId Name, use underscores in name instead of spaces (underscores will be replaced with spaces if name change goes through)",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);

        g_Joshua.RegisterConsoleCommand(
            L"save_entities",
            L"",
            L"Write AI training data, collected so far from the current session, to entities XML file (default ehe_entities.xml)",
            (CALLBACK_HANDLER_GS_crGS_cvrGS)&GServerConsoleCommandHandler::HandleCommand,
            this);
    }

    GDZLOG(EDZLogLevel::Exit, L"Returning SDK::GAME_OK");
    return SDK::GAME_OK;
}

GString GServerConsoleCommandHandler::HandleCommand(const GString& in_sFullCommand)
{
    GDZLOG(EDZLogLevel::Entry, L"in_sFullCommand = " + in_sFullCommand);

    wsmatch l_vMatches;
    regex_search(in_sFullCommand, l_vMatches, m_rTextRegex);
    const GString l_sMainCommand = l_vMatches[1].str();

    GString l_sArgs = l_vMatches.suffix().str();
    GStringVec l_vsArgs;
    while(regex_search(l_sArgs, l_vMatches, m_rTextRegex))
    {
        l_vsArgs.push_back(l_vMatches[1].str());
        l_sArgs = l_vMatches.suffix().str();
    }

    const GString l_sReturnMessage = HandleCommand(l_sMainCommand, l_vsArgs);

    GDZLOG(EDZLogLevel::Exit, L"Returning " + l_sReturnMessage);
    return l_sReturnMessage;
}

GString GServerConsoleCommandHandler::HandleCommand(const GString& in_sCommand, const vector<GString>& in_vArgs)
{
    GDZLOG(EDZLogLevel::Entry, L"in_sCommand = " + in_sCommand + L", in_vArgs.size() = " + GString(in_vArgs.size()));

    for(auto it = in_vArgs.cbegin(); it < in_vArgs.cend(); ++it)
        GDZLOG(EDZLogLevel::Info1, L"in_vArgs[" + GString(distance(in_vArgs.cbegin(), it)) + L"]: " + *it);

    GString l_sReturnMessage;

    if(in_sCommand == L"print_server_name")
    {
        l_sReturnMessage = L"Server name: " + g_Joshua.ServerName();
    }
    else if(in_sCommand == L"name")
    {
        g_Joshua.ServerName(in_vArgs[0]);
        l_sReturnMessage = L"Changed server name to: " + in_vArgs[0];
    }
    else if(in_sCommand == L"version")
    {
        l_sReturnMessage = g_Joshua.CurrentMod().m_sName + " V" + g_Joshua.CurrentMod().m_sVersion + L" (" + GHdmRelease::c_sName + L")";;
    }
    else if(in_sCommand == L"new")
    {
        g_SP2Server->RequestNewGame(c_sDatabaseLocation);
    }
    else if(in_sCommand == L"newfrom")
    {
        g_SP2Server->RequestNewGame(in_vArgs[0]);
    }
    else if(in_sCommand == L"load")
    {
        g_SP2Server->RequestLoadGame(in_vArgs[0]);
    }
    else if(in_sCommand == L"save")
    {
        g_SP2Server->RequestSaveGame(in_vArgs[0], 0);
    }
    else if(in_sCommand == "kick")
    {
        if(in_vArgs[0].ToINT32() > 0)
            g_SP2Server->RequestSomeKickingAction(in_vArgs[0].ToINT32());
        else
            g_SP2Server->RequestSomeKickingAction(in_vArgs[0]);
    }
    else if(in_sCommand == "ban")
    {
        if(in_vArgs[0].ToINT32() > 0)
            g_SP2Server->RequestSomeBanningAction(in_vArgs[0].ToINT32());
        else
            g_SP2Server->RequestSomeBanningAction(in_vArgs[0]);
    }
    else if(in_sCommand == "clear_ban_list")
    {
        // Clear list of banned people
        g_Joshua.ClearBanList();
    }
    else if(in_sCommand == "set_admin_country")
    {
        INT32 l_iNewAdminCountryID = in_vArgs[0].ToINT32();
        SDK::GPlayer* l_pNewAdminPlayer = g_Joshua.ActivePlayerByModID(l_iNewAdminCountryID);
        g_SP2Server->ChangeAdminPlayer(l_pNewAdminPlayer);
    }
    else if(in_sCommand == "set_admin_player")
    {
        INT32 l_iNewAdminID = in_vArgs[0].ToINT32();
        SDK::GPlayer* l_pNewAdminPlayer = g_Joshua.ActivePlayer(l_iNewAdminID);
        g_SP2Server->ChangeAdminPlayer(l_pNewAdminPlayer);
    }
    else if(in_sCommand == "print_players")
    {
        GString l_sPlayersList;
        for(SDK::GPlayers::const_iterator l_HumanPlayersIt = g_Joshua.HumanPlayers().begin();
            l_HumanPlayersIt != g_Joshua.HumanPlayers().end();
            ++l_HumanPlayersIt)
        {
            SDK::GPlayer* l_pPlayer = l_HumanPlayersIt->second;
            gassert(l_pPlayer != nullptr, L"No player for ID " + GString(l_HumanPlayersIt->first));

            const ENTITY_ID l_iCountryID = l_pPlayer->ModID();
            if(l_pPlayer->PlayerStatus() == SDK::PLAYER_STATUS_ACTIVE &&
                l_iCountryID > 0)
            {
                // Special handling when printing HDM information
                GString l_sHdmString;
                const GHdmRelease l_Hdm = g_SP2Server->PlayerHdm(l_HumanPlayersIt->first);
                if(l_Hdm == GHdmRelease::c_NonHdm)
                    l_sHdmString = (g_SP2Server->InternalPasswordFromPlayer(l_HumanPlayersIt->first) == 0) ? L"non-HDM or pre-10.7" : L"HDM 10.7 to 11.1";
                else
                    l_sHdmString = L"HDM " + l_Hdm.ToString();

                const SDK::GGenericNetworkClient* const l_pNetworkClient = dynamic_cast<SDK::GGenericNetworkClient*>(l_pPlayer->Client());
                const REAL64 l_fPing = l_pNetworkClient->Ping();

                g_Joshua.Log(L"Player ID " + GString(l_pPlayer->Id()) + L": " +
                    L"Name " + l_pPlayer->Name() + L", " +
                    L"country " + g_ServerDAL.CountryData(l_iCountryID)->NameAndIDForLog() + ", " +
                    l_sHdmString + L", " +
                    L"relative ping " + GString(l_fPing));
            }
        }
    }
    else if(in_sCommand == L"print_max_players")
    {
        g_Joshua.Log(L"Maximum number of players in server: " + GString(g_Joshua.MaxPlayers()));
    }
    else if(in_sCommand == L"set_max_players")
    {
        const INT32 l_iMaxPlayers = in_vArgs.front().ToINT32();
        if(l_iMaxPlayers > 0)
        {
            g_Joshua.Log(L"Setting maximum number of players in server to " + GString(l_iMaxPlayers));
            g_Joshua.MaxPlayers(l_iMaxPlayers);
        }
        else
            g_Joshua.Log(L"Invalid maximum number of players in server " + GString(l_iMaxPlayers), MSGTYPE_ERROR);
    }
    else if(in_sCommand == L"print_military_control")
    {
        const ENTITY_ID l_iCountryID = in_vArgs.front().ToINT32();
        if(0 < l_iCountryID && l_iCountryID <= g_ServerDAL.NbCountry())
        {
            g_Joshua.Log(g_ServerDAL.CountryData(l_iCountryID)->NameAndIDForLog() + L":");

            const set<UINT32> l_vMilitaryControl = g_ServerDAL.CountryMilitaryControl(l_iCountryID);
            for(auto it = l_vMilitaryControl.cbegin(); it != l_vMilitaryControl.cend(); ++it)
            {
                const GVector2D<REAL32> l_Location = g_ServerDCL.UnitMover().RegionLocation(*it);
                const GRegionEx* const l_pRegion = g_ServerDAL.GetGRegion(*it);
                g_Joshua.Log(g_ServerDAL.RegionNameAndIDForLog(*it) + L": " +
                    L"Location " + GString(l_Location.x) + L", " + GString(l_Location.y) + L", " +
                    L"political control " + g_ServerDAL.CountryData(l_pRegion->OwnerId())->NameAndIDForLog() + L", " +
                    L"claimant(s) " + l_pRegion->ClaimantsToString());
            }
        }
    }
    else if(in_sCommand == L"print_foreign_presence")
    {
        const ENTITY_ID l_iCountryID = in_vArgs.front().ToINT32();
        if(0 < l_iCountryID && l_iCountryID <= g_ServerDAL.NbCountry())
        {
            g_Joshua.Log(L"Foreign presence in " + g_ServerDAL.CountryData(l_iCountryID)->NameAndIDForLog() + L":");

            const set<UINT32>& l_vRegions = g_ServerDAL.CountryPoliticalControl(l_iCountryID);
            for(auto l_RegionIt = l_vRegions.cbegin(); l_RegionIt != l_vRegions.cend(); ++l_RegionIt)
            {
                const set<UINT32>& l_vUnitGroups = g_ServerDCL.UnitMover().UnitGroupsInsideRegion(*l_RegionIt);
                for(auto l_UnitGroupIt = l_vUnitGroups.cbegin(); l_UnitGroupIt != l_vUnitGroups.cend(); ++l_UnitGroupIt)
                {
                    const SDK::Combat::GUnitGroup* const l_pGroup = g_Joshua.UnitManager().UnitGroup(*l_UnitGroupIt);
                    const ENTITY_ID l_iGroupOwner = l_pGroup->OwnerId();

                    if(l_iGroupOwner != l_iCountryID)
                        g_Joshua.Log(g_ServerDAL.RegionNameAndIDForLog(*l_RegionIt) + L": " +
                            g_ServerDAL.CountryData(l_iGroupOwner)->NameAndIDForLog() + L", "
                            L"location " + GString(l_pGroup->Position().x) + L", " + GString(l_pGroup->Position().y));
                }
            }
        }
    }
    else if(in_sCommand == L"print_regions_in_area")
    {
        const REAL32 l_fMinLat  = in_vArgs[0].ToREAL32();
        const REAL32 l_fMaxLat  = in_vArgs[1].ToREAL32();
        const REAL32 l_fMinLong = in_vArgs[2].ToREAL32();
        const REAL32 l_fMaxLong = in_vArgs[3].ToREAL32();

        // For GRectangle, lowest coordinates are at the top-left
        const GRectangle<REAL32> l_Rectangle(l_fMinLong, l_fMinLat, l_fMaxLong - l_fMinLong, l_fMaxLat - l_fMinLat);

        // 1-based vector
        const vector<GRegionControl> l_vRegionControl = g_ServerDAL.RegionControlArray();
        for(auto it = l_vRegionControl.cbegin() + 1; it < l_vRegionControl.cend(); ++it)
        {
            const UINT32 l_iRegionID = distance(l_vRegionControl.cbegin(), it);
            const GVector2D<REAL32> l_Location = g_ServerDCL.UnitMover().RegionLocation(l_iRegionID);
            if(l_Rectangle.Contains(l_Location))
            {
                const GRegionEx* const l_pRegion = g_ServerDAL.GetGRegion(l_iRegionID);
                g_Joshua.Log(g_ServerDAL.RegionNameAndIDForLog(l_iRegionID) + L": " +
                    L"Location " + GString(l_Location.x) + L", " + GString(l_Location.y) + L", " +
                    L"political control " + g_ServerDAL.CountryData(l_pRegion->OwnerId())->NameAndIDForLog() + L", " +
                    L"military control " + g_ServerDAL.CountryData(l_pRegion->OwnerMilitaryId())->NameAndIDForLog() + L", " +
                    L"claimant(s) " + l_pRegion->ClaimantsToString());
            }
        }
    }
    else if(in_sCommand == L"declare_peace")
    {
        const auto& l_mWars = g_ServerDAL.CurrentWars();
        while(!l_mWars.empty())
        {
            const auto l_iTempWarCount = l_mWars.size();

            const auto l_WarIt = l_mWars.cbegin();
            const UINT32 l_iWarId = l_WarIt->first;
            const GWar& l_War = l_WarIt->second;

            if(!l_War.MasterAttackingWantsPeace())
                g_ServerDCL.ChangeOpinionOnWar(l_War.MasterAttacking(), l_iWarId, true);

            // ChangeOpinionOnWar might remove war if defender already wanted peace, so only try to change defender's opinion if war still exists
            if((l_mWars.count(l_iWarId) == 1) && !l_War.MasterDefendingWantsPeace())
                g_ServerDCL.ChangeOpinionOnWar(l_War.MasterDefending(), l_iWarId, true);

            gassert(l_mWars.size() == l_iTempWarCount - 1, L"Didn't remove a war properly");
        }

        const hash_set<UINT32>& l_vBombardingGroups = g_ServerDCL.UnitMover().BombardingGroupList();
        gassert(l_vBombardingGroups.empty(), GString(l_vBombardingGroups.size()) + L" peacetime bombarding groups");
    }
    else if(in_sCommand == L"set_password")
    {
        g_SP2Server->SetPassword(in_vArgs[0]);
        l_sReturnMessage = L"Changed password to: " + in_vArgs[0];
    }
    else if(in_sCommand == L"load_hdm_cfg")
    {
        const bool l_bHdmServerConfigLoaded = g_SP2Server->m_HdmServerConfig.LoadConfigFile();
        if(!l_bHdmServerConfigLoaded)
            g_Joshua.Log(L"Couldn't load HDM server config file", MSGTYPE_WARNING);
    }
    else if(in_sCommand == L"load_hdm_database")
    {
        const bool l_bHdmDatabaseLoaded = g_ServerDAL.m_HdmDatabase.Load();
        if(!l_bHdmDatabaseLoaded)
            g_Joshua.Log(L"Couldn't load HDM database", MSGTYPE_WARNING);
    }
    else if(in_sCommand == L"load_hdm_historical_events")
    {
        const bool l_bHistoricalEventsLoaded = g_ServerDAL.m_HistoricalEvents.Load();
        if(!l_bHistoricalEventsLoaded)
            g_Joshua.Log(L"Couldn't load HDM historical events", MSGTYPE_WARNING);
    }
    else if(in_sCommand == L"print_bombardments")
    {
        const hash_set<UINT32>& l_vBombardingGroups = g_ServerDCL.UnitMover().BombardingGroupList();
        for(auto it = l_vBombardingGroups.cbegin(); it != l_vBombardingGroups.cend(); ++it)
        {
            const SP2::GUnitGroupEx* const l_pGroup = dynamic_cast<const SP2::GUnitGroupEx*>(g_Joshua.UnitManager().UnitGroup(*it));
            const ENTITY_ID l_iGroupOwnerId = l_pGroup->OwnerId();
            const UINT32 l_iRegionId = l_pGroup->RegionToBombard();
            const ENTITY_ID l_iRegionPoliticalControlId = g_ServerDAL.RegionControl(l_iRegionId).m_iPolitical;

            g_Joshua.Log(L"Unit group ID " + GString(*it) + L" of " + g_ServerDAL.CountryData(l_iGroupOwnerId)->NameAndIDForLog() + L" bombarding " + g_ServerDAL.RegionNameAndIDForLog(l_iRegionId) + L" of " + g_ServerDAL.CountryData(l_iRegionPoliticalControlId)->NameAndIDForLog());
        }
    }
    else if(in_sCommand == L"build")
    {
        //Build units
        UINT32  l_iCountryID = (UINT32)in_vArgs[0].ToINT32();
        UINT32  l_iDesignID  = (UINT32)in_vArgs[1].ToINT32();
        UINT32  l_iQty       = (UINT32)in_vArgs[2].ToINT32();
        if((0 < l_iCountryID) && (l_iCountryID <= g_ServerDAL.NbCountry()) &&
           (g_Joshua.UnitManager().UnitDesign(l_iDesignID) != nullptr) &&
           (l_iQty > 0))
            g_ServerDCL.BuildUnits(l_iCountryID, l_iCountryID, l_iDesignID, l_iQty, 0, EUnitProductionPriority::Low);
    }
    else if(in_sCommand == L"start_game")
    {
        if(!g_SP2Server->GameStarted())
            l_sReturnMessage = g_SP2Server->StartGame() ? L"starting..." : L"error : no game data loaded";
        else
            l_sReturnMessage = L"already started";
    }
    else if(in_sCommand == L"givemoney")
    {
        const ENTITY_ID l_iCountry = in_vArgs[0].ToINT32();
        if((0 < l_iCountry) && (l_iCountry <= g_ServerDAL.NbCountry()))
            g_ServerDCL.GiveMoney(l_iCountry, in_vArgs[1].ToREAL64());
    }
    else if(in_sCommand == L"bombard")
    {
        UINT32  l_iCountry1  = (UINT32)in_vArgs[0].ToINT32();
        UINT32  l_iCountry2  = (UINT32)in_vArgs[1].ToINT32();

        //Find the bombardment points of the target country
        vector<UINT32> l_vBombardableRegions = g_ServerDAL.BombardableRegions(l_iCountry2);

        //Find unit group that can bombard given a country
        vector<SDK::Combat::GUnitGroup*> l_vGroupThatCanBombard = g_ServerDAL.UnitGroupsThatCanBombard(l_iCountry1);


        //Pick the 1st region
        if(l_vBombardableRegions.size() && l_vGroupThatCanBombard.size())
        {
            g_ServerDCL.NavalUnitsBombardRegion(l_vGroupThatCanBombard, l_vBombardableRegions[0]);
        }
    }
    else if(in_sCommand == L"fight")
    {
        //Create an arena
        GVector2D<REAL32> l_Pos(0, 0);
        SDK::Combat::GArena* l_pArena = g_CombatManager.CreateArena(g_Joshua.UnitManager().NewArenaInfoID(), l_Pos);
        SP2::GUnitGroup* l_pDefender = (SP2::GUnitGroup*)g_Joshua.UnitManager().UnitGroup(238);
        SP2::GUnitGroup* l_pAttacker = (SP2::GUnitGroup*)g_Joshua.UnitManager().UnitGroup(750);
        set<UINT32> l_Attackers;
        l_Attackers.insert(l_pDefender->OwnerId());
        g_ServerDCL.DeclareNewWar(l_Attackers, l_pDefender->OwnerId(), l_pAttacker->OwnerId());
        l_pArena->AddGroup(l_pDefender);
        l_pArena->AddGroup(l_pAttacker);
    }
    else if(in_sCommand == L"status")
    {
        //Change status of the unit
        SDK::Combat::GUnitGroup* l_pUnit = g_Joshua.UnitManager().UnitGroup(in_vArgs[0].ToINT32());
        EMilitaryStatus::Enum l_eNewStatus = (EMilitaryStatus::Enum)in_vArgs[1].ToINT32();
        g_ServerDCL.UnitMover().ChangeUnitState(l_pUnit, l_eNewStatus);
    }
    else if(in_sCommand == L"declare_war")
    {
        do
        {
            const ENTITY_ID l_iMasterAttacker = in_vArgs[0].ToINT32();
            if((0 < l_iMasterAttacker) && (l_iMasterAttacker <= g_ServerDAL.NbCountry()))
                GDZLOG(EDZLogLevel::Info1, L"Master attacker: " + GString(l_iMasterAttacker));
            else
            {
                l_sReturnMessage = L"Invalid attackers " + in_vArgs[0];
                break;
            }
            set<ENTITY_ID> l_viAttackers = GDataAccessLayerServer::StringToIds(in_vArgs[0], 1, g_ServerDAL.NbCountry());

            const ENTITY_ID l_iMasterDefender = in_vArgs[1].ToINT32();
            if((0 < l_iMasterDefender) && (l_iMasterDefender <= g_ServerDAL.NbCountry()))
                GDZLOG(EDZLogLevel::Info1, L"Master defender: " + GString(l_iMasterDefender));
            else
            {
                l_sReturnMessage = L"Invalid defenders " + in_vArgs[1];
                break;
            }
            set<ENTITY_ID> l_viDefenders = GDataAccessLayerServer::StringToIds(in_vArgs[1], 1, g_ServerDAL.NbCountry());

            g_ServerDCL.DeclareNewWar(l_viAttackers, l_iMasterAttacker, l_viDefenders, l_iMasterDefender, false);
        } while(false);
    }
    else if(in_sCommand == L"start_civil_war")
    {
        const ENTITY_ID l_iCountry = in_vArgs[0].ToINT32();
        if(0 < l_iCountry && l_iCountry <= g_ServerDAL.NbCountry())
        {
            if(g_ServerDCL.ExecuteCivilWar(l_iCountry, true))
                g_ServerDAL.m_mCivilWarEndDates.erase(l_iCountry);
        }
        else
            l_sReturnMessage = L"Invalid country ID " + in_vArgs[0];
    }
    else if(in_sCommand == L"print_wars")
    {
        const hash_map<UINT32, GWar>& l_Wars = g_ServerDAL.CurrentWars();
        for(hash_map<UINT32, GWar>::const_iterator it = l_Wars.begin();
            it != l_Wars.end(); it++)
        {
            const GWar& l_CurWar = it->second;
            GString SideA = L"";
            GString SideB = L"";
            for(set<UINT32>::const_iterator it = l_CurWar.AttackingSide().begin();
                it != l_CurWar.AttackingSide().end(); it++)
            {
                SideA += g_ServerDAL.GetString(g_ServerDAL.CountryData(*it)->NameID()) + L", ";
            }
            g_Joshua.Log(SideA);
            g_Joshua.Log(L" vs ");
            for(set<UINT32>::const_iterator it = l_CurWar.DefendingSide().begin();
                it != l_CurWar.DefendingSide().end(); it++)
            {
                SideB += g_ServerDAL.GetString(g_ServerDAL.CountryData(*it)->NameID()) + L", ";
            }
            g_Joshua.Log(SideB);
            g_Joshua.Log(L"------------");
        }
    }
    else if(in_sCommand == L"occupy")
    {
        set<UINT32> l_EmptyListOfCountries;
        g_ServerDCL.OccupyCountry(in_vArgs[0].ToINT32(), in_vArgs[1].ToINT32(), l_EmptyListOfCountries);
    }
    else if(in_sCommand == L"military_removal")
    {
        //First country ask for second country to leave
        set<UINT32> l_SideA;
        set<UINT32> l_SideB;
        set<UINT32> l_SidePressure;
        l_SideA.insert(in_vArgs[0].ToINT32());
        l_SideB.insert(in_vArgs[1].ToINT32());
        UINT32 l_iConditions[ETreatyConditions::ItemCount];
        Memory::Fill<UINT32>(l_iConditions, 0, ETreatyConditions::ItemCount);
        GString l_sTreatyName = g_ServerDAL.TreatyName(ETreatyType::RequestMilitaryPresenceRemoval, g_ServerDAL.GetString(g_ServerDAL.CountryData(in_vArgs[0].ToINT32())->NameID()));
        UINT32 l_iTreatyID = g_ServerDCL.CreateNewTreaty(in_vArgs[0].ToINT32(), l_SideA, l_SideB, l_SidePressure, ETreatyType::RequestMilitaryPresenceRemoval, true, l_sTreatyName, l_iConditions);
        g_ServerDCL.JoinTreaty(in_vArgs[1].ToINT32(), l_iTreatyID, 2);
    }
    else if(in_sCommand == L"embargo")
    {
        //First country make a trade embargo against second country
        set<UINT32> l_SideA;
        set<UINT32> l_SideB;
        set<UINT32> l_SidePressure;
        l_SideA.insert(in_vArgs[0].ToINT32());
        l_SideB.insert(in_vArgs[1].ToINT32());
        UINT32 l_iConditions[ETreatyConditions::ItemCount];
        Memory::Fill<UINT32>(l_iConditions, 0, ETreatyConditions::ItemCount);
        GString l_sTreatyName = g_ServerDAL.TreatyName(ETreatyType::EconomicEmbargo, g_ServerDAL.GetString(g_ServerDAL.CountryData(in_vArgs[0].ToINT32())->NameID()));
        g_ServerDCL.CreateNewTreaty(in_vArgs[0].ToINT32(), l_SideA, l_SideB, l_SidePressure, ETreatyType::EconomicEmbargo, true, l_sTreatyName, l_iConditions);
    }
    else if(in_sCommand == "annex")
    {
        UINT32 l_iConquering = in_vArgs[0].ToINT32();
        UINT32 l_iConquered = in_vArgs[1].ToINT32();
        const set<UINT32>& l_MilControl = g_ServerDAL.CountryMilitaryControl(l_iConquering);
        for(set<UINT32>::const_iterator i = l_MilControl.begin(); i != l_MilControl.end(); ++i)
        {
            if(g_ServerDAL.RegionControl(*i).m_iPolitical == l_iConquered)
            {
                g_ServerDCL.AnnexRegion(l_iConquering, *i);
            }
        }
    }
    else if(in_sCommand == "print_relations")
    {
        UINT32 l_iCountry = in_vArgs[0].ToINT32();
        g_Joshua.Log(g_ServerDAL.CountryData(l_iCountry)->NameAndIDForLog() + L": ");

        multimap<REAL32, ENTITY_ID> l_mRelationsMap;
        for(UINT32 i=1; i<= (UINT32)g_ServerDAL.NbCountry(); i++)
        {
            l_mRelationsMap.insert(make_pair<REAL32, ENTITY_ID>(g_ServerDAL.RelationBetweenCountries(l_iCountry, i), i));
        }

        for(multimap<REAL32, ENTITY_ID>::const_reverse_iterator l_Rit = l_mRelationsMap.crbegin();
            l_Rit != l_mRelationsMap.crend();
            ++l_Rit)
        {
            if(l_Rit->second != l_iCountry)
                g_Joshua.Log(g_ServerDAL.CountryData(l_Rit->second)->NameAndIDForLog() + L": " +
                    GString::FormatNumber(l_Rit->first, 1));
        }
    }
    else if(in_sCommand == "sell_units")
    {
        g_Joshua.Log(L"*** Selling units ***");
        for(UINT32 i=1; i<= (UINT32)g_ServerDAL.NbCountry(); i++)
        {
            REAL64 l_fBudgetRevenues = g_ServerDAL.CountryData(i)->BudgetRevenues();
            REAL64 l_fBudgetExpenses = g_ServerDAL.CountryData(i)->BudgetExpenses();

            multimap<REAL32, UINT32> l_UnitsPossibleToSale;
            //Make a list of units that may be sold
            const set<UINT32>& l_vUnitGroupsID = g_Joshua.UnitManager().CountryUnitGroups(i);
            for(set<UINT32>::const_iterator l_It = l_vUnitGroupsID.begin();
                l_It != l_vUnitGroupsID.end();
                l_It++)
            {
                SDK::Combat::GUnitGroup* l_pUnitGroup = g_Joshua.UnitManager().UnitGroup((*l_It));
                for(vector<SDK::Combat::GUnit*>::const_iterator l_It2 = l_pUnitGroup->Units().begin();
                    l_It2 != l_pUnitGroup->Units().end();
                    l_It2++)
                {
                    SDK::Combat::GUnit* l_pUnitThatMayBeDisbanded = *l_It2;
                    if(((SP2::GUnitDesign*)l_pUnitThatMayBeDisbanded->Design())->Type()->Category() == EUnitCategory::Infantry ||
                        g_ServerDCL.IsUnitForSale(l_pUnitThatMayBeDisbanded->Id()) ||
                        g_ServerDCL.UnitMover().IsUnitInTraining(l_pUnitThatMayBeDisbanded->Id()))
                        continue;
                    l_UnitsPossibleToSale.insert(make_pair<REAL32, UINT32>(((SP2::GUnitDesign*)l_pUnitThatMayBeDisbanded->Design())->Cost(), l_pUnitThatMayBeDisbanded->Id()));
                }
            }

            if(l_fBudgetExpenses <= l_fBudgetRevenues)
                continue;

            //Now, disband that kind of units until we have a positive budget. We have to disband at least 1 group
            for(multimap<REAL32, UINT32>::iterator it = l_UnitsPossibleToSale.begin();
                it != l_UnitsPossibleToSale.end(); it++)
            {
                //We must calculate the cost of that unit
                REAL32 l_fUpkeepCostOfUnit = g_ServerDCL.UpkeepFeeForSingleUnit((SP2::GUnit*)g_Joshua.UnitManager().Unit(it->second));
                l_fBudgetExpenses -= l_fUpkeepCostOfUnit;

                g_Joshua.Log(GString(it->second));
                g_ServerDCL.SellUnit(it->second);

                if(l_fBudgetExpenses <= l_fBudgetRevenues)
                    break;
            }
        }
        g_Joshua.Log(L"*** End Selling units ***");
    }
    else if(in_sCommand == L"build_amds")
    {
        ENTITY_ID l_iCountry = in_vArgs[0].ToINT32();
        g_ServerDCL.StartAMDSResearch(l_iCountry);
    }
    else if(in_sCommand == L"print_amds")
    {
        ENTITY_ID l_iCountry = in_vArgs[0].ToINT32();
        const GCountryData* l_pCountryData = g_ServerDAL.CountryData(l_iCountry);
        l_sReturnMessage = l_pCountryData->NameAndIDForLog() + L": AMDS level " + GString::FormatNumber(l_pCountryData->AMDSLevel(), 3);
    }
    else if(in_sCommand == L"force_occupy")
    {
        const ENTITY_ID l_iOccupier = in_vArgs[0].ToINT32();
        const ENTITY_ID l_iTarget    = in_vArgs[1].ToINT32();
        const bool l_bTestConquer    = in_vArgs[2].ToINT32() > 0;

        const set<UINT32>& l_vPoliticalRegions = g_ServerDAL.CountryPoliticalControl(l_iTarget);
        for(set<UINT32>::const_iterator l_It = l_vPoliticalRegions.begin();
            l_It != l_vPoliticalRegions.end();
            ++l_It)
        {
            if(g_ServerDAL.RegionControlArray()[*l_It].m_iMilitary != l_iOccupier)
                g_ServerDCL.ChangeRegionMilitaryControl(*l_It, l_iOccupier, l_bTestConquer);
        }
    }
    else if(in_sCommand == L"research_nuclear")
    {
        const ENTITY_ID l_iCountry = in_vArgs[0].ToINT32();
        g_ServerDCL.StartNuclearResearch(l_iCountry);
    }
    else if(in_sCommand == L"print_nuclear_research")
    {
        const ENTITY_ID l_iCountry = in_vArgs[0].ToINT32();
        const GCountryData* const l_pCountryData = g_ServerDAL.CountryData(l_iCountry);
        GString l_sNuclearResearchString(l_pCountryData->NameAndIDForLog() + L": " + GString(l_pCountryData->NuclearReady()));
        for(INT32 i = EUnitDesignCharacteristics::MissileRange; i <= EUnitDesignCharacteristics::MissileDamage; i++)
        {
            const EUnitDesignCharacteristics::Enum l_eResearchCategory = static_cast<EUnitDesignCharacteristics::Enum>(i);
            l_sNuclearResearchString += L" " + GString(l_pCountryData->ResearchInfo()->m_fMaxValues[EUnitCategory::Nuclear][l_eResearchCategory]);
        }
        l_sReturnMessage = l_sNuclearResearchString;
    }
    else if(in_sCommand == L"force_client")
    {
        set<ENTITY_ID> l_vSideA;
        const ENTITY_ID l_iMaster = in_vArgs[0].ToINT32();
        l_vSideA.insert(l_iMaster);

        set<ENTITY_ID> l_vSideB;
        const ENTITY_ID l_iClient = in_vArgs[1].ToINT32();
        l_vSideB.insert(l_iClient);

        //Needs non-const GString reference for some reason
        GString l_sName(GSatelliteStateControl::c_sTreatyPrefix + L" - " + g_ServerDAL.CountryData(l_iMaster)->NameAndIDForLog() + L"-" + g_ServerDAL.CountryData(l_iClient)->NameAndIDForLog());

        const vector<UINT32> l_vConditions(ETreatyConditions::ItemCount, 0);

        g_ServerDCL.CreateNewTreaty(l_iMaster, l_vSideA, l_vSideB, set<ENTITY_ID>(), ETreatyType::MilitaryAccess, true, l_sName, l_vConditions.data());
    }
    else if(in_sCommand == L"force_anarchy")
    {
        const ENTITY_ID l_iCountryId = in_vArgs[0].ToINT32();
        if(g_ServerDAL.CountryValidityArray(l_iCountryId))
            g_ServerDCL.ChangeGovernmentType(l_iCountryId, static_cast<EGovernmentType::Enum>(g_ServerDAL.CountryData(l_iCountryId)->GvtType()), EGovernmentType::Anarchy);
    }
    else if(in_sCommand == L"set_stability")
    {
        const ENTITY_ID l_iCountryId = in_vArgs[0].ToINT32();
        const REAL32     l_fStability = in_vArgs[1].ToREAL32() / 100.f;
        if(g_ServerDAL.CountryValidityArray(l_iCountryId))
        {
            const REAL32 l_fOldStability = g_ServerDAL.CountryData(l_iCountryId)->GvtStability();
            g_ServerDCL.ChangeCountryStability(l_iCountryId, l_fStability - l_fOldStability, false);
        }
    }
    else if(in_sCommand == L"transfer_region")
    {
        set<UINT32> l_viRegions = GDataAccessLayerServer::StringToIds(in_vArgs[0], 1, g_ServerDAL.NbRegion());
        for(auto it = l_viRegions.cbegin(); it != l_viRegions.cend(); ++it)
        {
            const UINT32 l_iRegionId = *it;
            GDZLOG(EDZLogLevel::Info1, L"Region ID: " + GString(l_iRegionId));

            if(l_iRegionId <= g_ServerDAL.NbRegion())
            {
                const ENTITY_ID l_iNewOwnerId = in_vArgs[1].ToINT32();
                if((l_iNewOwnerId == 0) || (g_ServerDAL.NbCountry() < l_iNewOwnerId))
                {
                    GDZLOG(EDZLogLevel::Warning, L"Invalid new owner ID " + GString(l_iNewOwnerId));
                    break;
                }

                const ENTITY_ID l_iOldOwnerId = g_ServerDAL.RegionControl(l_iRegionId).m_iPolitical;
                gassert((0 < l_iOldOwnerId) && (l_iOldOwnerId <= g_ServerDAL.NbCountry()),"Invalid old owner ID");

                const bool l_bChangeUnitOwner = in_vArgs[3].ToINT32() > 0;
                if(l_bChangeUnitOwner)
                    g_ServerDCL.ChangeUnitsOwnerInRegion(l_iRegionId, l_iOldOwnerId, l_iNewOwnerId);

                //Change military control before political, in line with other region transfers in the game
                const bool l_bChangeMilitaryControl = in_vArgs[2].ToINT32() > 0;
                if(l_bChangeMilitaryControl)
                {
                    if(!g_ServerDCL.ChangeRegionMilitaryControl(l_iRegionId, l_iNewOwnerId))
                        g_Joshua.Log(L"Region military control transfer failed", MSGTYPE_WARNING);
                }

                if(!g_ServerDCL.ChangeRegionPoliticalControl(l_iRegionId, l_iNewOwnerId, true))
                    g_Joshua.Log(L"Region political control transfer failed", MSGTYPE_WARNING);
            }
        }
    }
    else if(in_sCommand == L"transfer_units_in_region")
    {
        const UINT32 l_iRegionId = in_vArgs[0].ToINT32();
        const ENTITY_ID l_iOldOwnerId = in_vArgs[1].ToINT32();
        const ENTITY_ID l_iNewOwnerId = in_vArgs[2].ToINT32();

        if(((0 < l_iOldOwnerId) && (l_iOldOwnerId <= g_ServerDAL.NbCountry())) &&
           ((0 < l_iNewOwnerId) && (l_iNewOwnerId <= g_ServerDAL.NbCountry())))
            g_ServerDCL.ChangeUnitsOwnerInRegion(l_iRegionId, l_iOldOwnerId, l_iNewOwnerId);
    }
    else if(in_sCommand == L"transfer_country")
    {
        //Transfer naval units first
        const ENTITY_ID l_iOldOwnerId = in_vArgs[0].ToINT32();
        const ENTITY_ID l_iNewOwnerId = in_vArgs[1].ToINT32();
        g_ServerDCL.ChangeUnitsOwnerInRegion(0, l_iOldOwnerId, l_iNewOwnerId);

        //Implement rest of functionality by calling transfer_region on all old owner's remaining regions
        GStringVec l_vsTransferRegionArgs = in_vArgs;

        l_vsTransferRegionArgs[0] = L""; //Region IDs
        const set<UINT32> l_vRegions = g_ServerDAL.CountryPoliticalControl(l_iOldOwnerId);
        for_each(l_vRegions.cbegin(), l_vRegions.cend(), [&l_vsTransferRegionArgs](const UINT32 in_iId) { l_vsTransferRegionArgs[0] += GString(in_iId) + "/"; });

        //l_vsTransferRegionArgs[1] New owner ID
        //l_vsTransferRegionArgs[2] Change military control
        l_vsTransferRegionArgs.push_back(L"1"); //Change unit owner

        HandleCommand(L"transfer_region", l_vsTransferRegionArgs);
    }
    else if(in_sCommand == L"transfer_claims")
    {
        const ENTITY_ID l_iOldOwner = in_vArgs[0].ToINT32();
        const ENTITY_ID l_iNewOwner = in_vArgs[1].ToINT32();

        //Implement by calling transfer_region on all old owner's regions that new owner claims
        GStringVec l_vsTransferRegionArgs = in_vArgs;

        l_vsTransferRegionArgs[0] = L""; //Region IDs
        const set<UINT32> l_viRegions = g_ServerDAL.CountryPoliticalControl(l_iOldOwner);
        for(auto it = l_viRegions.cbegin(); it != l_viRegions.cend(); ++it)
        {
            if(g_ServerDAL.GetGRegion(*it)->Claims().count(l_iNewOwner) == 1)
                l_vsTransferRegionArgs[0] += GString(*it) + "/";
        }

        HandleCommand(L"transfer_region", l_vsTransferRegionArgs);
    }
    else if(in_sCommand == L"transfer_research")
    {
        const ENTITY_ID l_iOldOwnerId = in_vArgs[0].ToINT32();
        const ENTITY_ID l_iNewOwnerId = in_vArgs[1].ToINT32();

        for(INT32 i = EUnitCategory::Infantry; i < EUnitCategory::ItemCount; i++)
        {
            EUnitCategory::Enum l_eCategory = static_cast<EUnitCategory::Enum>(i);
            for(INT32 j = EUnitDesignCharacteristics::GunRange; j < EUnitDesignCharacteristics::ItemCount; j++)
            {
                EUnitDesignCharacteristics::Enum l_eChar = static_cast<EUnitDesignCharacteristics::Enum>(j);
                REAL32& l_fNewOwnerLevel = g_ServerDAL.CountryData(l_iNewOwnerId)->ResearchInfo()->m_fMaxValues[l_eCategory][l_eChar];
                l_fNewOwnerLevel = max(l_fNewOwnerLevel, g_ServerDAL.CountryData(l_iOldOwnerId)->ResearchInfo()->m_fMaxValues[l_eCategory][l_eChar]);
            }
        }
    }
    else if(in_sCommand == L"join_treaty")
    {
        const ENTITY_ID l_iCountryId  = in_vArgs[0].ToINT32();
        const UINT32    l_iTreatyId   = in_vArgs[1].ToINT32();
        const UINT32    l_iSideToJoin = in_vArgs[2].ToINT32();
        g_ServerDCL.JoinTreaty(l_iCountryId, l_iTreatyId, l_iSideToJoin, true);
    }
    else if(in_sCommand == L"leave_treaty")
    {
        const ENTITY_ID l_iCountry = in_vArgs[0].ToINT32();
        if((l_iCountry > 0) && (l_iCountry <= g_ServerDAL.NbCountry()))
        {
            const UINT32 l_iTreaty = in_vArgs[1].ToINT32();
            g_ServerDCL.LeaveTreaty(l_iCountry, l_iTreaty);
        }
        else
            l_sReturnMessage = L"Invalid country ID " + GString(l_iCountry);
    }
    else if(in_sCommand == L"print_region_religions")
    {
        const UINT32 l_iRegionId = in_vArgs[0].ToINT32();
        if((l_iRegionId > 0) || (l_iRegionId <= g_ServerDAL.NbRegion()))
        {
            GRegionEx* const l_pRegion = g_ServerDAL.GetGRegion(l_iRegionId);
            const INT64 l_iRegionPop = l_pRegion->Population();
            g_Joshua.Log(l_pRegion->NameForLog() + L", population " + GString::FormatNumber(static_cast<REAL64>(l_iRegionPop), L",", L"", L"", L"", 3, 0) + L":");

            GReligionList l_mReligionList;
            l_pRegion->GetReligions(l_mReligionList);

            GCountryDataItf* const l_pCountry = g_ServerDAL.CountryData(l_pRegion->OwnerId());

            for_each(l_mReligionList.cbegin(), l_mReligionList.cend(), [l_pCountry, l_iRegionPop](const pair<INT32, INT64>& in_ReligionListPair)
            {
                const INT32 l_iReligionId = in_ReligionListPair.first;
                const GString l_sReligion = g_ServerDAL.GetString(g_ServerDAL.StringIdReligion(l_iReligionId));
                const GString l_sStatus = g_ServerDAL.GetString(g_ServerDAL.StringIdStatus(l_pCountry->ReligionGetStatus(l_iReligionId)));

                const INT64 l_iReligionPop = in_ReligionListPair.second;
                const GString l_sReligionPop = GString::FormatNumber(static_cast<REAL64>(l_iReligionPop), L",", L"", L"", L"", 3, 0);
                const GString l_sReligionPercent = GString::FormatNumber(static_cast<REAL32>(l_iReligionPop) / l_iRegionPop * 100.f, L"", L".", L"", L"%", 3, 2);

                g_Joshua.Log(l_sReligion + L" (" + GString(l_iReligionId) + L"): " + l_sStatus + L", population " + l_sReligionPop + L", " + l_sReligionPercent);
            });
        }
        else
            l_sReturnMessage = L"Invalid region ID " + GString(l_iRegionId);
    }
    else if(in_sCommand == L"print_region_languages")
    {
        const UINT32 l_iRegionId = in_vArgs[0].ToINT32();
        if((l_iRegionId > 0) || (l_iRegionId <= g_ServerDAL.NbRegion()))
        {
            GRegionEx* const l_pRegion = g_ServerDAL.GetGRegion(l_iRegionId);
            const INT64 l_iRegionPop = l_pRegion->Population();
            g_Joshua.Log(l_pRegion->NameForLog() + L", population " + GString::FormatNumber(static_cast<REAL64>(l_iRegionPop), L",", L"", L"", L"", 3, 0) + L":");

            GLanguageList l_mLanguageList;
            l_pRegion->GetLanguages(l_mLanguageList);

            GCountryDataItf* const l_pCountry = g_ServerDAL.CountryData(l_pRegion->OwnerId());

            for_each(l_mLanguageList.cbegin(), l_mLanguageList.cend(), [l_pCountry, l_iRegionPop](const pair<INT32, INT64>& in_LanguageListPair)
            {
                const INT32 l_iLanguageId = in_LanguageListPair.first;
                const GString l_sLanguage = g_ServerDAL.GetString(g_ServerDAL.StringIdLanguage(l_iLanguageId));
                const GString l_sStatus = g_ServerDAL.GetString(g_ServerDAL.StringIdStatus(l_pCountry->LanguageGetStatus(l_iLanguageId)));

                const INT64 l_iLanguagePop = in_LanguageListPair.second;
                const GString l_sLanguagePop = GString::FormatNumber(static_cast<REAL64>(l_iLanguagePop), L",", L"", L"", L"", 3, 0);
                const GString l_sLanguagePercent = GString::FormatNumber(static_cast<REAL32>(l_iLanguagePop) / l_iRegionPop * 100.f, L"", L".", L"", L"%", 3, 2);

                g_Joshua.Log(l_sLanguage + L" (" + GString(l_iLanguageId) + L"): " + l_sStatus + L", population " + l_sLanguagePop + L", " + l_sLanguagePercent);
            });
        }
        else
            l_sReturnMessage = L"Invalid region ID " + GString(l_iRegionId);
    }
    else if(in_sCommand == L"give_population")
    {
        const ENTITY_ID l_iCountry = in_vArgs[0].ToINT32();
        do
        {
            if((l_iCountry <= 0) || (l_iCountry > g_ServerDAL.NbCountry()))
            {
                l_sReturnMessage = L"Invalid country ID " + GString(l_iCountry);
                break;
            }

            const set<UINT32>& l_viRegions = g_ServerDAL.CountryPoliticalControl(l_iCountry);
            if(l_viRegions.empty())
            {
                l_sReturnMessage = L"Country has no regions to add population to";
                break;
            }

            const GCountryData* const l_pCountry = g_ServerDAL.CountryData(l_iCountry);
            const INT64 l_iCountryPop = l_pCountry->Population();
            GDZLOG(EDZLogLevel::Info1, l_pCountry->NameAndIDForLog() + L" has population " + GString::FormatNumber(static_cast<REAL64>(l_iCountryPop), L",", L"", L"", L"", 3, 0));

            const INT64 l_iPopToAdd = in_vArgs[1].ToINT64();
            INT64 l_iPopRemaining = l_iPopToAdd;

            for(auto it = l_viRegions.cbegin(); it != l_viRegions.cend(); ++it)
            {
                const GRegion* const l_pRegion = g_ServerDAL.GetGRegion(*it);
                const INT64 l_iRegionPop = l_pRegion->Population();

                //Handle case of country with regions but no population
                const INT64 l_iRegionPopToAdd = (l_iCountryPop > 0) ? static_cast<INT64>(static_cast<REAL32>(l_iRegionPop) / l_iCountryPop * l_iPopToAdd) : (l_iPopToAdd / l_viRegions.size());

                GDZLOG(EDZLogLevel::Info1, g_ServerDAL.RegionNameAndIDForLog(*it) + L" has population " + GString::FormatNumber(static_cast<REAL64>(l_iRegionPop), L",", L"", L"", L"", 3, 0) + L", will add " + GString::FormatNumber(static_cast<REAL64>(l_iRegionPopToAdd), L",", L"", L"", L"", 3, 0));
                if(l_iRegionPopToAdd > 0)
                {
                    g_ServerDCL.AddPopulationToRegion(*it, l_iRegionPopToAdd);
                    l_iPopRemaining -= l_iRegionPopToAdd;
                }
            }

            if(l_iPopRemaining > 0)
            {
                //If we still need to add population, then add to capital region, or add to most-populated region if no capital
                UINT32 l_iRegion = g_ServerDAL.CapitalRegion(l_iCountry);
                if(l_iRegion == 0)
                    l_iRegion = *max_element(l_viRegions.cbegin(), l_viRegions.cend(), [](const UINT32 in_iRegionA, const UINT32 in_iRegionB) { return g_ServerDAL.GetGRegion(in_iRegionA)->Population() < g_ServerDAL.GetGRegion(in_iRegionB)->Population(); });
                GDZLOG(EDZLogLevel::Info1, L"Extra population " + GString::FormatNumber(static_cast<REAL64>(l_iPopRemaining), L",", L"", L"", L"", 3, 0) + L", will add to " + g_ServerDAL.RegionNameAndIDForLog(l_iRegion));
                g_ServerDCL.AddPopulationToRegion(l_iRegion, l_iPopRemaining);
            }
        } while(false);
    }
    else if(in_sCommand == L"set_diplomatic_relations")
    {
        const ENTITY_ID l_iCountry = in_vArgs[0].ToINT32();
        do
        {
            if((l_iCountry <= 0) || (l_iCountry > g_ServerDAL.NbCountry()))
            {
                l_sReturnMessage = L"Invalid country ID " + GString(l_iCountry);
                break;
            }
            const REAL32 l_fDiplomaticRelations = in_vArgs[1].ToREAL32();
            for(ENTITY_ID i = 1; i < g_ServerDAL.NbCountry(); i++)
            {
                if(l_iCountry != i)
                    g_ServerDAL.RelationBetweenCountries(l_iCountry, i, l_fDiplomaticRelations);
            }
        } while(false);
    }
    else if(in_sCommand == L"copy_dr")
    {
    const ENTITY_ID l_iCountryA = in_vArgs[0].ToINT32();
    const ENTITY_ID l_iCountryB = in_vArgs[1].ToINT32();
    do
    {
        if((l_iCountryA <= 0) || (l_iCountryA > g_ServerDAL.NbCountry()))
        {
            l_sReturnMessage = L"Invalid country ID " + GString(l_iCountryA);
            break;
        }
        if((l_iCountryB <= 0) || (l_iCountryB > g_ServerDAL.NbCountry()))
        {
            l_sReturnMessage = L"Invalid country ID " + GString(l_iCountryB);
            break;
        }
        for(ENTITY_ID i = 1; i < g_ServerDAL.NbCountry(); i++)
            g_ServerDAL.RelationBetweenCountries(l_iCountryB, i, g_ServerDAL.RelationBetweenCountries(l_iCountryA, i));
    } while(false);
    }
    else if(in_sCommand == L"set_country_name")
    {
        GString l_sName = in_vArgs[1];
        while(l_sName.find(L"_") != GString::npos)
            l_sName = l_sName.ReplaceNextPattern(L" ", L"_");
        g_ServerDCL.ChangeCountryName(in_vArgs[0].ToINT32(), l_sName);
    }
    else if(in_sCommand == L"save_entities")
        g_SP2Server->SaveEntities();

    GDZLOG(EDZLogLevel::Exit, L"Returning " + l_sReturnMessage);
    return l_sReturnMessage;
}
