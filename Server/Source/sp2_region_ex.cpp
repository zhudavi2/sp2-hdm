/**************************************************************
*
* sp2_region_ex.cpp
*
* Description
* ===========
*  See corresponding .h file
*
* Copyright  (C) 2017, D.Z. "BlasterMillennia"
**************************************************************/
#include "golem_pch.hpp"

void GRegionEx::ChangePopulation(const INT64 in_iPop15, const INT64 in_iPop1565, const INT64 in_iPop65, const GReligionList& in_mReligions, const GLanguageList& in_mLanguages)
{
    GDZLOG(EDZLogLevel::Entry, L"in_iPop15 = " + GDZDebug::FormatInt(in_iPop15) + L", in_iPop1565 = " + GDZDebug::FormatInt(in_iPop1565) + L", in_iPop65 = " + GDZDebug::FormatInt(in_iPop65));

    gassert(in_iPop15 >= 0, L"Negative under-15 population");
    Population15(in_iPop15);

    gassert(in_iPop1565 >= 0, L"Negative 15-65 population");
    Population1565(in_iPop1565);

    gassert(in_iPop65 >= 0, L"Negative 15-65 population");
    Population65(in_iPop65);

    //Almost identical code to set population by religion or language
    //Use 2-iteration loop
    for(INT32 i = 0; i < 2; i++)
    {
        const bool l_bReligion = (i == 0);
        hash_map<INT32, INT64> l_mOldCultures;
        if(l_bReligion)
            GetReligions(l_mOldCultures);
        else
            GetLanguages(l_mOldCultures);

        const hash_map<INT32, INT64>& l_mCultures = l_bReligion ? in_mReligions : in_mLanguages;
        INT64 l_iCulturePop = 0;
        for(auto it = l_mCultures.cbegin(); it != l_mCultures.cend(); ++it)
        {
            gassert(it->second >= 0, L"Negative culture population");
            GDZLOG(EDZLogLevel::Info2, L"Culture ID " + GString(it->first) + L", is religion " + GString(l_bReligion) + L", new population " + GDZDebug::FormatInt(it->second));
            if(l_mOldCultures.count(it->first) != 0)
            {
                GDZLOG(EDZLogLevel::Info2, L"Cultural population before: " + GDZDebug::FormatInt(l_mOldCultures[it->first]));
                if(l_bReligion)
                    ReligionChangePopulation(it->first, it->second);
                else
                    LanguageChangePopulation(it->first, it->second);
            }
            else
            {
                gassert(it->second > 0, L"Nonpositive new culture population");
                GDZLOG(EDZLogLevel::Info2, L"Adding new culture");
                if(l_bReligion)
                    InsertReligion(it->first, static_cast<INT32>(it->second));
                else
                    InsertLanguage(it->first, static_cast<INT32>(it->second));
            }
            l_iCulturePop += it->second;
        }
        gassert(Population() == l_iCulturePop, L"Culture population mismatch");
    }

    VerifyData();
    GDZLOG(EDZLogLevel::Exit, L"");
}

void GRegionEx::VerifyData()
{
#if GASSERT_ENABLE
    GDZLOG(EDZLogLevel::Entry, L"");

    const INT64 l_iPop = Population();
    const INT64 l_iPop15 = Population15();
    const INT64 l_iPop1565 = Population1565();
    const INT64 l_iPop65 = Population65();

    gassert(l_iPop >= 0, L"Invalid total population " + GDZDebug::FormatInt(l_iPop));
    gassert(l_iPop15 >= 0, L"Invalid under-15 population " + GDZDebug::FormatInt(l_iPop15));
    gassert(l_iPop1565 >= 0, L"Invalid 15-65 population " + GDZDebug::FormatInt(l_iPop1565));
    gassert(l_iPop65 >= 0, L"Invalid over-65 population " + GDZDebug::FormatInt(l_iPop65));

    gassert(l_iPop == l_iPop15 + l_iPop1565 + l_iPop65, L"Population by age inconsistent");

    {
        GReligionList l_Religions;
        GetReligions(l_Religions);
        INT64 l_iTotalPopByReligion = 0;
        for(auto it = l_Religions.cbegin(); it != l_Religions.cend(); ++it)
        {
            gassert(it->second >= 0, L"Invalid religion " + GString(it->first) + L" population " + GDZDebug::FormatInt(it->second));
            l_iTotalPopByReligion += it->second;
        }
        gassert(l_iPop == l_iTotalPopByReligion, L"Population by religion inconsistent");
    }

    {
        GLanguageList l_Languages;
        GetLanguages(l_Languages);
        INT64 l_iTotalPopByLanguage = 0;
        for(auto it = l_Languages.cbegin(); it != l_Languages.cend(); ++it)
        {
            gassert(it->second >= 0, L"Invalid language " + GString(it->first) + L" population " + GDZDebug::FormatInt(it->second));
            l_iTotalPopByLanguage += it->second;
        }
        gassert(l_iPop == l_iTotalPopByLanguage, L"Population by language inconsistent");
    }

    const REAL64 l_fTourismIncome = TourismIncome();
    gassert(!_isnan(l_fTourismIncome), "NaN tourism income");
    gassert(0.0 <= l_fTourismIncome, "Tourism income out of range");

    for(INT32 i = EResources::Cereals; i < EResources::ItemCount; i++)
    {
        const EResources::Enum l_eResource = static_cast<EResources::Enum>(i);
        REAL64 l_fProduction = ResourceProduction(l_eResource);
        gassert(!_isnan(l_fProduction), "NaN production");
        gassert(0.0 <= l_fProduction, "Production out of range");
    }

    GDZLOG(EDZLogLevel::Exit, L"");
#endif //#if GASSERT_ENABLE
}

GString GRegionEx::Name() const
{
    return g_ServerDAL.GetString(g_ServerDAL.StringIdRegion(Id()));
}

GString GRegionEx::NameForLog() const
{
    return Name() + L" (" + GString(Id()) + L")";
}

const hash_map<ENTITY_ID, bool>& GRegionEx::Claims() const
{
    return m_mClaims;
}

void GRegionEx::AddClaim(const ENTITY_ID in_iClaimant, const bool in_bAggressive)
{
    gassert((in_iClaimant != 0) && (in_iClaimant <= g_ServerDAL.NbCountry()),"Invalid claimant ID");
    m_mClaims[in_iClaimant] = in_bAggressive;
}

GString GRegionEx::ClaimantsToString() const
{
    GString l_sClaimants;
    for(auto it = m_mClaims.cbegin(); it != m_mClaims.cend(); ++it)
    {
        l_sClaimants += (it == m_mClaims.cbegin()) ? L"" : L", ";
        l_sClaimants += g_ServerDAL.CountryData(it->first)->NameAndIDForLog();
    }
    return l_sClaimants;
}
