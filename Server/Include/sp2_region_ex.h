/**************************************************************
*
* sp2_region_ex.h
*
* Description
* ===========
*  Extension of GRegion class for region-specific utility methods
*
* Copyright  (C) 2017, D.Z. "BlasterMillennia"
***************************************************************/

#ifndef _SP2_REGION_EX_H_
#define _SP2_REGION_EX_H_

namespace SP2
{
    class GRegionEx : public GRegion
    {
    public:
        /*!
         * External entites that modify region population should call this method to add or remove population
         * All population params must be nonnegative
         * In Debug, method will verify population consistency before and after
         *
         * @param in_iPop15: Under-15 population to add
         * @param in_iPop1565: 15-65 population to add
         * @param in_iPop65: Over-65 population to add
         * @param in_mReligions: Population-by-religion to add
         * @param in_mLanguages: Population-by-language to add
         */
        void ChangePopulation(INT64 in_iPop15, INT64 in_iPop1565, INT64 in_iPop65, const GReligionList& in_mReligions, const GLanguageList& in_mLanguages);

        GString Name() const;
        GString NameForLog() const;

        const hash_map<ENTITY_ID, bool>& Claims() const;

        /*!
         * @param in_iClaimant: Claimant country ID
         * @param in_bAggressive: If this claimant gains control of region during civil war, whether this claimant attacks original controller (aggressive), or vice versa (defensive, or non-aggressive)
         */
        void AddClaim(ENTITY_ID in_iClaimant, bool in_bAggressive);

        GString ClaimantsToString() const;

        /*!
         * Debug utility to verify data consistency. Does nothing if asserts are disabled
         * Non-const because GetReligions and GetLanguages are non-const
         **/
        void VerifyData();

    private:
        //! Claims (<claimant ID, aggressive>)
        hash_map<ENTITY_ID, bool> m_mClaims;
    };
}

#endif //_SP2_REGION_EX_H_
