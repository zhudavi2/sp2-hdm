/**************************************************************
*
* sp2_server_console_command_handler.h
*
* Description
* ===========
*  Helper class to handle HDM server console commands.
*
* Copyright  (C) 2021, D. Z. (BlasterMillennia)
***************************************************************/

#ifndef _SP2_SERVER_CONSOLE_COMMAND_HANDLER_H
#define _SP2_SERVER_CONSOLE_COMMAND_HANDLER_H


namespace SP2
{
    class GServerConsoleCommandHandler
    {
    public:
        GServerConsoleCommandHandler();

        //! Registers commands. Returns SDK::GAME_OK.
        SDK::GAME_MSG Initialize();

        //! Helper for historical events, takes full command string as it would be typed in console
        GString HandleCommand(const GString& in_sFullCommand);

    private:
        //! Returns any console message that command should output
        GString HandleCommand(const GString& in_sCommand, const vector<GString>& in_vArgs);

        //! Helper regex. For some reason, local initialization in method would randomly cause access violation
        wregex m_rTextRegex;
    };
}

#endif // _SP2_SERVER_CONSOLE_COMMAND_HANDLER_H
