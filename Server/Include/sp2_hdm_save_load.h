/**************************************************************
*
* sp2_hdm_save_load.h
*
* Description
* ===========
* Save and load HDM-related data to and from save files
*
* Copyright  (C) 2022, D.Z. (BlasterMillennia)
***************************************************************/
#ifndef _SP2_HDM_SAVE_LOAD_H_
#define _SP2_HDM_SAVE_LOAD_H_

namespace SP2
{
   class GHdmSaveLoad
   {
   public:
       static void Save(GIBuffer& io_Buffer);
       static void Load(GOBuffer& io_Buffer);

   private:
       //! Generate HDM data when loading non-HDM save
       static void GenerateHdmData();
   };
}

#endif // _SP2_HDM_SAVE_LOAD_H_