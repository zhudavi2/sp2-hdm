/**************************************************************
*
* sp2_hdm_database.h
*
* Description
* ===========
*  Helper class to load and store HDM-specific database additions.
*
* Copyright  (C) 2021, D. Z. (BlasterMillennia)
***************************************************************/

#ifndef _SP2_HDM_DATABASE_H
#define _SP2_HDM_DATABASE_H


namespace SP2
{
    class GHdmDatabase
    {
    public:
        //! Returns true if successful
        bool Load();

        const vector<ENTITY_ID>& RebelIds() const;

    private:
        vector<ENTITY_ID> m_viRebelIds;
    };
}

#endif // _SP2_HDM_DATABASE_H
