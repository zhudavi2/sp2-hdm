/**************************************************************
*
* sp2_hdm_server_config.h
*
* Description
* ===========
*  Helper class to contain HDM server config settings.
*
* Copyright  (C) 2021, D. Z. (BlasterMillennia)
***************************************************************/

#ifndef _SP2_HDM_SERVER_CONFIG_H
#define _SP2_HDM_SERVER_CONFIG_H


namespace SP2
{
    namespace EGlobalTaxSpecialType
    {
        enum Enum
        {
            ExportAll = 0,
            MeetDomestic,
            ImportAll,
            PrivatizeAll,
            ItemCount,
        };
    }

    struct GAnarchyConfig
    {
        REAL32 m_fChanceDueToStability;
        REAL32 m_fExpectedStabilityLowerLimit;
        REAL32 m_fExpectedStabilityUpperLimit;
        REAL32 m_fStabilityLowerLimit;
    };

    struct GCivilWarConfig
    {
        REAL32 m_fChance;
        REAL32 m_fPeriod;
        REAL32 m_fControlChance;
        REAL32 m_fAnnexChance;
    };

    struct GPlayerLimitsConfig
    {
        bool m_bAdminsAffected;
        INT32 m_iDaysBeforeWar;
    };

    class GHdmServerConfig
    {
    public:
        GHdmServerConfig();

        //! Returns true if successful
        bool LoadConfigFile();

        bool AddCurrentRegionOwnerClaims() const;
        bool DatabaseWarningsEnabled() const;
        bool AllowAIAssumeDebt() const;
        bool AllowDefenderAttackAttackerTerritory() const;

        GAnarchyConfig AnarchyConfig() const;

        REAL32 AnnexationRelationLossPercent() const;

        bool AutoCovertMissions() const;

        GCivilWarConfig CivilWarConfig() const;

        REAL32 CombatThresholdSquare() const;

        bool CompatibleSaveFormat() const;
        bool ContinueAfterEconomicFailure() const;
        bool ContinueAfterElectionLoss() const;
        bool CountryNeedsRegions() const;

        REAL32 DedicatedServerAutosavePeriod() const;
        bool   DedicatedServerAutosaveToJoshuaFolder() const;

        bool DisableNuclearOnOccupy() const;
        bool DisbandAMDSOnOccupy() const;

        REAL64 EntitySavePeriod() const;

        //! Maximum global resource tax rate
        REAL32 GlobalTaxLimit() const;
        void   GlobalTaxLimit(REAL32 in_fGlobalTaxLimit);

        INT32 GlobalTaxSpecial(EGlobalTaxSpecialType::Enum in_eGlobalTaxSpecial) const;

        REAL64 GvtTypeProductionModifier(EGovernmentType::Enum in_iGvtType) const;

        //! Maximum income tax rate per government type
        REAL64 IncomeTaxLimit(EGovernmentType::Enum in_eGovernmentType) const;
        void   IncomeTaxLimit(EGovernmentType::Enum in_eGovernmentType, REAL64 in_fIncomeTaxLimit);

        bool IncomeTaxRateAffectsGrowth() const;
        bool IncreaseDeathRateForAgingPopulation() const;
        bool LogBankruptcies() const;

        //! Maximum number of covert cells each country can have in each foreign country, 0 means no limit
        INT32 MaximumCellsInForeignCountry() const;
        void  MaximumCellsInForeignCountry(INT32 in_iMaxCells);

        GString Message() const;

        REAL32 MilitaryProductionCapacityPercent() const;

        REAL32 MilitaryUpkeepPercentages(EUnitCategory::Enum in_eUnitCategory, ETrainingLevel::Enum in_eTrainingLevel) const;

        bool NavalRuleEnabled() const;

        REAL32 NuclearMissileRangePercentage() const;
        REAL32 NuclearUpkeepPercentage() const;
        REAL32 OccupiedRegionPercentageForNuclear() const;

        GPlayerLimitsConfig PlayerLimitsConfig() const;

        REAL32 ProductionLossOnAnnex() const;

        bool ReinforcedUnitTypesToArenaEdge(EUnitType::Enum in_eUnitType) const;

        //! Maximum tax rate for any resource
        REAL32 ResourceTaxLimit() const;
        void   ResourceTaxLimit(REAL32 in_fResourceTaxLimit);

        bool ServerChatsEnabled() const;
        REAL32 TributePercent() const;

        bool UseNewExportMechanics() const;
        bool UseNewResourceTaxFormula() const;

    private:
        bool m_bAddCurrentRegionOwnerClaims;
        bool m_bDatabaseWarningsEnabled;
        bool m_bAllowAIAssumeDebt;
        bool m_bAllowDefenderAttackAttackerTerritory;

        GAnarchyConfig m_AnarchyConfig;

        REAL32 m_fAnnexationRelationLossPercent;

        bool   m_bAutoCovertMissions;
      
        GCivilWarConfig m_CivilWarConfig;

        REAL32 m_fCombatThresholdSquare;

        bool m_bCompatibleSaveFormat;
        bool m_bContinueAfterEconomicFailure;
        bool m_bContinueAfterElectionLoss;
        bool m_bCountryNeedsRegions;

        REAL32 m_fDedicatedServerAutosavePeriod;
        bool   m_bDedicatedServerAutosaveToJoshuaFolder;

        bool m_bDisableNuclearOnOccupy;
        bool m_bDisbandAMDSOnOccupy;

        REAL64 m_fEntitySavePeriod;

        REAL32 m_fGlobalTaxLimit;

        //! INT32 to eliminate precision issues with REAL32
        map<EGlobalTaxSpecialType::Enum,INT32> m_GlobalTaxSpecials;

        map<EGovernmentType::Enum,REAL64> m_mGvtTypeProductionModifiers;
        map<EGovernmentType::Enum,REAL64> m_IncomeTaxLimits;

        bool m_bIncomeTaxRateAffectsGrowth;
        bool m_bIncreaseDeathRateForAgingPopulation;
        bool m_bLogBankruptcies;

        INT32 m_iMaximumCellsInForeignCountry;

        GString m_sMessage;

        REAL32 m_fMilitaryProductionCapacityPercent;

        map<EUnitCategory::Enum,map<ETrainingLevel::Enum,REAL32>> m_mMilitaryUpkeepPercentages;

        bool m_bNavalRuleEnabled;

        REAL32 m_fNuclearMissileRangePercentage;
        REAL32 m_fNuclearUpkeepPercentage;
        REAL32 m_fOccupiedRegionPercentageForNuclear;

        GPlayerLimitsConfig m_PlayerLimitsConfig;

        REAL32 m_fProductionLossOnAnnex;

        map<EUnitType::Enum,bool> m_mReinforcedUnitTypesToArenaEdge;

        REAL32 m_fResourceTaxLimit;
        bool m_bServerChatsEnabled;
        REAL32 m_fTributePercent;

        bool m_bUseNewExportMechanics;
        bool m_bUseNewResourceTaxFormula;
    };
}

#endif // _SP2_HDM_SERVER_CONFIG_H
