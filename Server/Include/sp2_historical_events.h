/**************************************************************
*
* sp2_historical_events.h
*
* Description
* ===========
*  Helper class to load and store historical (scripted) event details.
*
* Copyright  (C) 2022, D. Z. (BlasterMillennia)
***************************************************************/

#ifndef _SP2_HISTORICAL_EVENTS_H
#define _SP2_HISTORICAL_EVENTS_H


namespace SP2
{
    typedef map<GDateTime, GStringVec>::const_iterator GHistoricalEventCItr;

    class GHistoricalEvents
    {
    public:
        //! Returns true if successful
        bool Load();

        //! Get event iterators within [in_DateA, in_DateB)
        pair<GHistoricalEventCItr, GHistoricalEventCItr> GetEventsWithinDates(const GDateTime& in_DateA, const GDateTime& in_DateB) const;

    private:
        map<GDateTime, GStringVec> m_mEvents;
    };
}

#endif // _SP2_HISTORICAL_EVENTS_H
