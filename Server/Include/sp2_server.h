/**************************************************************
*
* sp2_server.h
*
* Description
* ===========
*  Class that represents the game server.
*
* Owner
* =====
*  Mathieu Tremblay
*
* Copyright  (C) 2003, Laboratoires Golemlabs Inc.
***************************************************************/

#ifndef _SP2_SERVER_H
#define _SP2_SERVER_H


namespace SP2
{
   //Forward Declaration
   class GGeneralEventHandler;
   class GEconomicEventHandler;
   class GMilitaryEventHandler;
   /*!
   * Superpower 2 Game Server Main class
   **/
   class GServer : public SDK::GGameServerItf , public SDK::GGameDataNode
   {
      friend class GServerConsoleCommandHandler;

   public:
      GServer();
      ~GServer();
      SDK::GAME_MSG   Initialize();
      SDK::GAME_MSG   Iterate(void* param);
      SDK::GAME_MSG   Shutdown();
      SDK::GStringTable * StrTable();

      //Function called when a player has joined
      void OnNewPlayerJoined(SDK::GPlayer* in_pPlayer);
      //! Handle disconnections
      void OnPlayerDisconnect(SDK::GPlayer* in_pPlayer);

      //! Gets the data access layer instance
      /*!
       * Get the Data Access Layer Object
       * @return GDataAccessLayerServer: A reference to the DAL
       **/
      inline GDataAccessLayerServer& DAL() { return m_DAL; }

      //! Get the IDs of the human players
      //const vector<UINT32>& HumanPlayers();

      //! Initialize the combat engine, register types etc.
      bool CombatEngineInit();
      //! Shutdown the combat engine
      bool CombatEngineShutdown();

      void SendCountryList(INT32 = SDK::Event::ESpecialTargets::BroadcastHumanPlayers);
      void SendPlayersList(INT32 = SDK::Event::ESpecialTargets::BroadcastHumanPlayers);
      

      //! Get the data control layer
      inline GDataControlLayer& DCL() { return m_DCL; }

      bool GameStarted();
      void GameStarted(bool in_bGameStarted);
      void TryStartGame(void);

      //! Returns true only if game data is loaded
      bool GameLoaded() const;

      void ActivateAIPlayers(void);
      void ActivateReadyHumanPlayers(void);
      UINT32 IsCountryChoosen(UINT16 in_iCountryID);
      void SendInitialDataToPlayer(SDK::GPlayer* in_pPlayer);
      void SendCombatInfo(UINT32 in_iPlayerID);
      void SyncronizeMilitary(INT32 in_iTarget,REAL64 in_fActualClock);
      void SendGameStartEvent(INT32 in_Target = SDK::Event::ESpecialTargets::BroadcastActivePlayers);

      void RequestNewGame(const GString& in_sNewGameDB);
      void RequestNewGame(const GString& in_sNewGameDB, UINT32 in_iSource);
      void RequestLoadGame(const GString& in_sLoadGameFile);
      void RequestLoadGame(const GString& in_sLoadName, UINT32 in_iSource);
      void RequestSaveGame(const GString& in_sSaveGameFile);
      void RequestSaveGame(const GString& in_sSaveName, UINT32 in_iSource);
      void RequestSomeKickingAction(const GString& in_sPlayerNameToKick);
      void RequestSomeKickingAction(UINT32 in_iPlayerID);
      void RequestSomeBanningAction(const GString& in_sPlayerNameToBan);
      void RequestSomeBanningAction(UINT32 in_iPlayerID);


      GCountries& Countries(){return m_Countries;}

      GWorldBehavior& WorldBehavior(){return m_WorldBehavior;}

      inline REAL64 LoadClockAdjust() const { return m_fLoadClockAdjust; }

      inline bool UpdateStaticOnly() const { return m_bUpdateStaticOnly; }
      inline bool CleanVariableOnly() const { return m_bCleanVariableOnly; }
      inline REAL64 NextGameSpeed() const { return m_fNextGameSpeed; }

		REAL32 AIAggressiveness();
		void AIAggressiveness(REAL32 in_fAIAggressiveness);

        //! AI iteration calls this method to track if all AIs have iterated once
        void SetIteratedAI();

      void SendChatMessage(INT32 in_iSource, INT32 in_iTarget, const GString& in_sMessage, bool in_bPrivate) const;

      /*!
      * Helper method to synchronize a player's own country data with client
      * @param in_Player: Player, target of sync event. Non-const because GPlayer::Id and ModID are non-const
      * @param in_bSendAll: True if all data should be sent, false if only dirty data should be sent
      **/
      void SynchronizePlayerCountryData(SDK::GPlayer& in_Player, bool in_bSendAll) const;

      const GHdmServerConfig& HdmServerConfig() const;

      //! Player data helpers
      void AddPlayer(INT32 in_iId, UINT32 in_iPassword, const GHdmRelease& in_Hdm);
      bool PlayerExists(INT32 in_iId) const;
      GHdmRelease PlayerHdm(INT32 in_iId) const;
      UINT32 InternalPasswordFromPlayer(INT32 in_iId) const;
      void SetPlayerJoinDate(INT32 in_iId);

      bool PlayerCanDeclareWar(INT32 in_iId) const;

   protected:
      //! Trigger load game flag
      void GameLoaded(bool in_bGameStarted);

      //! Start the game
      bool StartGame();

      //! Load our mod command line parameters
      void LoadGameOptions();

   private:
      //! Initialize the AI (register actions, objectives and entities in the EHE)
      bool InitAI();
      void RegisterObjectives();
      void RegisterActions();


      //! Function that will receive the UDP Packets
      void OnUDPPacketReception(int in_iPacketSize, void* in_pPointer);

      
      //Handle general game events		
      GGeneralEventHandler    m_EventHandler;
		GAdvisorEventHandler    m_AdvisorEventHandler;
      GEconomicEventHandler   m_EconomicGameEventHandler;
      GPoliticEventHandler    m_PoliticGameEventHandler;
      GMilitaryEventHandler   m_MilitaryGameEventHandler;
		GTreatyEventHandler		m_TreatyGameEventHandler;
      GUnitProductionEventHandler   m_UnitProductionGameEventHandler;

      GAIGeneralEventHandler  m_AIGeneralEventHandler;

      //  Game Data
      //-------------
      GCountries  m_Countries; //Country Id = Vector index + 1
      SDK::GStringTable m_StrTable;
      GFile m_StrTableFile;

      //! DAL
      GDataAccessLayerServer  m_DAL;
      //! DCL
      GWorldBehavior          m_WorldBehavior;
      GDataControlLayer m_DCL;

      REAL64         m_fPreviousSyncDataUpdate;
      REAL64         m_fHalfSecondClock;
      REAL64         m_fFiveSecondsClock;
		REAL64		   m_fCombatIterationPeriod;
		REAL64		   m_fNextCombatIterationTime;
		UINT32			m_iMilitaryRankCountryToIterate;
      multimap<INT32,UINT32> m_OrderCountriesByMilitaryRank;
		multimap<INT32,UINT32>::iterator m_IteratorMilitaryRank;

        //! Last game date on which historical events were queried
        GDateTime m_LastHistoricalEventsDate;

		bool           m_bGameStarted;

      UINT32         m_iNuclearMissilesSubmarineCountryToReload;

      // GGameDataNode implementation
      virtual bool OnSave(GIBuffer& io_Buffer);
      virtual bool OnLoad(GOBuffer& io_Buffer);
      virtual bool OnNew(GDatabase* in_pDatabase);
      virtual void OnClean();

      bool           m_bGameLoaded;

      struct GSaveRequest
      {
         UINT32      m_iSourceID;
         GString     m_sSaveName;
         GString     m_sSaveFile;
      };

      list<GSaveRequest> m_vPendingSaves;
      void SaveGame(const GSaveRequest& in_SaveInfo);
      void LoadGame();
      void NewGame();

      //! Inform game lobby of new player arrival/departure
      void InformPlayerJoined(SDK::GPlayer* in_pPlayer);
      void InformPlayerLeft(SDK::GPlayer* in_pPlayer);

      bool           m_bPendingLoad;
      UINT32         m_iLoadsource;
      GString        m_sLoadName;
      GString        m_sLoadFileName;

      bool           m_bPendingNew;
      UINT32         m_iNewsource;
      GString        m_sNewGameDB;
      GString        m_sCurrentDB;

      REAL64         m_fLoadClockAdjust;
      REAL64         m_fNextGameSpeed;
      REAL64         m_fLastSpeed;
		REAL32			m_fAIAggressiveness;
      bool           m_bUpdateStaticOnly;
      bool           m_bCleanVariableOnly;		

      //! True while the game is still iterating over all AIs for the first time
      bool m_bFirstAIIteration;

      //! Number of first AI iterations that have started, valid when m_bFirstAIIteration is true
      UINT32 m_iIteratedAICount;

      GServerConsoleCommandHandler m_ConsoleCommandHandler;
      GHdmServerConfig m_HdmServerConfig;

      REAL64 m_fTimeOfLastAutosave;

      //! Keep track of players who have joined, and other data
      struct GPlayerData
      {
          GHdmRelease m_Hdm;
          UINT32      m_iInternalPasswordFromClient;
          UINT32      m_iInternalPasswordToClient;
          REAL64      m_fJoinGameDate;
      };
      map<INT32, GPlayerData> m_mPlayerData;

      //! Helper for changing the admin player; returns true if successful
      bool ChangeAdminPlayer(SDK::GPlayer* in_pPlayer);

      GString m_sPlaintextPassword;

      void SetPassword(const GString& in_sPlaintextPassword);

      //! Save AI training data to ENTITIES_FILE
      bool SaveEntities();

      REAL64 m_fLastEntitySaveTime;
   };

}

#define g_SP2Server ((SP2::GServer*)g_Joshua.Server())
#define g_ServerDAL ((SP2::GServer*)g_Joshua.Server())->DAL()
#define g_ServerDCL ((SP2::GServer*)g_Joshua.Server())->DCL()

#endif