set MSBUILD="C:\Program Files\Microsoft Visual Studio\2022\Community\Msbuild\Current\Bin\MsBuild.exe"
set SOLUTIONS=Client\client.sln Server\Server.sln

for %%S in (%SOLUTIONS%) do %MSBUILD% %%S -t:Clean -property:Configuration=Release
for %%S in (%SOLUTIONS%) do %MSBUILD% %%S -property:Configuration=Release

rem Best way to compress files is using PowerShell, so wrap zip file names with \", since PS commands already need to be wrapped with "
set MANUAL_INSTALL_ZIP=\"HDM Manual Install.zip\"
set INSTALLER_ZIP=\"HDM Installer.zip\"

rem Since zip file names already wrapped with \", wrap delete command in " and direct it to PS
powershell "del %MANUAL_INSTALL_ZIP%"
powershell "del %INSTALLER_ZIP%"

powershell "Compress-Archive -Path Client\Release\client.dll, Client\hdm_cfg_client.xml, Server\Release\Server.dll, Server\hdm_cfg_server.xml, Server\hdm_database.xml, Server\hdm_historical_events.xml, README.txt -DestinationPath %MANUAL_INSTALL_ZIP% -Force"

"C:\Program Files (x86)\Inno Setup 6\iscc.exe" Installer.iss
powershell "Compress-Archive -Path *.exe -DestinationPath %INSTALLER_ZIP% -Force"
